#!/usr/bin/env ruby
require 'socket'
require 'json'
 
$words = ['AMON','ASMODAY','ASTAROTH','AZMODEUS','BARBATOS','BASSAGO','BEELZEBUB',
     'BELETB','BELIAL','BERITH','BOTIS','BUER','CAMIO','CROCELL','ELIJOS',
     'PURSON', 'SAMAEL', 'SITRI', 'VALEFOR', 'ZEPAR']

$concept = "Demonios"
$t1 = 0   #tiempo uno
$t2 = 0   #tiempo 2
$leaderboard = []
$keyldb = 1
Socket.tcp_server_loop(3000) do |conn, addr|
  Thread.new do           #CREAR UN HILO POR CADA CLIENTE QUE SE CONECTE.
    client = "#{addr.ip_address}:#{addr.ip_port}"
    puts "#{client} conectado"
    begin
      loop do
        line = conn.readline
        comms = JSON.parse(line)
        if comms[0]=="end"    #END - EL JUGADOR HA TERMINADO DE JUGAR
          $t2 = Time.now      #OBTENER TIEMPO
          delta = $t2 - $t1   #>> agregar esa partida a la tabla de partidas
          $leaderboard.push([comms[1],comms[2],comms[3],((delta*100).round/100.0)])
          puts "TIMER ENDED"
          conn.puts(delta.to_s)
        elsif comms[0]=="reqlst"        #EL JUGADOR HA SOLICITADO LA LISTA DE PALABRAS.
          conn.puts($words.to_json)     #ENVÍAR LA LISTA DE PALABRAS COMO JSON.
        elsif comms[0]=="concept"        #EL JUGADOR HA SOLICITADO EL CONCEPTO.
          conn.puts($concept.to_json)     #ENVÍAR PALABRA COMO JSON.
        elsif comms[0]=="leaderboard"   #EL JUGADOR QUIERE VER LA TABLA DE PUNTUACIONES.
          conn.puts($leaderboard.to_json) #ENVÍAR LA TABLA DE PUNTUACIONES COMO JSON.
        elsif comms[0]=="start"
          $t1 = Time.now     #START - EL JUGADOR HA INICIADO EL JUEGO
          puts "TIMER STARTED"
          conn.puts("timer.started")
        end
        puts "#{client} MSG: #{line}"
      end
    rescue EOFError
      conn.close
      puts "#{client} desconectado"
    end
  end
end
