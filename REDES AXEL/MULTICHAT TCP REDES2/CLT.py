#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import wx, os, wx.richtext, sys, socket, time, pickle, datetime, hashlib, shutil
from threading import Thread, Lock
CarpetaCliente = "Archivos_Recibidos"
HOST = socket.gethostname()
PORT = 6669
PORT_ARCH = 6699
PORT_RECV_ARCH = 6969
NAME = "testClient"		#nombre de la persona que use el app, se establece mediante l.comandos

def recvall(sock,unpickle=None,buffsiz=None):        # Funcion que se encarga de recibir datos a traves de un socket sin importar su tamaño
	if(buffsiz):            #en caso de que se desee recibir datos grandes
		BUFF_SIZE = 2**buffsiz	#18 es recomendado.
	else:   BUFF_SIZE = 512    # Chunks de 512B
	data = b''          # data = arreglo de bytes
	while (True):           # loop infinito
		part = sock.recv(BUFF_SIZE) #recibir de 512B en 512B
		data += part                #anexar los datos
		if(buffsiz):    print("[I]  Recibiendo de ",sock.getpeername(),"... ",round(sys.getsizeof(data)/1000**2,2)," MB\r",end="",sep="")
		if (len(part) < BUFF_SIZE): #Terminar cuando sea 0 o ya no hayan datos
			break                       #-->break
	if(unpickle):
		if(data):	return pickle.loads(data)
		else:	return None
	else:	return data

class Datillo():
    def __init__(self,dato,destinatario,nombreArchivo):
        self.dato = dato
        self.destinatario = destinatario
        self.nombreArchivo = nombreArchivo
        self.tamArchivo = sys.getsizeof(self.dato)/(1000**2)
        sha_1 = hashlib.sha1()
        sha_1.update(dato)
        self.hash = sha_1.hexdigest()

def Archivo2ByteArray(filepath):
    datillos = b''
    if((getSize(filepath)/1000**2)>500):    #500MB
        chunkSize = 2**27   #Trozos de ~134MB
    else:   chunkSize = 2**22   #Trozos de ~4MB
    leidos = 0
    f = open(filepath,'rb')
    l = f.read(chunkSize)
    while (l):
        leidos += sys.getsizeof(l)
        print("leyendo... ",round(((leidos*100)/getSize(filepath)),1)," % \r", sep=' ', end='')
        datillos += l
        l = f.read(chunkSize)
    f.close()
    return datillos

def ByteArray2Archivo(filename,bytearr):
    nuevoArchivo = open(filename, "wb")
    ArregloBytesChido = bytearray(bytearr)
    nuevoArchivo.write(ArregloBytesChido)
    nuevoArchivo.close()

def getSize(filename):
    st = os.stat(filename)  #probar si esta el archivo
    return st.st_size       #checar tam

def enviarArchivo(filename,destinatario):	#Nombre de archivo y tipo de privacidad (global/específico)
	datillos = Archivo2ByteArray(filename)	#Leer el archivo y meterlo en un arreglo de bytes
	ClaseAEnviar = Datillo(datillos,destinatario,os.path.basename(filename))	#inicializar la clase
	datosAEnviar = pickle.dumps(ClaseAEnviar)	#Enviar la clase
	s = socket.socket()         # crear un objeto de socket
	try:	s.connect((HOST, PORT_ARCH))	#Tratar de conectar
	except:	sys.exit(0)
	s.send(datosAEnviar)		#Enviar toda la clase de golpe
	s.close()
	sys.exit(0)

def solicitarArchivo(filename):	#Nombre de archivo y tipo de privacidad (global/específico)
	cwd = os.path.dirname(os.path.realpath(__file__))
	s = socket.socket()         # crear un objeto de socket
	try:	s.connect((HOST, PORT_RECV_ARCH))	#Tratar de conectar
	except:	sys.exit(0)
	try:
		s.send(filename.encode())
		print("[TH]	solicitando...",filename)
	except:	print("error al conectar...")
	while True:
		try:
			data = recvall(s,0,18)
			if data:
				try:
					ass = pickle.loads(data)
				except: print("error al des/serializar")
				#recibido = pickle.loads(data)
				print("[OK] recibido-->",ass.nombreArchivo)
				os.chdir(CarpetaCliente)
				ByteArray2Archivo(ass.nombreArchivo,ass.dato)
				os.chdir(cwd)
				break
			else: #if not data
				raise ValueError('Client disconnected')
		except:     #si el cliente cierra la conexión
			print("[TH] desconectado: ")
			break
		finally:
			s.close()
			print("[TH] salida")
			sys.exit(0)

def enviarMensaje(sock,mensaje,destino=None,func=None):
								if(not func):
									if(destino):
										try:	sock.send(pickle.dumps(["msj",NAME,mensaje,destino]))
										except:
											print("error fatal. (mensajePRV) saliendo...")
											sys.exit(0)
									else:
										try:	sock.send(pickle.dumps(["msj",NAME,mensaje,"global"]))
										except:
											print("error fatal. (mensajeGLB) saliendo...")
											sys.exit(0)
								if(func==1):
									try:	sock.send(pickle.dumps(["listaArchivos","","",""]))
									except:
											print("error fatal. (L.A) saliendo...")
											sys.exit(0)
								if(func==2):
									try:	sock.send(pickle.dumps(["listaClientes","","",""]))
									except:
											print("error fatal. (L.C) saliendo...")
											sys.exit(0)
								if(func==3):
									try:	sock.send(pickle.dumps(["archivo",mensaje,"",""]))
									except:
											print("error fatal. (A) saliendo...")
											sys.exit(0)


class ventPrincipal(wx.Frame):
				def __init__(self, *args, **kwds):
								kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_FRAME_STYLE
								wx.Frame.__init__(self, *args, **kwds)
								#crear conexión
								self.sock = socket.socket()		#thread de recepción de mensajes
								try:	self.sock.connect((HOST,PORT))
								except:	sys.exit(0)
								self.sock.send(pickle.dumps(["agregarNombre",NAME,"",""]))
								#variables utilizadas en toda la UI
								self.listaPeers = []		#Lista de peers actualizada constantemente desde el servidor
								self.listaPeersUI = []		#Lista de peers que utiliza la UI y sincronizada con listaPeers
								self.listaArchivos = {}
								self.listaArchivosUI = []
								self.privacidad = "global"	#Modo de privacidad para el envío de mensajes y archivos
								self.peerSeleccionado = ""	#Peer seleccionado (envío privado)
								#inicializar los widgets de la UI
								self.SetSize((960, 505))
								self.notebook_1 = wx.Notebook(self, wx.ID_ANY)
								self.notebook_1_pane_1 = wx.Panel(self.notebook_1, wx.ID_ANY)
								self.window_1 = wx.SplitterWindow(self.notebook_1_pane_1, wx.ID_ANY)
								self.window_1_pane_1 = wx.Panel(self.window_1, wx.ID_ANY)
								self.window_2 = wx.SplitterWindow(self.window_1_pane_1, wx.ID_ANY)
								self.window_2_pane_1 = wx.Panel(self.window_2, wx.ID_ANY)
								self.rich_text_ctrl_1 = wx.richtext.RichTextCtrl(self.window_2_pane_1, wx.ID_ANY, "", style=wx.HSCROLL | wx.TE_MULTILINE | wx.TE_READONLY)
								self.window_2_pane_2 = wx.Panel(self.window_2, wx.ID_ANY)
								self.text_ctrl_1 = wx.TextCtrl(self.window_2_pane_2, wx.ID_ANY, "", style=wx.HSCROLL | wx.TE_MULTILINE | wx.TE_PROCESS_ENTER)
								self.window_1_pane_2 = wx.Panel(self.window_1, wx.ID_ANY)
								self.window_3 = wx.SplitterWindow(self.window_1_pane_2, wx.ID_ANY, style=wx.SP_3D | wx.SP_THIN_SASH)
								self.window_3_pane_1 = wx.Panel(self.window_3, wx.ID_ANY)
								self.list_ctrl_2 = wx.ListCtrl(self.window_3_pane_1, wx.ID_ANY, style=wx.LC_AUTOARRANGE | wx.LC_HRULES | wx.LC_REPORT | wx.LC_SINGLE_SEL | wx.LC_VRULES)
								self.window_3_pane_2 = wx.Panel(self.window_3, wx.ID_ANY)
								self.window_4 = wx.SplitterWindow(self.window_3_pane_2, wx.ID_ANY, style=wx.SP_3D | wx.SP_THIN_SASH)
								self.window_4_pane_1 = wx.Panel(self.window_4, wx.ID_ANY)
								self.window_5 = wx.SplitterWindow(self.window_4_pane_1, wx.ID_ANY)
								self.window_5_pane_1 = wx.Panel(self.window_5, wx.ID_ANY)
								self.window_5_pane_2 = wx.Panel(self.window_5, wx.ID_ANY)
								self.choice_1 = wx.Choice(self.window_5_pane_2, wx.ID_ANY, choices=["global", "privado"])
								self.window_4_pane_2 = wx.Panel(self.window_4, wx.ID_ANY)
								self.window_6 = wx.SplitterWindow(self.window_4_pane_2, wx.ID_ANY, style=wx.SP_3D | wx.SP_THIN_SASH)
								self.window_6_pane_1 = wx.Panel(self.window_6, wx.ID_ANY)
								self.window_9 = wx.SplitterWindow(self.window_6_pane_1, wx.ID_ANY)
								self.window_9_pane_1 = wx.Panel(self.window_9, wx.ID_ANY)
								self.button_1 = wx.Button(self.window_9_pane_1, wx.ID_ANY, "ENVIAR MENSAJE")
								self.window_9_pane_2 = wx.Panel(self.window_9, wx.ID_ANY)
								self.window_10 = wx.SplitterWindow(self.window_9_pane_2, wx.ID_ANY)
								self.window_10_pane_1 = wx.Panel(self.window_10, wx.ID_ANY)
								self.button_15 = wx.Button(self.window_10_pane_1, wx.ID_ANY, u"\U0001f4f7")
								self.window_10_pane_2 = wx.Panel(self.window_10, wx.ID_ANY)
								self.button_16 = wx.Button(self.window_10_pane_2, wx.ID_ANY, u"\U0001f4c4")
								self.window_6_pane_2 = wx.Panel(self.window_6, wx.ID_ANY)
								self.button_2 = wx.Button(self.window_6_pane_2, wx.ID_ANY, u"\U0001f600")
								self.button_3 = wx.Button(self.window_6_pane_2, wx.ID_ANY, u"\U0001f602")
								self.button_8 = wx.Button(self.window_6_pane_2, wx.ID_ANY, u"\U0001f525")
								self.button_4 = wx.Button(self.window_6_pane_2, wx.ID_ANY, u"\U0001f621")
								self.button_5 = wx.Button(self.window_6_pane_2, wx.ID_ANY, u"\U0001f44c\U0001f3fc")
								self.button_9 = wx.Button(self.window_6_pane_2, wx.ID_ANY, u"\U0001f4af")
								self.button_6 = wx.Button(self.window_6_pane_2, wx.ID_ANY, u"\U0001f61e")
								self.button_7 = wx.Button(self.window_6_pane_2, wx.ID_ANY, u"\U0001f171\ufe0f")
								self.button_10 = wx.Button(self.window_6_pane_2, wx.ID_ANY, u"\U0001f346")
								self.button_11 = wx.Button(self.window_6_pane_2, wx.ID_ANY, u"\U0001f643")
								self.button_12 = wx.Button(self.window_6_pane_2, wx.ID_ANY, u"\U0001f914")
								self.button_13 = wx.Button(self.window_6_pane_2, wx.ID_ANY, u"\U0001f4a9")
								self.notebook_1_Archivoscompartidos = wx.Panel(self.notebook_1, wx.ID_ANY)
								self.window_7 = wx.SplitterWindow(self.notebook_1_Archivoscompartidos, wx.ID_ANY, style=wx.SP_3D | wx.SP_THIN_SASH)
								self.window_7_pane_1 = wx.Panel(self.window_7, wx.ID_ANY)
								self.list_ctrl_1 = wx.ListCtrl(self.window_7_pane_1, wx.ID_ANY, style=wx.LC_AUTOARRANGE | wx.LC_HRULES | wx.LC_REPORT | wx.LC_SINGLE_SEL | wx.LC_VRULES)
								self.window_7_pane_2 = wx.Panel(self.window_7, wx.ID_ANY)
								self.window_8 = wx.SplitterWindow(self.window_7_pane_2, wx.ID_ANY)
								self.window_8_pane_1 = wx.Panel(self.window_8, wx.ID_ANY)
								self.button_14 = wx.Button(self.window_8_pane_1, wx.ID_ANY, "Descargar archivo seleccionado")
								self.window_8_pane_2 = wx.Panel(self.window_8, wx.ID_ANY)
								self.__set_properties()			#establecer propiedades
								self.__do_layout()				#crear diseño
								self.__carpeta_Cliente()		#carpeta del cliente, borrar/crear
								self.recepServ = Thread(target=self.__thread_recepcionDatos).start()	#hilo que recibe mensajes
								self.recepPeerServ = Thread(target=self.__thread_listaSdatos).start()	#hilo que se encarga de actualizar datos
								#enlazar eventos de botones, enter, etc... con funciones
								self.Bind(wx.EVT_TEXT_ENTER, self.ENTER_PRESS, self.text_ctrl_1)
								self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.listaPeerSeleccionadoEVN, self.list_ctrl_2)
								self.Bind(wx.EVT_CHOICE, self.MODO_CHAT, self.choice_1)
								self.Bind(wx.EVT_BUTTON, self.BTN_ENVIAR, self.button_1)
								self.Bind(wx.EVT_BUTTON, self.ENV_IMG, self.button_15)
								self.Bind(wx.EVT_BUTTON, self.ENV_ARCH, self.button_16)
								self.Bind(wx.EVT_BUTTON, self.emoji_smiley1, self.button_2)
								self.Bind(wx.EVT_BUTTON, self.emoji_litaf1, self.button_3)
								self.Bind(wx.EVT_BUTTON, self.emoji_litaf2, self.button_8)
								self.Bind(wx.EVT_BUTTON, self.emoji_angery, self.button_4)
								self.Bind(wx.EVT_BUTTON, self.emoji_litaf3, self.button_5)
								self.Bind(wx.EVT_BUTTON, self.emoji_litaf4, self.button_9)
								self.Bind(wx.EVT_BUTTON, self.emoji_sad, self.button_6)
								self.Bind(wx.EVT_BUTTON, self.emoji_B, self.button_7)
								self.Bind(wx.EVT_BUTTON, self.emoji_eggplant, self.button_10)
								self.Bind(wx.EVT_BUTTON, self.emoji_smiley, self.button_11)
								self.Bind(wx.EVT_BUTTON, self.emoji_thinking, self.button_12)
								self.Bind(wx.EVT_BUTTON, self.emoji_poop, self.button_13)
								self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.listCtrl_seleccion, self.list_ctrl_1)
								self.Bind(wx.EVT_BUTTON, self.btn_descargar, self.button_14)
								self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGED, self.tab_update, self.notebook_1)
								self.Bind(wx.EVT_NOTEBOOK_PAGE_CHANGING, self.tab_update2, self.notebook_1)

				def __set_properties(self):
								#propiedades de los elementos de la UI
								self.SetTitle("Cliente de chat (PID: "+str(os.getpid())+", USUARIO: '"+NAME+" ')")
								self.window_2.SetMinimumPaneSize(50)
								self.list_ctrl_2.InsertColumn(0, "Lista de usuarios", format=wx.LIST_FORMAT_LEFT, width=300)
								self.choice_1.SetSelection(0)
								self.window_5.SetMinimumPaneSize(20)
								self.window_10.SetMinimumPaneSize(20)
								self.window_9.SetMinimumPaneSize(20)
								self.window_6.SetMinimumPaneSize(20)
								self.window_4.SetMinimumPaneSize(20)
								self.window_3.SetMinimumPaneSize(20)
								self.window_1.SetMinimumPaneSize(240)
								self.list_ctrl_1.InsertColumn(0, "Nombre del archivo", format=wx.LIST_FORMAT_LEFT, width=559)
								self.list_ctrl_1.InsertColumn(1, u"Tama\u00f1o (MB)", format=wx.LIST_FORMAT_LEFT, width=250)
								self.list_ctrl_1.InsertColumn(3, "Privacidad", format=wx.LIST_FORMAT_LEFT, width=150)
								self.window_8.SetMinimumPaneSize(650)
								self.window_7.SetMinimumPaneSize(30)

				def __do_layout(self):
								# crear diseño
								sizer_1 = wx.BoxSizer(wx.VERTICAL)
								sizer_14 = wx.BoxSizer(wx.HORIZONTAL)
								sizer_16 = wx.BoxSizer(wx.HORIZONTAL)
								sizer_18 = wx.BoxSizer(wx.HORIZONTAL)
								sizer_17 = wx.BoxSizer(wx.HORIZONTAL)
								sizer_15 = wx.BoxSizer(wx.HORIZONTAL)
								sizer_2 = wx.BoxSizer(wx.VERTICAL)
								sizer_4 = wx.BoxSizer(wx.HORIZONTAL)
								sizer_5 = wx.BoxSizer(wx.VERTICAL)
								sizer_9 = wx.BoxSizer(wx.HORIZONTAL)
								grid_sizer_1 = wx.GridSizer(4, 3, 0, 0)
								sizer_10 = wx.BoxSizer(wx.HORIZONTAL)
								sizer_20 = wx.BoxSizer(wx.HORIZONTAL)
								sizer_22 = wx.BoxSizer(wx.HORIZONTAL)
								sizer_21 = wx.BoxSizer(wx.HORIZONTAL)
								sizer_19 = wx.BoxSizer(wx.HORIZONTAL)
								sizer_6 = wx.BoxSizer(wx.HORIZONTAL)
								sizer_8 = wx.BoxSizer(wx.HORIZONTAL)
								sizer_7 = wx.BoxSizer(wx.HORIZONTAL)
								sizer_13 = wx.BoxSizer(wx.HORIZONTAL)
								sizer_3 = wx.BoxSizer(wx.VERTICAL)
								sizer_11 = wx.BoxSizer(wx.HORIZONTAL)
								sizer_12 = wx.BoxSizer(wx.HORIZONTAL)
								sizer_12.Add(self.rich_text_ctrl_1, 1, wx.EXPAND, 0)
								self.window_2_pane_1.SetSizer(sizer_12)
								sizer_11.Add(self.text_ctrl_1, 1, wx.EXPAND, 0)
								self.window_2_pane_2.SetSizer(sizer_11)
								self.window_2.SplitHorizontally(self.window_2_pane_1, self.window_2_pane_2, 379)
								sizer_3.Add(self.window_2, 1, wx.EXPAND, 0)
								self.window_1_pane_1.SetSizer(sizer_3)
								sizer_13.Add(self.list_ctrl_2, 1, wx.EXPAND, 0)
								self.window_3_pane_1.SetSizer(sizer_13)
								label_1 = wx.StaticText(self.window_5_pane_1, wx.ID_ANY, "   Modo de chat:")
								sizer_7.Add(label_1, 1, wx.ALIGN_CENTER, 0)
								self.window_5_pane_1.SetSizer(sizer_7)
								sizer_8.Add(self.choice_1, 1, wx.EXPAND, 0)
								self.window_5_pane_2.SetSizer(sizer_8)
								self.window_5.SplitVertically(self.window_5_pane_1, self.window_5_pane_2, 118)
								sizer_6.Add(self.window_5, 1, wx.EXPAND, 0)
								self.window_4_pane_1.SetSizer(sizer_6)
								sizer_19.Add(self.button_1, 1, wx.ALIGN_CENTER | wx.EXPAND, 0)
								self.window_9_pane_1.SetSizer(sizer_19)
								sizer_21.Add(self.button_15, 0, 0, 0)
								self.window_10_pane_1.SetSizer(sizer_21)
								sizer_22.Add(self.button_16, 0, 0, 0)
								self.window_10_pane_2.SetSizer(sizer_22)
								self.window_10.SplitVertically(self.window_10_pane_1, self.window_10_pane_2, 39)
								sizer_20.Add(self.window_10, 1, wx.EXPAND, 0)
								self.window_9_pane_2.SetSizer(sizer_20)
								self.window_9.SplitVertically(self.window_9_pane_1, self.window_9_pane_2, 203)
								sizer_10.Add(self.window_9, 1, wx.EXPAND, 0)
								self.window_6_pane_1.SetSizer(sizer_10)
								grid_sizer_1.Add(self.button_2, 1, wx.EXPAND, 0)
								grid_sizer_1.Add(self.button_3, 1, wx.EXPAND, 0)
								grid_sizer_1.Add(self.button_8, 0, wx.EXPAND, 0)
								grid_sizer_1.Add(self.button_4, 1, wx.EXPAND, 0)
								grid_sizer_1.Add(self.button_5, 0, wx.EXPAND, 0)
								grid_sizer_1.Add(self.button_9, 0, wx.EXPAND, 0)
								grid_sizer_1.Add(self.button_6, 1, wx.EXPAND, 0)
								grid_sizer_1.Add(self.button_7, 1, wx.EXPAND, 0)
								grid_sizer_1.Add(self.button_10, 0, wx.EXPAND, 0)
								grid_sizer_1.Add(self.button_11, 0, wx.EXPAND, 0)
								grid_sizer_1.Add(self.button_12, 0, wx.EXPAND, 0)
								grid_sizer_1.Add(self.button_13, 0, wx.EXPAND, 0)
								self.window_6_pane_2.SetSizer(grid_sizer_1)
								self.window_6.SplitHorizontally(self.window_6_pane_1, self.window_6_pane_2, 23)
								sizer_9.Add(self.window_6, 1, wx.EXPAND, 0)
								self.window_4_pane_2.SetSizer(sizer_9)
								self.window_4.SplitHorizontally(self.window_4_pane_1, self.window_4_pane_2, 28)
								sizer_5.Add(self.window_4, 1, wx.EXPAND, 0)
								self.window_3_pane_2.SetSizer(sizer_5)
								self.window_3.SplitHorizontally(self.window_3_pane_1, self.window_3_pane_2, 266)
								sizer_4.Add(self.window_3, 1, wx.EXPAND, 0)
								self.window_1_pane_2.SetSizer(sizer_4)
								self.window_1.SplitVertically(self.window_1_pane_1, self.window_1_pane_2, 651)
								sizer_2.Add(self.window_1, 1, wx.EXPAND, 0)
								self.notebook_1_pane_1.SetSizer(sizer_2)
								sizer_15.Add(self.list_ctrl_1, 1, wx.ALIGN_CENTER | wx.EXPAND, 0)
								self.window_7_pane_1.SetSizer(sizer_15)
								sizer_17.Add(self.button_14, 1, wx.EXPAND, 0)
								self.window_8_pane_1.SetSizer(sizer_17)
								label_2 = wx.StaticText(self.window_8_pane_2, wx.ID_ANY, "Estado: listo para descargar", style=wx.ALIGN_CENTER)
								sizer_18.Add(label_2, 1, wx.ALIGN_CENTER, 0)
								self.window_8_pane_2.SetSizer(sizer_18)
								self.window_8.SplitVertically(self.window_8_pane_1, self.window_8_pane_2, 401)
								sizer_16.Add(self.window_8, 1, wx.EXPAND, 0)
								self.window_7_pane_2.SetSizer(sizer_16)
								self.window_7.SplitHorizontally(self.window_7_pane_1, self.window_7_pane_2, 409)
								sizer_14.Add(self.window_7, 1, wx.EXPAND, 0)
								self.notebook_1_Archivoscompartidos.SetSizer(sizer_14)
								self.notebook_1.AddPage(self.notebook_1_pane_1, "  Chat  ")
								self.notebook_1.AddPage(self.notebook_1_Archivoscompartidos, "  Archivos compartidos  ")
								sizer_1.Add(self.notebook_1, 1, wx.EXPAND, 0)
								self.SetSizer(sizer_1)
								self.Layout()

				def __carpeta_Cliente(self):
					try:
						os.mkdir(CarpetaCliente)
						print("[I] Carpeta del cliente creada exitosamente.")
					except:
						shutil.rmtree(CarpetaCliente, ignore_errors=True)
						os.mkdir(CarpetaCliente)
						print("[I] Limpiando carpeta del cliente.")

				def __thread_listaSdatos(self):	#NO ABRE NINGÚN SOCKET!
					while True:
						enviarMensaje(self.sock,"","",2)	#solicitar cada .8S la lista de peers
						enviarMensaje(self.sock,"","",1)	#solicitar cada .8S la lista de archivos
						for entr in range(self.list_ctrl_1.GetItemCount()):
							self.listaArchivosUI.append(self.list_ctrl_1.GetItemText(entr))
						for i in self.listaArchivos:
							if(i not in self.listaArchivosUI):
								if(self.listaArchivos[i][0] in ["global",NAME]):	#No mostrar otros archivos que no sean
									index = self.list_ctrl_1.InsertItem(999,i)	#globales o para este cliente
									wx.CallAfter(self.list_ctrl_1.SetItem,index, 1, str(round(self.listaArchivos[i][1],3)))
									wx.CallAfter(self.list_ctrl_1.SetItem,index, 2, self.listaArchivos[i][0])
						for entrada in self.listaPeers:		#si no está la entrada en la lista que tiene la UI
							if(entrada not in self.listaPeersUI and entrada!=NAME):	#agregarlo con append pero no agregar al cliente mismo
								wx.CallAfter(self.list_ctrl_2.InsertItem,999, entrada)
								self.listaPeersUI.append(entrada)
						for entrada in self.listaPeersUI:		#si hay una entrada que no esté en la lista de peers
							if(entrada not in self.listaPeers):	#eliminarla de la lista de peers de la UI
								for entr in range(self.list_ctrl_2.GetItemCount()):	#esto sucede si alguien se desconecta
									if(self.list_ctrl_2.GetItemText(entr)==entrada):
										wx.CallAfter(self.list_ctrl_2.DeleteItem,entr)
						time.sleep(0.7)

				def __thread_recepcionDatos(self):
					while True:
						datillos = recvall(self.sock,1)
						if(datillos):
							now = datetime.datetime.now()
							if(datillos[0]=="msj"):
								if(datillos[3]=="global"):
									label = "["+datillos[1] +" @ " + str(now.hour)+":"+\
											str(now.minute)+":"+str(now.second)+ "]: "\
											+datillos[2]
									wx.CallAfter(self.rich_text_ctrl_1.AppendText,label)
									wx.CallAfter(self.rich_text_ctrl_1.Newline)
									wx.CallAfter(self.rich_text_ctrl_1.ScrollPages,1)
									print("MSJ_PUBLICO",datillos)
								elif(datillos[3]==NAME):
									label = "[PRIVADO de "+datillos[1] +" @ " + str(now.hour)+":"+\
											str(now.minute)+":"+str(now.second)+ "]: "\
											+datillos[2]
									wx.CallAfter(self.rich_text_ctrl_1.AppendText,label)
									wx.CallAfter(self.rich_text_ctrl_1.Newline)
									wx.CallAfter(self.rich_text_ctrl_1.ScrollPages,1)
									print("MSJ_PRIV",datillos)
							elif(datillos[0]=="listaClientes"):
									self.listaPeers = datillos[1]
							elif(datillos[0]=="listaArchivos"):
									self.listaArchivos = dict(datillos[1])
							elif(datillos[0]=="archivo"):
									print("recibido el archivo",datillos[2])
						else:
							break

				def listaPeerSeleccionadoEVN(self,event):	#se ejecuta cada que se toca un ítem de la lista de peers
								self.peerSeleccionado = self.list_ctrl_2.GetItemText(self.list_ctrl_2.GetFocusedItem())	#establecer peer seleccionado
								self.privacidad = "privado"	#cambiar tipo de privacidad a privado
								self.choice_1.SetSelection(1)	#cambiar el widget de selección de "global" a "privado"

				def ENTER_PRESS(self, event):  # wxGlade: ventPrincipal.<event_handler>
								now = datetime.datetime.now()
								if(self.text_ctrl_1.GetValue()!=""):	#si no es vacio el mensaje
									if(self.privacidad=="global"):
										self.rich_text_ctrl_1.BeginBold()
										self.rich_text_ctrl_1.BeginTextColour((204, 223, 255))
										self.rich_text_ctrl_1.WriteText("[TÚ->todos @ " + str(now.hour)+":"+\
																		str(now.minute)+":"+str(now.second)+ "]: "\
																		 + self.text_ctrl_1.GetValue())
										self.rich_text_ctrl_1.EndTextColour()
										self.rich_text_ctrl_1.EndBold()
										enviarMensaje(self.sock,self.text_ctrl_1.GetValue())
									else:
										self.rich_text_ctrl_1.BeginBold()
										self.rich_text_ctrl_1.WriteText("[TÚ->"+self.peerSeleccionado+" @ " + str(now.hour)+":"+\
																	str(now.minute)+":"+str(now.second)+ "]: "\
																	 + self.text_ctrl_1.GetValue())
										self.rich_text_ctrl_1.EndBold()
										enviarMensaje(self.sock,self.text_ctrl_1.GetValue(),self.peerSeleccionado)
									self.rich_text_ctrl_1.Newline()
									self.rich_text_ctrl_1.ScrollPages(1)
									self.text_ctrl_1.SetValue("")

				def MODO_CHAT(self, event):  # wxGlade: ventPrincipal.<event_handler>
								self.privacidad = self.choice_1.GetString(self.choice_1.GetCurrentSelection())
								if(len(self.listaPeers)<2):
									self.choice_1.SetSelection(0)
									self.rich_text_ctrl_1.AppendText("⚠️ Estás solo en la sala. Cambiando a modo global...")
									self.rich_text_ctrl_1.Newline()
									self.rich_text_ctrl_1.ScrollPages(1)
								elif(self.peerSeleccionado==""):
									self.choice_1.SetSelection(0)
									self.rich_text_ctrl_1.AppendText("⚠️ No ha seleccionado un cliente para mensaje privado. Cambiando a modo global...")
									self.rich_text_ctrl_1.Newline()
									self.rich_text_ctrl_1.ScrollPages(1)
								print(self.choice_1.GetString(self.choice_1.GetCurrentSelection()))

				def BTN_ENVIAR(self, event):	self.ENTER_PRESS(event)	#hace lo mismo que si se presionara enter

				def ENV_IMG(self, event):  # wxGlade: ventPrincipal.<event_handler>
								self.rich_text_ctrl_1.WriteText("satanic peppa")
								aimage = wx.Image('image.jpg', wx.BITMAP_TYPE_ANY)
								aimage = aimage.Scale(200, 200, wx.IMAGE_QUALITY_HIGH)
								self.rich_text_ctrl_1.AddImage(aimage)
								self.rich_text_ctrl_1.AppendText("\r")
								self.rich_text_ctrl_1.ScrollPages(1)

				def ENV_ARCH(self, event):  # wxGlade: ventPrincipal.<event_handler>
								openFileDialog = wx.FileDialog(self, "Subir archivos", "~", "", "Archivos (*.*)|*.*",wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
								if openFileDialog.ShowModal() == wx.ID_CANCEL:	pass
								else:
									if(self.choice_1.GetString(self.choice_1.GetCurrentSelection())!=""):
										if(self.choice_1.GetString(self.choice_1.GetCurrentSelection())=="global"):
											Thread(target=enviarArchivo,args=(openFileDialog.GetPath(),"global")).start()
										else:	#Meter el envío en un hilo para no bloquear la UI mientras se lea/envíe el archivo
											Thread(target=enviarArchivo,args=(openFileDialog.GetPath(),self.peerSeleccionado)).start()

				def emoji_smiley1(self, event):	self.text_ctrl_1.AppendText(u"\U0001f600")
				def emoji_litaf1(self, event):	self.text_ctrl_1.AppendText(u"\U0001f602")
				def emoji_litaf2(self, event):	self.text_ctrl_1.AppendText(u"\U0001f525")
				def emoji_angery(self, event):	self.text_ctrl_1.AppendText(u"\U0001f621")
				def emoji_litaf3(self, event):	self.text_ctrl_1.AppendText(u"\U0001f44c\U0001f3fc")
				def emoji_litaf4(self, event):	self.text_ctrl_1.AppendText(u"\U0001f4af")
				def emoji_sad(self, event):	self.text_ctrl_1.AppendText(u"\U0001f61e")
				def emoji_B(self, event):	self.text_ctrl_1.AppendText(u"\U0001f171\ufe0f")
				def emoji_eggplant(self, event):	self.text_ctrl_1.AppendText(u"\U0001f346")
				def emoji_smiley(self, event):	self.text_ctrl_1.AppendText(u"\U0001f643")
				def emoji_thinking(self, event):	self.text_ctrl_1.AppendText(u"\U0001f914")
				def emoji_poop(self, event):	self.text_ctrl_1.AppendText(u"\U0001f4a9")
				def listCtrl_seleccion(self, event):
								print(self.list_ctrl_1.GetItemText(self.list_ctrl_1.GetFocusedItem(),0))
				def btn_descargar(self, event):  # wxGlade: ventPrincipal.<event_handler>
								archsel = self.list_ctrl_1.GetItemText(self.list_ctrl_1.GetFocusedItem())
								if(archsel!=""):
									Thread(target=(solicitarArchivo),args=(archsel,)).start()
				def tab_update(self, event):  # wxGlade: ventPrincipal.<event_handler>
								pass

				def tab_update2(self, event):  # wxGlade: ventPrincipal.<event_handler>
								print("Event handler 'tab_update2' not implemented!")
								event.Skip()

def serverProbe():		#esta función se encarga de verificar que el servidor esté vivo
	for i in range(0,5):
			s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			try:
					s.connect((HOST, PORT))
					print(s.getpeername()," verificando conectividad... ",i+1,sep="",end="\r")
					s.close()
					time.sleep(.2)
			except:
					print("error al conectar al servidor")
					sys.exit(0)

def disponibilidadNick():
	if(len(sys.argv)>1):
		if(sys.argv[1]!=""):
			global NAME
			NAME = sys.argv[1]
		if(len(sys.argv)>2):
			if(sys.argv[2]!="" and sys.argv[2]!="localhost"):
				global HOST
				HOST = sys.argv[2]
				print("utilizando de host, [",HOST,"]",sep="")
	serverProbe()
	sock = socket.socket()
	try:	sock.connect((HOST,PORT))
	except:	sys.exit(0)
	try:
		sock.send(pickle.dumps(["login",NAME,"",""]))
	except:	sys.exit(0)
	response = sock.recv(256)
	if response.decode("utf-8")=="ok":
		print("\nLOGIN EXITOSO")
		sock.close()
	else:
		print("\nNombre de usuario existente, elija otro y ejecute nuevamente")
		sock.close()
		sys.exit(0)

class MyApp(wx.App):
				def OnInit(self):
								self.frame = ventPrincipal(None, wx.ID_ANY, "")
								self.SetTopWindow(self.frame)
								self.frame.Show()
								return True
if __name__ == "__main__":
		try:
				disponibilidadNick()
				app = MyApp(0)
				app.MainLoop()
		except:
				sys.exit(0)
				sys.exit(0)
