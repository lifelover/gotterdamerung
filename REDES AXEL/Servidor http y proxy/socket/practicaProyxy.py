import socket
from threading import Thread
from time import sleep
import itertools

def recvall(sock):		# Funcion que se encarga de recibir datos a traves de un socket sin importar su tamaño
    BUFF_SIZE = 2**12	# Chunks de 4KB
    data = b''			# data = arreglo de bytes
    while (True):			# loop infinito
        part = sock.recv(BUFF_SIZE)	#recibir de 8KB
        data += part				#anexar los datos
        if (len(part) < BUFF_SIZE): #Terminar cuando sea 0 o ya no hayan archivos
            break						#-->break
    return data

def ProxyPassthrough(datos,serv,port):
	s = socket.socket()         # crear un objeto de socket
	try:	s.connect((serv, port))	#Tratar de conectar
	except:	
		print("[E] No se ha podido conectar al servidor!")
		return None
	try:
		s.sendall(datos)
		print("[INFO] Reenviando datos...")
		sleep(.01)
	except:	print("[E]	error al conectar...")
	while True:
		try:
			data = recvall(s)
			if data:
				try:
					return data
				except: print("[E]	Error al recibir.")
				break
			else: #if not data
				raise ValueError('Cliente desconectado')
		except:     #si el cliente cierra la conexión
			print("[E] desconectado: ")
			break
		finally:
			s.close()
			print("[INFO] Cerrando socket...")

def wachadorDeConexiones(client,address,servidorDestino):
		while True:
			try:
				data = recvall(client)     
				if data:
					print("[INFO] Recibidos datos de:",address)
					datillos = ProxyPassthrough(data,servidorDestino[0],servidorDestino[1])
					try:	
						print("[INFO]",servidorDestino,"---> enviando datos a --->",address)
						client.send(datillos)
					except Exception as e:	print(">>>",e)
				else: #if not data
					raise ValueError('Cliente desconectado')
			except:     #si el cliente cierra la conexión
				print("[E] error de recepción ",client.getpeername(),sep="")
			finally:
				print("[INFO] desconectado: ",client.getpeername(),sep="")
				client.close()                          #cerrar socket
				break                    

def ProgramaChido():
	listaServidores = [('localhost',6969),('localhost',9696),('localhost',1234)]
	round_robin = itertools.cycle(listaServidores)
	host = socket.gethostname()
	port = 6666
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #TCP
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #REUSAR ADDR
	#s.settimeout(1)
	try:	s.bind(('',port))
	except Exception as e:	print(">>",e)
	s.listen(port)
	while True:
		try:
			client, address = s.accept()    #aceptar conexiones
			print("[INFO] recibida conexión desde:",client.getpeername())
			Thread(target = wachadorDeConexiones, args = (client,address,next(round_robin))).start() #thread
		except Exception as e:	
			print(">>",e)
			#client.close()
if __name__ == '__main__':
	try:	
		while True:	ProgramaChido()
	except Exception as e:	print(">",e)