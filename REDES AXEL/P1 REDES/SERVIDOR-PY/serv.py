import socket      ## El servidor consta de 3 partes, una que se encarga de recibir archivos de parte del cliente (loop3), otra
import os,secrets  ## que se encarga de recibir una lista de archivos que el cliente desea descargar y que a su vez sube los archivos
import pickle      ## (loop2) y un servicio de echo que envía una lista de archivos cuando el cliente hace un ping al server (loop1)
import zipfile     ## getEverything regresa una lista con la ruta del archivo/directorio, tamaño y tipo (arch/dir)
import sys         ## CompressPendingFiles comprime una lista de archivos y subfile lo sube a determinado host
from multiprocessing import Process  #libreria para multiproceso
sys.tracebacklimit = 0
CarpetaDelServidor = "Archivos_Servidor" #Nombre global de la carpeta de descargas

def recvall(sock):		# Funcion que se encarga de recibir datos a traves de un socket sin importar su tamaño
    BUFF_SIZE = 8192	# Chunks de 8KB
    data = b''			# data = arreglo de bytes
    while (True):			# loop infinito
        part = sock.recv(BUFF_SIZE)	#recibir de 8KB
        data += part				#anexar los datos
        if (len(part) < BUFF_SIZE): #Terminar cuando sea 0 o ya no hayan archivos
            break						#-->break
    return data

def in_directory(file, directory):
    directory = os.path.join(os.path.realpath(directory), '')
    file = os.path.realpath(file)
    return os.path.commonprefix([file, directory]) == directory

def getSize(filename):
    st = os.stat(filename)  #probar si esta el archivo
    return st.st_size       #checar tam

def getDirsize(start_path): #para obtener el tamaño de un directorio
        total_size = 0
        for dirpath, dirnames, filenames in os.walk(start_path):
            for f in filenames:
                fp = os.path.join(dirpath, f)
                total_size += os.path.getsize(fp)
        return total_size

def getEverything(start_path):  #Funcion que guarda en una lista la ruta, tamaño y tipo de archivo/directorio
    listachida=[]
    for root, dirs, files in os.walk(start_path):
        for file in files:
            arch_end=os.path.join(root, file)
            if(getSize(arch_end)>=1000*1000):
                listachida.append((os.path.abspath(arch_end),str(round((getSize(arch_end)/(1000*1000)),3))+" MB","Archivo"))
            else:
                listachida.append((os.path.abspath(arch_end),str(round((getSize(arch_end)/(1000)),3))+" kB","Archivo"))
        for dirx in dirs:
            dir_end=os.path.join(root, dirx)
            if(getDirsize(dir_end)>=1000*1000):
                listachida.append((os.path.abspath(dir_end),str(round((getDirsize(dir_end)/(1000*1000)),3))+" MB","Directorio"))
            else:
                listachida.append((os.path.abspath(dir_end),str(round((getDirsize(dir_end)/(1000)),3))+" kB","Directorio"))
    return listachida

try:                                        #En esta parte, se crea la carpeta del servidor donde se guardan las cosas
    os.mkdir(CarpetaDelServidor)
    print("[inf] Carpeta de archivos creada")
except:
    print("[inf] Usando carpeta de servidor existente")

def subfile(hostK,portK,fileK):
	s = socket.socket()				#crear un socket
	host = socket.gethostname()		#obtener host local
	try:
		s.connect((host, portK))
	except:
		raise ValueError('\n[!/downloader] Cliente está offline')
		os.remove(fileK)
	f = open(fileK,'rb')
	print ("[uploader] transferencia en curso...  (",(getSize(fileK)/(1000**2))," MBytes)",sep='')
	l = f.read(4096)				#leer los primeros 4096 bytes del archivo a subir
	transferidos = 0				#inicializar variable de conteo
	while (l):
	    transferidos += s.send(l)			#s.send(l) regresa la cantidad de bytes enviados
	    print("[uploader] transfiriendo... ",round(((transferidos*100)/getSize(fileK)),2)," % \r", sep=' ', end='', flush=True)
	    l = f.read(4096)					#leer los siguientes 4096 bytes
	f.close()								#cerrar descriptor de archivo
	print("\n[OK/uploader]",fileK,(getSize(fileK))/(1000**2),"MBytes")
	os.remove(fileK)						#eliminar archivo temporal
	s.shutdown(socket.SHUT_WR)				#anunciar cierre de conexiones al socket
	s.close()								#cerrar el socket

def CompressPendingFiles(listaArchDir,pto):		#Funcion para comprimir y subir archivos al cliente
	nomrand = secrets.token_hex(10) + "_" + "PendingUpload.zip" #Crear nombre aleatorio
	tempz = zipfile.ZipFile(nomrand,"w") #zip a enviar
	home = os.path.expanduser('~')+"/" #dir del usuario-UNIX!
	cont=0								#para contar archivos (innecesario)
	for i in listaArchDir:						#iterar sobre archivos superficiales en la lista	
		if(os.path.isfile(i)):					#si es archivo, agregar 
			cont=cont+1
			filePath=os.path.relpath(os.path.join(home, i), ".")	#obtener ruta del archivo respecto al home del usuario
			#print("[ok:",cont,"]",filePath)
			tempz.write(filePath)								#meter en el zip (tempz)
		else:
			for root, dirs, files in os.walk(i):	#explorar cada carpeta en profundidad
				for f in files:						#solo agregar archivos
					cont=cont+1						
					xFile = os.path.join(root, f) #agregar cada archivo encontrado con su ruta
					filePath=os.path.relpath(os.path.join(home, xFile), ".")
					#print("[ok:",cont,"]",filePath)
					tempz.write(filePath)					#meter en el zip
	tempz.close()											#cerrar descriptor del zip
	subfile("127.0.0.1",pto,nomrand)						#subir archivo al cliente
                                                                                        
def loop1():				#Loop que espera un token para enviar la lista de archivos
	HOST = "localhost"      #Host
	PORT = 50025            #Puerto
	s = socket.socket()
	try:
		s.bind((HOST, PORT))	#Bindear al puerto para socket de servidor
		print("[ok/sendlist] Envío de información corriendo en ",HOST,":",PORT,sep="")
	except socket.error:
		print("[!/sendlist] ERROR! (bind:",PORT,"), ejecute #fuser -k ",PORT,"/tcp",sep="")
	s.listen(5)
	while True:	#Serializar lista de archivos y directorios en la CarpetaDelServidor
		data_string = pickle.dumps(getEverything(CarpetaDelServidor))			
		try:
			conn, addr = s.accept()
		except:
			print("\n[!/sendlist] KeyboardInterrupt")
			sys.exit(0)
		data_string = pickle.dumps(getEverything(CarpetaDelServidor))	#Serializar de nuevo por si acaso	
		while(True):
			try:
				data = conn.recv(256) #512 para que el cliente "toque" al servidor, no se necesita mucha memoria
			except:
				print("[!/sendlist] conexión reiniciada por el cliente")            	
			if(not data): 
				break
			try:
				totbytes = conn.send(data_string)	#Enviar la lista de archivos en el servidor
				print("[sendlist] ",(totbytes/1000),"KB/",(len(data_string)/1000)," KB en la lista enviados a ",addr,sep="")
			except:			#En caso de que el cliente falle al recibir, revivir el socket (connection reset by peer)
				print("[!/sendlist] reviviendo proceso de envío de información")
				Process(target=loop1).start()
		conn.close()

def loop2():				#Loop que espera una lista de archivos para enviar datos al cliente
	HOST = "localhost"      #Host 
	PORT = 50026            #Puerto en el cual se recibe la lista
	PORT_UP_CLIENT = 44444	#Puerto en el cual el servidor va enviar archivos al cliente
	s = socket.socket()
	datax = []
	try:
		s.bind((HOST, PORT))
		print("[ ok/sender ] Solicitud de envío de archivos corriendo en ",HOST,":",PORT,sep="")
		print("[ ok/sender ] Subida de archivos utilizando ",HOST,":",PORT_UP_CLIENT,sep="")
	except socket.error:
		print("[!/sender] ERROR! (bind:",PORT,"), ejecute #fuser -k ",PORT,"/tcp",sep="")
	s.listen(5)
	while True:
		conn, addr = s.accept()		#Esperar una conexión del cliente
		print("[sender] ",addr,"está en proceso de descargar archivos")
		datax = pickle.loads(recvall(conn))	#Des-Serializar la lista que el cliente está mandando en el socket (recvall la recibe)
		CompressPendingFiles(datax,PORT_UP_CLIENT)	#Mandar la lista para comprimir los archivos que esten en la ruta
		conn.close()								#Cerrar el descriptor

def loop3():					# Loop(PRINCIPAL) para recibir archivos
	s = socket.socket()         # Socket para crear archivos
	host = socket.gethostname() # localhost
	puertoarch = 12345          # Puerto para transferencia de archivos
	filename = "SrvTMPFile.zip" # 'Seed' para el nombre de archivo temporal
	curpath = os.getcwd() #Obtener la ruta actual donde se esta ejecutando el script
	try:
		s.bind((host, puertoarch))        # Ligar a puertoarch
		print("[  ok/main  ] Recepción corriendo en ",host,":",puertoarch,sep="")
	except:
		print("[!/main] ERROR! (bind:",PORT,"), ejecute #fuser -k ",PORT,"/tcp",sep="")
		sys.exit(0)
	s.listen(5)                 #Backlog de 5 conexioness
	while True:                 #Ciclo infinito
		nomrand = secrets.token_hex(10) + "_" + filename
		transferidos = 0
		f = open(nomrand,'wb')
		print("[main] temp",nomrand,"creado")
		try: 
			c, addr = s.accept()     #Aceptar conexiones del cliente
		except:
			print("\n[!/main] saliendo limpiamente")
			os.remove(nomrand)
			f.close()
			s.close()
			sys.exit(0)
		print("[main] recibiendo desde: ",addr)
		l = c.recv(1024)
		while (l):
			transferidos += f.write(l)
			print("[main] recibiendo... ",transferidos/(1000**2)," MBytes \r", sep=' ', end='', flush=True)
			try:
				l = c.recv(1024)
			except KeyboardInterrupt:
				print("[!/main] Transferencia incompleta. Saliendo limpiamente...")
				os.remove(nomrand)
				f.close()
				c.close()
				s.close()
				sys.exit(0)
		f.close()
		tempz = zipfile.ZipFile(nomrand, 'r')
		print("[main] recibidos",round(((getSize(nomrand))/(1000*1000)),3),"MBytes en",len(tempz.namelist()),"archivos")
		os.chdir(CarpetaDelServidor)
		tempz.extractall(os.getcwd())
		tempz.close()
		os.chdir(curpath)
		os.remove(nomrand)
		print("[main] temp",nomrand,"eliminado")
		c.close()           

Process(target=loop1).start()	#Iniciar servicio de escucha para enviar la lista de archivos actualizada
Process(target=loop2).start()	#Iniciar servicio de escucha para recibir la lista de archivos que desean descargarse y enviarla
Process(target=loop3).start()	#Iniciar servicio de recepcion de archivos