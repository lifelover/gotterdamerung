import socket
import pickle
import os
import sys
def recvall(sock,unpickle):
    BUFF_SIZE = 8192	# 8 KB
    data = b''			# data = arreglo de bytes
    while True:			# loop infinito
        part = sock.recv(BUFF_SIZE)	#recibir de 8KB
        data += part				#anexar los datos
        if (len(part) < BUFF_SIZE):
            break
    if unpickle==1:
    	return pickle.loads(data)
    else:
    	return data

def getRemoteListOfFiles(host,port):
	HOST = host
	PORT = port
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	try:
		s.connect((HOST, PORT))
	except:
		print("[!] Error fatal al obtener lista de archivos...")
		sys.exit(0)
	b = ("123").encode()
	print(s.send(b),"KB ping")
	datillos = recvall(s,1)
	for element in datillos:
		print(sorted(datillos),"\n")
	s.shutdown(socket.SHUT_WR)				#anunciar cierre de conexiones al socket
	s.close()
	
getRemoteListOfFiles("127.0.0.1",50025)
