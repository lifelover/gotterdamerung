import socket, pickle
def recvall(sock,unpickle=None):        # Funcion que se encarga de recibir datos a traves de un socket sin importar su tamaño
    BUFF_SIZE = 512    # Chunks de 512B
    data = b''          # data = arreglo de bytes
    while (True):           # loop infinito
        part = sock.recv(BUFF_SIZE) #recibir de 16KB en 16KB
        data += part                #anexar los datos
        if (len(part) < BUFF_SIZE): #Terminar cuando sea 0 o ya no hayan archivos
            break                       #-->break
    if(unpickle):
        return pickle.loads(data)
    else:
        return data

def conectar():
    HOST = "localhost"
    PORT = 666
    s = socket.socket()
    try:
        s.connect((HOST, PORT))
    except:
        return False    #En caso de fallo, regresar false    
    s.send(("db_local").encode())   #Solicitar bd
    catalogo = recvall(s)         #El servidor enviará la lista serializada de vuelta, aquí se guarda ya des-serializada
    print(catalogo)
    s.shutdown(socket.SHUT_WR)              #anunciar cierre de conexiones al socket
    s.close()    

print(conectar())