#include <NewPing.h>

#define TRIGGER_PIN  2
#define ECHO_PIN     3
#define MAX_DISTANCE 200

int PIN_MEDICION = 7;
int last_button_state = 0;
int button_state = 0;
const int CANT_POBLACION = 6;
double DETENER_ANTES_DE = 3;
double distanciaInicial = 0;
double* y_esc;
float distancias[CANT_POBLACION];
double realentiza = 0;
double x0_bias[CANT_POBLACION];
double x1[CANT_POBLACION];
double sal_deseada[CANT_POBLACION];
double error_esc[CANT_POBLACION];
double w0[CANT_POBLACION];
double w1[CANT_POBLACION];
double distLimite = 0.30;

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);

float randddom() {
  return ( random(200) / 100.0 ) - 1;
}
double leerDistancia() { return medirDistanciaTiempoReal(); }

double* realizarCalculos(double x0_bias[], double w0[], double x1[], double w1[]) {
  y_esc = (double*)malloc(sizeof(double) * CANT_POBLACION );
  double res = 0.0;
  for (int i = 0; i < CANT_POBLACION; ++i)
  {
    res = x0_bias[i] * w0[i] + x1[i] * w1[i];
    Serial.print("| x0(bias): ");
    Serial.print(x0_bias[i]);
    Serial.print("| w0: ");
    Serial.print(w0[i]);
    Serial.print("| x1: ");
    Serial.print(x1[i]);
    Serial.print("| w1: ");
    Serial.print(w1[i]);
    Serial.print("| NET = ");
    Serial.print(res);
    if(res > 0) 
      y_esc[i] = 1;
    else
      y_esc[i] = 0;
    Serial.print(" y_esc = ");
    Serial.println(y_esc[i]);
  }
  Serial.println("__________");
  return y_esc;
}

void calcularDistancias()
{
  int cont = 0;
  while(1)
  {
    button_state = digitalRead(PIN_MEDICION);
    if(cont==7)
    {
      Serial.println("Ha presionado por septima vez, empezando a caminar...");
      break;  //cuando sea la medicion 7, que se siga
    }
    if(button_state != last_button_state) {
      if(button_state ==HIGH)
      {
        distancias[cont]= medirDistanciaTiempoReal();
        Serial.print("Medicion ")
        Serial.print(cont);
        Serial.println("/6")
        cont++; //se van a tomar 6 lecturas
      }
    }
    last_button_state = button_state;
  }
}

void setup() {
  // put your setup code here, to run once:
  randomSeed(analogRead(0));
  pinMode(PIN_MEDICION,INPUT);
  Serial.begin(9600);
  double decremento;
  double distanciaMax = 0;
  calcularDistancias();
  distanciaInicial = distancias[0];
//  distanciaInicial = leerDistancia();
//  distancias[0] = distanciaInicial;
//  if(distanciaInicial >= 1) {
//    decremento = distanciaInicial / CANT_POBLACION;
//    for(int i = 1; i < CANT_POBLACION; i++) {
//      distancias[i] = distanciaInicial-decremento*i;
//    }
//  }
  for(int i = 0; i< CANT_POBLACION; i++) {
    x0_bias[i] = 1;
  }
  //encontrar la distancia mas grande dentro de distancias
  for(int i = 0; i < CANT_POBLACION; i++) {
    distanciaMax = max(distanciaMax, distancias[i]);
    Serial.println("-------------");
    Serial.println(distanciaMax);
    Serial.println("-------------");
  }
  //poblar x1
  for (int i = 0; i < CANT_POBLACION; ++i) {
    x1[i] = (distancias[i] / distanciaMax);
    sal_deseada[i] = 0;
  }
  for (int i = CANT_POBLACION - DETENER_ANTES_DE; i < CANT_POBLACION; ++i) {
    sal_deseada[i] = 1;
  }
  int sumErr;
  while(true) 
  {
    sumErr = 0;
    double random_para_w0 = (double)randddom();
    for (int i = 0; i < CANT_POBLACION; ++i) {
      w0[i] = random_para_w0;
    }
    double random_para_w1 = (double)randddom();
    for (int i = 0; i < CANT_POBLACION; ++i) {
      w1[i] = random_para_w1;
    }
    y_esc = realizarCalculos(x0_bias,w0,x1,w1);
    for (int i = 0; i < CANT_POBLACION; ++i)
    {
      Serial.println("");
      Serial.print("[test] distancia");
      Serial.print(distancias[i]);
      error_esc[i] = abs(y_esc[i] - sal_deseada[i]);
      sumErr += error_esc[i];
    }
    Serial.println("");
    Serial.print("[test] SUMA: ");
    Serial.print(sumErr);
    Serial.println("");
    free(y_esc);
    if(sumErr == 0.0){
      Serial.print("OK!");
      break;
    }
  }
  realentiza = 0.0;
  for (int i = CANT_POBLACION - DETENER_ANTES_DE; i < CANT_POBLACION; ++i)
  {
    realentiza = max(realentiza, distancias[i]);
  }
  Serial.print("[SET] realentiza en: ");
  Serial.print(realentiza);
  Serial.print("[SET] distancias:  "); 
  for(int i = 0; i< CANT_POBLACION; i++) {
    Serial.print(distancias[i]);  
    Serial.print(" ") 
  }
  
  Serial.print("[SET] x1:   ");
  for(int i = 0; i< CANT_POBLACION; i++) {
    Serial.print(x1[i]);  
    Serial.print(" ") 
  }
  Serial.print("[SET] Y_esc:   "); 
 for(int i = 0; i< CANT_POBLACION; i++) {
    Serial.print(y_esc[i]);  
    Serial.print(" ") 
  }
  Serial.print("[SET] SalDeseada:  ");
  for(int i = 0; i< CANT_POBLACION; i++) {
    Serial.print(sal_deseada[i]); 
    Serial.print(" ")  
  }
  Serial.print("[SET] Error:     ");
  for(int i = 0; i< CANT_POBLACION; i++) {
    Serial.print(error_esc[i]);  
    Serial.print(" ") 
  }
  Serial.print("=");
  Serial.print(sumErr);
}


float medirDistanciaTiempoReal() {
  delay(50);
  float dist = sonar.ping_cm();
  if (dist == 0)
    dist = 5.0;
  dist = (float)(dist/100.0);
  return dist;
}
void loop() {
  // put your main code here, to run repeatedly:
	distanciaInicial = medirDistanciaTiempoReal();
  	if(distanciaInicial > distLimite) {
    	if(distanciaInicial>=realentiza){
        	analogWrite(4, (int)(255*0.90));
        	Serial.print("(caminando en modo normal) distancia restante: ");
        	Serial.println(distanciaInicial);
      	}else {
        	analogWrite(4, (int)(255*0.30));
        	Serial.print("(caminando en modo lento) distancia restante: ");
        	Serial.println(distanciaInicial);
      	}
  } else {
    	Serial.println("Carro detenido");
    	analogWrite(4, 0);
  }
 
}