#!/usr/bin/env python
#-*-coding:utf8;-*-
from random import randint,seed    #Para los números aleatorios
from sys import exit          #Para salir
from math import pow
def GenerarNum(): return randint(0,10000)  #Generar números random entre 0 y 10K
def I_num(num,m): return num // 10**m    #Obtener la parte izquierda, por ejemplo si es 255//(10**2) = 2.55, entero = 2
def D_num(num,m): return int(num % (10**m))  #Parte derecha, por ejemplo 255%(10**2)= 255%100 = 55
#12345 -> Inum -> 12345, m=2, 12345/100 = 123.45 -> 123 | 12 Inum=1 -> 1.2 -> 1
#12345 -> Dnum -> 12345%100  => 45 | 12 Dnum=1 -> 2 -> 2
def func(a,b):
  if (a < 10 or b < 10):   #Los casos base... (si a=10,b=1, entonces se usa el algoritmo)
    print("X:",a," Y:",b," regresando-> ",a*b,sep="")
    return a*b
  m = max(len(str(a)), len(str(b)))//2 #Longitud máxima de uno de los dos números, divida entre 2 y convertida a entero con // (es lo mismo que floor)
  ia = I_num(a,m)                   #obtener el lado izquierdo de a
  da = D_num(a,m)                   #obtener el lado derecho de a
  ib = I_num(b,m)                   #obtener el lado izquierdo de b
  db = D_num(b,m)                   #obtener el lado derecho de b
  print("X:",a,"Y:",b,"M:",m,"    |||...IA",ia,"DA",da,"IB",ib,"DB",db)
  iaib = func(ia,ib)                #multiplicar ia*db (la primera rama)
  dadb = func(da,db)                #multiplicar da*db (la tercera rama) 
  enmedio = func(ia + da, ib + db) -iaib -dadb  #obtener la segunda rama
  res = int(iaib*pow(10,2*m)+enmedio*pow(10,m)+dadb)  #multiplicar n*2 (se dividió entre 2 previamente)
  print("m=",m," | ",iaib,"*10^",2*m," + ",enmedio,"*10^",m," + ",dadb," = ",res,sep="")
  return res    #regresar el resultado

if __name__ == "__main__":
  try:    #En caso de que haya algún error (no debería de haber)
    while(True):    
      #a,b = 25132,213
      a,b = GenerarNum(),GenerarNum() #Generar dos valores aleatorios
      res = func(a,b)
      assert a*b==res,"Error en la comprobación..." #Comprobar que la multiplicación sea correcta
      print("Utilizando valores: [ ",a," * ",b," = ",res," ] ",sep="")
      if((input("'x' para detener la ejecución o enter para seguir: "))=="x"):  exit(0)
  except Exception as E:
    print("Error:",E)
    exit(0)
