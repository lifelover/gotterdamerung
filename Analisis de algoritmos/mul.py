#!/usr/bin/env python
#-*-coding:utf8;-*-
from random import randint,seed    #Para los números aleatorios
from sys import exit          #Para salir
from math import pow
import sys
def GenerarNum(rango):
	a = ''
	for i in range(1,rango+1):	a += str(i)
	return	int(a)  #Generar números random entre 0 y 10K

def GenerarNumBin(rango):
	a = '0b'
	bit = 1
	for i in range(1,rango+1):
		if(bit==1):
			a += str(1)
			bit = not bit
		else:
			a += str(0)
			bit = not bit
	return a
def retBin(bin):	return bin[2:]
def I_num(num,m):	return num // 10**m    #Obtener la parte izquierda, por ejemplo si es 255//(10**2) = 2.55, entero = 2
def D_num(num,m):	return int(num % (10**m))  #Parte derecha, por ejemplo 255%(10**2)= 255%100 = 55
#12345 -> Inum -> 12345, m=2, 12345/100 = 123.45 -> 123 | 12 Inum=1 -> 1.2 -> 1
#12345 -> Dnum -> 12345%100  => 45 | 12 Dnum=1 -> 2 -> 2
def funcCall(a,b):	
	return bin(func(int(a,2),int(b,2)))
def func(a,b):
	if (a < 10 or b < 10):   #Los casos base... (si a=10,b=1, entonces se usa el algoritmo)
		#print("X:",a," Y:",b," regresando-> ",a*b,sep="")
		return a*b
	m = max(len(str(a)), len(str(b)))//2 #Longitud máxima de uno de los dos números, divida entre 2 y convertida a entero con // (es lo mismo que floor)
	ia = I_num(a,m)                   #obtener el lado izquierdo de a
	da = D_num(a,m)                   #obtener el lado derecho de a
	ib = I_num(b,m)                   #obtener el lado izquierdo de b
	db = D_num(b,m)                   #obtener el lado derecho de b
	#print("X:",a,"Y:",b,"M:",m,"    |||...IA",ia,"DA",da,"IB",ib,"DB",db)
	iaib = func(ia,ib)                #multiplicar ia*db (la primera rama)
	dadb = func(da,db)                #multiplicar da*db (la tercera rama)
	enmedio = func(ia + da, ib + db) -iaib -dadb  #obtener la segunda rama
	res = int(iaib*pow(10,2*m)+enmedio*pow(10,m)+dadb)  #multiplicar n*2 (se dividió entre 2 previamente)
	#print("m=",m," | ",iaib,"*10^",2*m," + ",enmedio,"*10^",m," + ",dadb," = ",res,sep="")
	return res    #regresar el resultado

def Base10Prog():
	while(True):
		#a,b = 25132,213
		rangoA = int(input("	Ingrese cantidad de cifras para A: "))
		rangoB = int(input("	Ingrese cantidad de cifras para B: "))
		a,b = GenerarNum(rangoA),GenerarNum(rangoB) #Generar dos valores aleatorios
		res = func(a,b)
		assert a*b==res,"Error en la comprobación: "+str(res) #Comprobar que la multiplicación sea correcta
		print("	Utilizando valores: [ ",a," * ",b," = ",res," ] ",sep="")
		break

def Base2Prog():
	while(True):
		#a,b = 25132,213
		rangoA = int(input("	Ingrese cantidad de cifras para A: "))
		rangoB = int(input("	Ingrese cantidad de cifras para B: "))
		a,b = GenerarNumBin(rangoA),GenerarNumBin(rangoB) #Generar dos valores aleatorios binarios
		res = funcCall(a,b)				#Mandar a llamar a la función
		assert int(int(a,2)*int(b,2))==int(res,2),"Error en la comprobación: "+str(res) #Comprobar que la multiplicación sea correcta
		print("	Utilizando valores: [ ",retBin(a)," (",int(a,2),")", " * ",retBin(b)," (",int(b,2),") = ",retBin(res)," (",int(res,2),")] ",sep="")
		break


if __name__ == "__main__":
	try:    #En caso de que haya algún error (no debería de haber)
		while True:	
			sel=input("Seleccione: Binario[b], Decimal[d]: ")
			if(sel not in ["d","b"]):
				print("Por favor, seleccione de nuevo.")
			elif(sel=="d"):	Base10Prog()
			elif(sel=="b"):	Base2Prog()
	except Exception as E:
		print("Error:",E)
		exit(0)
