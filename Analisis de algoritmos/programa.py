import random
import math

def Mutar(listabin,vectF): #esperamos recibir el la lista de listas a mutar en forma de lista esta incluira todos los vecotores en decimal para cada variable
	#vectF es el vector a mutar
	n=len(listabin)#obtenemos la longitud de la lista
	lret=[]
	#print("Vector a mutar",vectF,n)
	for i in range(n-1):
		vect=[]
		#print("\nvect: ",i)
		n1=len(vectF)
		luckyvect=random.randint(0,n1-1)#tomamos una posicion aleatoria de la lista a la cual alterar
		vect=vectF
		#print(listabin[i])
		#print("Bits que se pueden alterar",vectF[luckyvect],"posicion",luckyvect)
		#print("vector a reemplazar",listabin[i])
		lvaslist=list(vectF[luckyvect])
		#print(lvaslist)
		n2=len(lvaslist)
		bitalterado=random.randint(0,n2-1)#escogemos una posicion de los bits
		#print(listabin)
		#print("Lista de bits a alterar\n",lvaslist,"longiud: ",n2,"bit que se alterara",bitalterado)
		#print(bitalterado)
		if(lvaslist[bitalterado]=="0"):  #cambiamos el valor que tenga el bit en tal posicion
			lvaslist[bitalterado]="1"
		else:
			lvaslist[bitalterado]="0"
		#print(lvaslist)
		aux1=''.join(lvaslist)#tomamos la lista mutada y la transformamos a cadena 
		#print(aux1,type(aux1)) 		#imprimimos el vector alterado en binario
		vect=list(vect)
		vect[luckyvect]=aux1
		#print(vect)
		lret.append(vect)
	lret.append(vectF)
		#listabin[i][luckyvect]=aux1
		#aux2=int(aux1,2) #convertimos la cadena a decimal
	return lret

def Entre(Psum,eval):
	n=len(Psum)
	if(Psum[0]>eval):
		#print(eval,0)
		return 0
	else:
		for i in range(n):
			#print(Psum[i],eval,Psum[i+1])
		#	print(Psum[i]<eval and eval<Psum[i+1])
			if(Psum[i]<eval and eval<=Psum[i+1]):
		#		print(i+1)
				return i+1


def VerificaXj(vector,restricc):
	res=0
	veri=0
	for i in range(4-len(vector)):
		vector.append(0)
	
	for i in range(len(restricc)):
		res=0
		res=restricc[i][0]*vector[0]+restricc[i][1]*vector[1]+restricc[i][2]*vector[2]+restricc[i][3]*vector[3]	
		#print(str(restricc[i][0]*vector[0])+'+'+str(restricc[i][1]*vector[1])+'+'+str(restricc[i][2]*vector[2])+'+'+str(restricc[i][3]*vector[3])+'='+str(res))
		#print(str(res)+restric[i][4]+str(restric[i][5]))
		if restricc[i][4]=='>':
			#print('mayor que')
			if res>restricc[i][5]:
				veri=veri+1
			
		else:
			if restricc[i][4]=='<':
			#print('menor que')
				if res<restricc[i][5]:
					veri=veri+1
			else:
				if res==restricc[i][5]:
					veri=veri+1
				
	
	if veri==len(restricc):
		return True
	#print(str(veri))
	return False


def Calculaxj(rango,mj,vec):#rango es una lista d edos elemntos [aj,bj]
	xj=int(rango[0])+vec*((int(rango[1])-int(rango[0]))/(pow(2,mj)-1))
	return xj
#def generarVectores(liminf,limsup):
def Calculamj(rango,nbits):#rango es una lista d edos elemntos [aj,bj]
	# mj se obtiene a partir de la sig formula:
	#2 elevado a la (mj -1)<(bj-aj)10 elevado a la n <=2 a la (mj )-1
	
	aux=(int(rango[1])-int(rango[0]))*math.pow(10,nbits)
	mj=(math.log2(aux))/(math.log2(2))
	#if(math.pow(2,mj-1)<aux and aux<(math.pow(2,mj)-1)):
	return math.ceil(mj) #math.ceil() redondea al valor siguiente ejemplo ceil(4.1)=5
	"""else:
		print("no valido")
		return -1""" 
"""def GenerarBinario(nbits):
	vect=[]
	for i in range (0,nbits):
		vect.append(random.randint(0,1))
	return vect"""

"""
s=list(str(101100))
#print(len(s))
#print("\n")
#x=Mutar(s)
mj=Calculamj([1,5],1) #Ejemplo del calculo de mj
print(mj)
aux1=(2**mj)-1 #Guardamos los valores de (2**mj)-1 y 2**(mj-1)
aux2=2**(mj-1)
print(aux2)"""


coef=['Ax','By','Cw','Dq']
numvariables = int (input('introduce el numero de variables'))
variables=[]#aqui se guardan los coeficientes de la funcion objetivo
#la funcion objetivo tiene forma de Z=Ax+by+Cw+Dq
print('Z=',end='')
for i in range(numvariables):
	print(coef[i],end='')
	if i==(numvariables-1):
		print('',end='')
	else:
		print('+',end='')
coeficientes=['A','B','C','D']
varr=['x','y','w','q']
print('')
variables = [0,0,0,0]
for i in range(numvariables):
	variables[i]=(float (input('introduce el valor de '+coeficientes[i]+' ')))


numres=int (input('introduce el numero de restricciones'))
# [numero restriccion][restriccion]

restricc = [[0 for x in range(6)] for y in range(numres)] 
#restricc[0] (restriccion)
#	0	,[A,B,C,D,>/<,N]
#	1	,[A,B,C,D,>/<,N]
#	2	,[A,B,C,D,>/<,N]
#x+y>20
#restricc[n][1]+restricc[n][2]+restricc[n][3]+restricc[n][4],>o<,N
cuatro=4
for i in range(numres):
	print('restriccion '+str(i))
	for e in range(numvariables):
		print(coef[e],end='')
		if e==(numvariables-1):
			print('',end='')
		else:
			print('+',end='')
	print('(< o >) N')
	
	for j in range(numvariables):
		restricc[i][j]=float(input('introduce el valor de '+coef[j]))
	restricc[i][4]=input('menor que o mayor que? (<,>)')
	restricc[i][5]=int(input('introduce el valor de N'))
	

bitseguridad=int(input("Introduce el numero de bits de seguridad"))
poblacion=int(input("Introduce la poblacion"))
nvars=numvariables
listaderangos=[]#Esta sera una lista con listas de dos elemtos que contengan los limites
listademj=[]#Esta sera una lista con el numero de bits para cada variable
listavectoresInt=[]
listavectoresBin=[]
listavectoresAlt=[]
lxj=[]
lz=[]
Psum=[]
lProb=[]
vectoreFuerte=[]
zaux=0.0
zsum=0.0
probacumulada=0.0
auxindex=0

for i in range(nvars):
	rango=[int(input("Introduce el limite inferior de la variable "+varr[i])),int((input("Introduce el limite superior de la variable "+varr[i])))]
	mj=Calculamj(rango,bitseguridad)
	listademj.append(mj)
	listaderangos.append(rango)
	
for i in range(poblacion):
	vector=[]
	vectorbin=[]
	for j in range(nvars):
		y=random.getrandbits(listademj[j])#Genera entero de n bits
		#print(y)
		vector.append(y)
		cadenaprueba="{0:0"+str(listademj[j])+"b}"
		vectorbin.append(cadenaprueba.format(y))#vector[i]=y
		#print(len(vectorbin[j]))

	listavectoresInt.append(vector)
	listavectoresBin.append(vectorbin)
	
	
for i in range(poblacion):
	vector=[]
	for j in range(nvars):
		y=Calculaxj(listaderangos[j],listademj[j],listavectoresInt[i][j])#Genera xj
		#print(y)
		vector.append(y)
	lxj.append(vector)


for i in range(poblacion):
	while True:
		if VerificaXj(lxj[i],restricc):##si cumple se sale y si no cumple se repite hasta que si cumpla
			break
		else:
			vector=[]
			vectorbin=[]
			for j in range(nvars):
				y=random.getrandbits(listademj[j])#Genera entero de n bits
				#print(y)
				vector.append(y)
				cadenaprueba="{0:0"+str(listademj[j])+"b}"
				vectorbin.append(cadenaprueba.format(y))#vector[i]=y
				#print(len(vectorbin[j]))
			listavectoresInt[i]=vector
			listavectoresBin[i]=vectorbin
			vector=[]
			for j in range(nvars):
				y=Calculaxj(listaderangos[j],listademj[j],listavectoresInt[i][j])#Genera xj
				#print(y)
				vector.append(y)
			lxj[i]=vector
		
	


	
	
print("Lista de rangos:\n",listaderangos)
print("Lista de numero de bits:\n",listademj)
print("Lista de vectores como enteros:\n",listavectoresInt)
print("Lista de vectores como binarios:\n",listavectoresBin)
print("Lista de xj:\n",lxj)	

bandMaxMin=input("Deseas maximizar(1) o minimizar(2)?")
iteraciones=int(input("Introduce numero de iteraciones"))
for j in range(iteraciones):
	for i in range(poblacion):
		#z=eval(funcion)
		z=0
		
		for j in range(numvariables):
			z=z+lxj[i][j]*float(int(variables[j]))
			
		if(i==0):
			zaux=z
			auxindex=i
		if(bandMaxMin=='1' and z>zaux):
			zaux=z
			auxindex=i
		elif(bandMaxMin=='2' and z<zaux):
			zaux=z
			auxindex=i
		lz.append(z)
	
	print("Valor a tomar",zaux,auxindex)
	print('z buscada = ',zaux)
	print('valores de las variables = ',lxj[auxindex])
	vectoreFuerte=listavectoresBin[auxindex]
	#print(vectoreFuerte)
	
	
	
	listavectoresAlt=Mutar(listavectoresBin,vectoreFuerte)
	listavectoresBin=listavectoresAlt
	for i in range(poblacion):
		for e in range(numvariables):
			listavectoresInt[i][e]=int(listavectoresBin[i][e],2)

	for i in range(poblacion):
		vector=[]
		for e in range(nvars):
			y=Calculaxj(listaderangos[e],listademj[e],listavectoresInt[i][e])#Genera xj
			#print(y)
			vector.append(y)
		lxj[i]=vector
	
	for i in range(poblacion):
		while True:
			if VerificaXj(lxj[i],restricc):##si cumple se sale y si no cumple se repite hasta que si cumpla
				break
			else:
				vector=[]
				vectorbin=[]
				for e in range(nvars):
					y=random.getrandbits(listademj[e])#Genera entero de n bits
					#print(y)
					vector.append(y)
					cadenaprueba="{0:0"+str(listademj[e])+"b}"
					vectorbin.append(cadenaprueba.format(y))#vector[i]=y
					#print(len(vectorbin[j]))
				listavectoresInt[i]=vector
				listavectoresBin[i]=vectorbin
				vector=[]
				for e in range(nvars):
					y=Calculaxj(listaderangos[e],listademj[e],listavectoresInt[i][e])#Genera xj
					#print(y)
					vector.append(y)
				lxj[i]=vector
		
		
		
	

	
	
	
	
	



