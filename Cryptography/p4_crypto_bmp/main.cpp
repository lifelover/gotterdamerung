#include <iostream>
#include "BMP.h"
using namespace std;
	/*
	 ************************************************************************************************************
	 * 
	 * ESCUELA SUPERIOR DE CÓMPUTO
	 * Juan Manuel Dávila Méndez
	 * C++ Backend intented to be used with the Python App Controller.
	 * Made using code from https://solarianprogrammer.com/2018/11/19/cpp-reading-writing-bmp-images/
	 *
	 ************************************************************************************************************
	 */ 
int main(int argc, char *argv[]) {
	int status = 1;
	try{
	if(! (argc>6)){
		cout << "[C++ BACKEND/ERROR] 6 parameters are required "<<endl;
		return 1;
	}
	BMP SomeBMPFile(argv[1]);
	int R = atoi(argv[2]);
	int G = atoi(argv[3]);
	int B = atoi(argv[4]);
	int MODE = atoi(argv[5]);
	char *OFILE = argv[6];
	//NiceFunction(B, G, R, ALPHA, 0)	//Add value and then use the modulo function on all pixels
	//NiceFunction(B, G, R, ALPHA, 1)	//Substract value and then use the modulo function  on all pixels
	SomeBMPFile.NiceFunction(B, G, R, 0,MODE);
	SomeBMPFile.write(OFILE);
	status = 0;
	}
	catch (const std::exception& e){
	cout << "[C++ BACKEND/ERROR] "<< e.what() <<endl;
	cout << "[C++ BACKEND/ERROR] Returning with status "<< status <<endl;
	return 1;
	}
	if(status)
		cout << "[C++ BACKEND/ERROR] Returning with status "<< status <<endl;
	cout << "[C++ BACKEND/OK] Success! Status: "<< status <<endl;
	return status;
}
