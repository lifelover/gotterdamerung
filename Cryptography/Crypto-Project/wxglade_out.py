#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import wx, threading, time, socket, pickle, random, string
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes
from Crypto.Cipher import AES, PKCS1_OAEP
from wx.lib.embeddedimage import PyEmbeddedImage
from helper import recv_data,send_data, get_own_ip

def random_string(N):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=N))

def search_in_list(theList,ip,field):
        return next((item for item in theList if item[field] == ip),None)

def SendCommand(ip,port,data_to_be_sent):
    try:    
        with socket.socket() as theSocket:
            theSocket.connect((ip,port))
            #print("COMMAND:",data_to_be_sent[0])
            data_to_be_sent = pickle.dumps(data_to_be_sent)
            send_data(theSocket,data_to_be_sent)
            received_data = recv_data(theSocket)
            theSocket.close()
            #print("SOCKET CLOSED\n")
            return received_data
    except Exception as e:  
        print("Something wrong has happened!",e)
        return None

def GeneratePriVateKey(keySize=4096):
    rand_str = random_string(10) + ".pem"
    new_key = RSA.generate(keySize, e=65537)
    private_key = new_key.exportKey('PEM')
    #public_key = new_key.publickey().exportKey('PEM')
    with open(rand_str,'wb') as privateKeyFile: #,   open(publicKeyName,'wb') as publicKeyFile:
        privateKeyFile.write(private_key)
        #publicKeyFile.write(public_key)
    return rand_str

class main_frame(wx.Frame):
    def __init__(self, *args, **kwds):
        # begin wxGlade: main_frame.__init__
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        self.SetSize((900, 461))    
        icon = PyEmbeddedImage('iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAABghJREFUeJzt3U+rJFcdx+HvjYsgd+K4UONCkCxD3kAYcGFwqW/AF6Cg2bjIUmkzWWSRd5OAu+ig+AeEcRTdqG9As0jIGFwExsW9xzSdW32qq0796erngYLbcPlxTlMfalPVlQAAAAAAAAAAAAAAAAAAAAAAAAAAAFyEqyT3Gs164XYebMJVkneSPEnylZGzXkzytyRvRSRsQInj2e0xJpISR5klEs7aYRxjIjmMQyScta44hkTSFYdIOEu1OE6JpBaHSDgrfePoE0nfOETCWTg1jmORnBqHSFi1oXHcFcnQOETCKo2NYz+SlzMuDpGwKq3ieJbkgyT/bDRLJCxuzXGIhEWdQxwiYRHnFIdImNU5xiESZnHOcYiESW0hDpEwiS3FIRKa2mIcIqGJLcchEka5hDhEwiAt4/hXkp82miUSFtc6jldu577RaKZIWMxUcRRrj+RRkueHfnls29RxFGuN5FHa/W4XGzNXHMXaIhEHneaOo1hLJOKg01JxFEtHIg46LR1HsVQk4qDTWuIo5o5EHHRaWxzFXJGIg05rjaOYOhJx0Kn1vVXfn2idU0XyOMk3J1ozZ651HH9P8jTJtyZab+tIniT5JG3eT8IGvZTk47SLo3w+h0hKHPufRcLnPMi4SA7jOIdIDuMQCUcNjaQrjjVH0hWHSDjq1EhqcawxklocIuGovpF8kOQfPf5vTZH0jUMkHFWL5NQ41hDJqXGIhKO6Ihkax5KRDI1DJBx1GMnYOJaIZGwc5fhTPFHIHUokreKYM5JWcTxL8vpEa2UDvpu2ccwRycOIgxm8mOSvaR/HlJF8O+JgBlPHMUUk4mAWc8XRMhJxMIu542gRiTiYxdj3kC8RiTiYxdJxDInktYiDGawljlMiEQezWFscfSJpGcePB3xnXIivZZ1xHIvElWPDnlt6AQc+SfLvpRdxxHWSX+SzSF5L8l6SLzaY/Zsk34jXGlBxLzc/e7P01aJ2Jflh2l05fr3399sRCRVrj+RxpolDJPS21kimjkMk9La2SOaKQyT0tpZI5o5DJPR2nWUjWSoOkdDbUpEsHYdI6G3uSNYSh0joba5I1haHSOht6kjWGodI6O06ya+y7jh+kptbSKaIWCRUtY6kZRzlrtwXIhIW1CqS93PzNOBHDWb96GCNImFRYyN5/3ZGkryacZEcxlGIhEUNjWQ/jmJoJF1xFCJhUddJfplxcRSnRlKLoxAJi+obybE4ir6R9I2jEAmLqkXSJ46iFsmpcRQiYVFdkZwSR9EVydA4CpGwqMNIhsRRHEYyNo5CJCyqRDImjuLVJB+mXRyFSFjUdcbHUXy10ZxDIoEKkUCFSKBCJFAhEqgQCVSIBCpEAhUigQqRQIVIoEIkUCESqBAJVIgEKkQCFSKBCpFAhUigQiRQIRKoEAlUfCnJHyMS+JyrJO8keZrkSUQC/1fiKCeySODWYRwigVtdcYiEi1eLQyRcrL5xiISLc2ocIuFiDI1DJGze2DhEwma1ikMkbNLDtD+RnyZ5PMFckTC7B0k+TtuT+FGSr8cNjmxEy0geJbl3O9ddwGxGi0j24yhEwmaMieSuOAqRsBlDIjkWRyESNuOUSPrEUYiEzegTySlxFCJhM45FMiSOQiRsxl2RjImjEAmbsR9JizgKkbAZD5K8m3ZxFBcRyXNLL4DJ/TbJ93Jzn1VLX0jyaZK/NJ6bJP/NTShwlr6c5A+5OYn/k+TPaXf12M23DWhvP45ytIpkN982oL274mgVyW6+bUB7x+IYG8luvm1Ae/dTj2NoJLv5tgHtnRLHqZHs5tsGtDckjr6R7ObbBrQ3Jo5aJLv5tgHttYijK5LdfNuA9lrGcRjJbr5tQHtTxFGOn824D2jufpLfZ5o4dvNtA9oTB3QQB3QQB3SYMo6fz7gPaO5+kt9lujhW8TQgDCEO6CAO6CAO6CAOOOIHmSaONyMONuAqyVsRB3RqGYk42KQWkYiDTRsTiTi4CEMieRhxcEFOiUQcXKQ+kYiDi3YsEnFA7o5EHLBnPxJxwB2uknwn4gAAAAAAAAAAAAAAAAAAAG78D3RBZn+BbaFgAAAAAElFTkSuQmCC').GetIcon()
        self.SetIcon(icon)
        self.notebook_1 = wx.Notebook(self, wx.ID_ANY)
        self.notebook_1_pane_2 = wx.Panel(self.notebook_1, wx.ID_ANY)
        self.window_1 = wx.SplitterWindow(self.notebook_1_pane_2, wx.ID_ANY)
        self.window_1_pane_1 = wx.Panel(self.window_1, wx.ID_ANY)
        self.window_2 = wx.SplitterWindow(self.window_1_pane_1, wx.ID_ANY, style=wx.SP_3D | wx.SP_THIN_SASH)
        self.window_2_pane_1 = wx.Panel(self.window_2, wx.ID_ANY)
        self.chatbox_textctrl = wx.TextCtrl(self.window_2_pane_1, wx.ID_ANY, "", style=wx.TE_MULTILINE | wx.TE_READONLY)
        self.window_2_pane_2 = wx.Panel(self.window_2, wx.ID_ANY)
        self.list_ctrl_1 = wx.ListCtrl(self.window_2_pane_2, wx.ID_ANY, style=wx.LC_HRULES | wx.LC_REPORT | wx.LC_VRULES)
        self.window_1_pane_2 = wx.Panel(self.window_1, wx.ID_ANY)
        self.window_3 = wx.SplitterWindow(self.window_1_pane_2, wx.ID_ANY)
        self.window_3_pane_1 = wx.Panel(self.window_3, wx.ID_ANY)
        self.input_textctrl = wx.TextCtrl(self.window_3_pane_1, wx.ID_ANY, "", style=wx.TE_MULTILINE | wx.TE_PROCESS_ENTER)
        self.window_3_pane_2 = wx.Panel(self.window_3, wx.ID_ANY)
        self.window_4 = wx.SplitterWindow(self.window_3_pane_2, wx.ID_ANY)
        self.window_4_pane_1 = wx.Panel(self.window_4, wx.ID_ANY)
        self.button_1 = wx.Button(self.window_4_pane_1, wx.ID_ANY, "SEND")
        self.window_4_pane_2 = wx.Panel(self.window_4, wx.ID_ANY)
        self.connect_button = wx.Button(self.window_4_pane_2, wx.ID_ANY, u"🔌")
        self.notebook_1_TRACKERS = wx.Panel(self.notebook_1, wx.ID_ANY)
        self.window_5 = wx.SplitterWindow(self.notebook_1_TRACKERS, wx.ID_ANY)
        self.window_5_pane_1 = wx.Panel(self.window_5, wx.ID_ANY)
        self.tracker_textctrl = wx.TextCtrl(self.window_5_pane_1, wx.ID_ANY, "", style=wx.TE_MULTILINE)
        self.window_5_pane_2 = wx.Panel(self.window_5, wx.ID_ANY)
        self.spin_ctrl_1 = wx.SpinCtrl(self.window_5_pane_2, wx.ID_ANY, "1", min=1, max=120)
        self.tracker_button = wx.Button(self.window_5_pane_2, wx.ID_ANY, "Announce/Parse")
        self.notebook_1_SETTINGS = wx.Panel(self.notebook_1, wx.ID_ANY)
        self.private_key_button = wx.Button(self.notebook_1_SETTINGS, wx.ID_ANY, "Select private key file...")
        self.nickname_textctrl = wx.TextCtrl(self.notebook_1_SETTINGS, wx.ID_ANY, "")
        self.ok_nickname_button = wx.Button(self.notebook_1_SETTINGS, wx.ID_ANY, "Change/Set nickname")
        self.button_3 = wx.Button(self.notebook_1_SETTINGS, wx.ID_ANY, "Generate private key... (4096 bits)")
        self.apply_button = wx.Button(self.notebook_1_SETTINGS, wx.ID_ANY, "Apply")
        self.checkbox_1 = wx.CheckBox(self.notebook_1_SETTINGS, wx.ID_ANY, "Show notifications")
        self.checkbox_2 = wx.CheckBox(self.notebook_1_SETTINGS, wx.ID_ANY, "Remember me")
        self.checkbox_3 = wx.CheckBox(self.notebook_1_SETTINGS, wx.ID_ANY, "Don't allow private keys with size of 2048 bits")

        self.__set_properties()
        self.__do_layout()
        self.__set_vars()

        self.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.on_item_right_click, self.list_ctrl_1)
        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.on_item_selected, self.list_ctrl_1)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_enter_pressed, self.input_textctrl)
        self.Bind(wx.EVT_BUTTON, self.on_send_press, self.button_1)
        self.Bind(wx.EVT_BUTTON, self.on_connect_press, self.connect_button)
        self.Bind(wx.EVT_SPINCTRL, self.on_update_interval_change, self.spin_ctrl_1)
        self.Bind(wx.EVT_BUTTON, self.on_announce_press, self.tracker_button)
        self.Bind(wx.EVT_BUTTON, self.on_private_key_file_button_press, self.private_key_button)
        self.Bind(wx.EVT_BUTTON, self.on_ok_nickname_button, self.ok_nickname_button)
        self.Bind(wx.EVT_BUTTON, self.on_generate_private_key_press, self.button_3)
        self.Bind(wx.EVT_BUTTON, self.on_apply_button, self.apply_button)
        self.Bind(wx.EVT_CHECKBOX, self.on_show_notifications, self.checkbox_1)
        self.Bind(wx.EVT_CHECKBOX, self.on_remember_me, self.checkbox_2)
        self.Bind(wx.EVT_CHECKBOX, self.on_dont_allow_small_keys, self.checkbox_3)
        # end wxGlade
    def __set_vars(self):
        self.public_key = ""
        self.nickname = ""
        self.update_interval = 1
        self.tracker_count = 0
        self.is_running_the_thread = False
        self.tracker_list = []
        self.is_okay_to_spam_server = False
        self.peer_list = []

    def __set_properties(self):
        # begin wxGlade: main_frame.__set_properties
        self.SetTitle("6 6 6")
        self.list_ctrl_1.InsertColumn(0, "Peers", format=wx.LIST_FORMAT_LEFT, width=130)
        self.window_2.SetMinimumPaneSize(20)
        self.window_4.SetMinimumPaneSize(20)
        self.window_3.SetMinimumPaneSize(20)
        self.window_1.SetMinimumPaneSize(20)
        self.window_5.SetMinimumPaneSize(20)
        self.checkbox_1.SetValue(1)
        self.nickname_textctrl.Disable()
        self.ok_nickname_button.Disable()
        self.tracker_button.Disable()
        self.apply_button.Disable()
        self.checkbox_3.SetValue(1)
        # end wxGlade

    def __do_layout(self):
        # begin wxGlade: main_frame.__do_layout
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_15 = wx.BoxSizer(wx.VERTICAL)
        sizer_18 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_17 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_16 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_12 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_14 = wx.BoxSizer(wx.VERTICAL)
        sizer_13 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_4 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_5 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_9 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_11 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_10 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_6 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_3 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_8 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_7 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_7.Add(self.chatbox_textctrl, 1, wx.ALIGN_BOTTOM | wx.ALIGN_CENTER | wx.ALIGN_RIGHT | wx.ALL | wx.EXPAND, 0)
        self.window_2_pane_1.SetSizer(sizer_7)
        sizer_8.Add(self.list_ctrl_1, 1, wx.EXPAND, 0)
        self.window_2_pane_2.SetSizer(sizer_8)
        self.window_2.SplitVertically(self.window_2_pane_1, self.window_2_pane_2, 739)
        sizer_3.Add(self.window_2, 1, wx.EXPAND, 0)
        self.window_1_pane_1.SetSizer(sizer_3)
        sizer_6.Add(self.input_textctrl, 1, wx.ALIGN_BOTTOM | wx.ALIGN_CENTER | wx.ALIGN_RIGHT | wx.ALL | wx.EXPAND, 0)
        self.window_3_pane_1.SetSizer(sizer_6)
        sizer_10.Add(self.button_1, 0, wx.ALIGN_BOTTOM | wx.ALIGN_CENTER | wx.ALIGN_RIGHT | wx.ALL | wx.EXPAND, 0)
        self.window_4_pane_1.SetSizer(sizer_10)
        sizer_11.Add(self.connect_button, 1, wx.ALIGN_BOTTOM | wx.ALIGN_CENTER | wx.ALIGN_RIGHT | wx.ALL | wx.EXPAND, 0)
        self.window_4_pane_2.SetSizer(sizer_11)
        self.window_4.SplitVertically(self.window_4_pane_1, self.window_4_pane_2, 80)
        sizer_9.Add(self.window_4, 1, wx.EXPAND, 0)
        sizer_5.Add(sizer_9, 1, wx.EXPAND, 0)
        self.window_3_pane_2.SetSizer(sizer_5)
        self.window_3.SplitVertically(self.window_3_pane_1, self.window_3_pane_2, 738)
        sizer_4.Add(self.window_3, 1, wx.EXPAND, 0)
        self.window_1_pane_2.SetSizer(sizer_4)
        self.window_1.SplitHorizontally(self.window_1_pane_1, self.window_1_pane_2, 336)
        sizer_2.Add(self.window_1, 1, wx.EXPAND, 0)
        self.notebook_1_pane_2.SetSizer(sizer_2)
        sizer_13.Add(self.tracker_textctrl, 1, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 0)
        self.window_5_pane_1.SetSizer(sizer_13)
        label_1 = wx.StaticText(self.window_5_pane_2, wx.ID_ANY, "Update inverval (s):", style=wx.ALIGN_CENTER)
        sizer_14.Add(label_1, 0, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 11)
        sizer_14.Add(self.spin_ctrl_1, 0, 0, 0)
        sizer_14.Add(self.tracker_button, 0, wx.ALIGN_CENTER | wx.EXPAND | wx.TOP, 15)
        self.label_2 = wx.StaticText(self.window_5_pane_2, wx.ID_ANY, "Status: Waiting for servers...")
        self.label_2.Wrap(101)
        sizer_14.Add(self.label_2, 0, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 19)
        self.window_5_pane_2.SetSizer(sizer_14)
        self.window_5.SplitVertically(self.window_5_pane_1, self.window_5_pane_2, 710)
        sizer_12.Add(self.window_5, 1, wx.EXPAND, 0)
        self.notebook_1_TRACKERS.SetSizer(sizer_12)
        sizer_16.Add(self.private_key_button, 1, wx.ALL | wx.EXPAND, 12)
        self.waiting_for_key_label = wx.StaticText(self.notebook_1_SETTINGS, wx.ID_ANY, "Status: waiting for private key file....")
        sizer_16.Add(self.waiting_for_key_label, 1, wx.ALL, 13)
        sizer_15.Add(sizer_16, 1, wx.EXPAND, 0)
        sizer_17.Add(self.nickname_textctrl, 2, wx.ALL | wx.EXPAND, 12)
        sizer_17.Add(self.ok_nickname_button, 1, wx.BOTTOM | wx.RIGHT | wx.TOP, 13)
        sizer_15.Add(sizer_17, 1, wx.EXPAND, 0)
        sizer_15.Add(self.button_3, 1, wx.ALL | wx.EXPAND, 13)
        sizer_15.Add(self.apply_button, 1, wx.ALL | wx.EXPAND, 13)
        sizer_18.Add(self.checkbox_1, 1, wx.ALIGN_CENTER | wx.ALL, 13)
        sizer_15.Add(sizer_18, 1, wx.EXPAND, 0)
        sizer_15.Add(self.checkbox_2, 1, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 13)
        sizer_15.Add(self.checkbox_3, 1, wx.LEFT | wx.RIGHT | wx.TOP, 13)
        try:    self.own_ip = "    |    " + get_own_ip()
        except: self.own_ip = ""
        label_5 = wx.StaticText(self.notebook_1_SETTINGS, wx.ID_ANY, u"Made with ❤ and 🍺  by Satan"+self.own_ip)
        self.own_ip = str((([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")] or [[(s.connect(("8.8.8.8", 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) + ["no IP found"])[0])
        sizer_15.Add(label_5, 1, wx.ALIGN_CENTER | wx.ALL, 13)
        self.notebook_1_SETTINGS.SetSizer(sizer_15)
        self.notebook_1.AddPage(self.notebook_1_pane_2, "    CHAT    ")
        self.notebook_1.AddPage(self.notebook_1_TRACKERS, "    TRACKERS    ")
        self.notebook_1.AddPage(self.notebook_1_SETTINGS, "    SETTINGS    ")
        sizer_1.Add(self.notebook_1, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        self.Layout()
        # end wxGlade

    def on_item_right_click(self, event):  # wxGlade: main_frame.<event_handler>
        pass
#UWU
    def on_item_selected(self, event):  # wxGlade: main_frame.<event_handler>
        self.this_selected_ip = self.list_ctrl_1.GetItemText(self.list_ctrl_1.GetFocusedItem(),0)
        print(self.this_selected_ip)
        try:
            self.client = Client(self.this_selected_ip, 4321)
            self.client.initTCP()
            self.client.EscuchaThread()
            self.client.EnviarThread()
            wx.MessageBox("Connected", "Hell no!",  wx.OK | wx.ICON_ERROR)  
        except:
            wx.MessageBox("Error connecting to peer.", "Hell no!",  wx.OK | wx.ICON_ERROR)  

    def on_enter_pressed(self, event):  # wxGlade: main_frame.<event_handler>
        mensaje = self.input_textctrl.GetValue()
        self.input_textctrl.SetValue("")
        if self.client.connected:
            self.client.enviarMensaje(mensaje)
        else:
            self.server.enviarMensaje(mensaje)
#UWU
    def on_send_press(self, event):  # wxGlade: main_frame.<event_handler>
        self.on_enter_pressed(self)
        #wx.CallAfter(self.list_ctrl_1.InsertItem,999, "entrada")


    def on_connect_press(self, event):  # wxGlade: main_frame.<event_handler>
        print("Event handler 'on_connect_press' not implemented!")
        event.Skip()

    def on_update_interval_change(self, event):  # wxGlade: main_frame.<event_handler>
        self.update_interval = int(self.spin_ctrl_1.GetValue())
        event.Skip()

    def update_peer_list_in_ui(self):
        ip_list = []
        while True:
            ui_ip_list = []
            for entry in range(self.list_ctrl_1.GetItemCount()):
                ui_ip_list.append(self.list_ctrl_1.GetItemText(entry))   

            for peer in self.peer_list:
                if peer["ip"] not in ui_ip_list and peer["ip"] != self.own_ip: 
                    print(peer["ip"],"is now in the list",self.own_ip)
                    wx.CallAfter(self.list_ctrl_1.InsertItem,999, peer["ip"])

            for peer in self.peer_list:
                if peer not in ip_list:
                    ip_list.append(peer["ip"])
            for peer in ip_list:
                if peer not in [peersito["ip"] for peersito in self.peer_list]:
                    ip_list.remove(peer)

            for entry in range(self.list_ctrl_1.GetItemCount()):
                if self.list_ctrl_1.GetItemText(entry) not in ip_list:
                    wx.CallAfter(self.list_ctrl_1.DeleteItem,entry)

            time.sleep(self.update_interval)



    def register_in_every_tracker(self):
        self.is_running_the_thread = True
        list1 = list2  = []
        list_toggle = True
        while True:
            while self.is_okay_to_spam_server:
                #Conectar a cada IP (tracker)
                for ip_pair in self.tracker_list:
                    try:    tracker_response = pickle.loads(SendCommand(ip_pair[0],ip_pair[1],["Register",{"nick":self.nickname}]))
                    except: tracker_response = None
                    #Si la respuesta a este tracker es nula, eliminarlo de nuestra lista de trackers
                    if tracker_response == None:
                        print("Removing faulty tracker...",ip_pair)
                        try:    
                            self.tracker_list.remove(ip_pair)
                            print("OK: Removed:",ip_pair,self.tracker_list)
                            if(len(self.tracker_list)==0):
                                wx.CallAfter(wx.MessageBox,"None of the trackers seem to be available. Please check them out.\n(Announce thread is stopped for now).", "Hell no!",  wx.OK | wx.ICON_ERROR)
                                wx.CallAfter(self.label_2.SetLabel,("Status: connected to \n"+str(self.tracker_count))+" trackers.")
                                self.is_running_the_thread = False
                                return 
                        except: pass
                    #Si el tracker responde correctamente a nuestra petición
                    if tracker_response == "OK":
                        self.tracker_count += 1
                        #Esperar a que el tracker responda nuestra petición de recibir la lista
                        data_from_tracker = pickle.loads(SendCommand(ip_pair[0],ip_pair[1],["GetList"]))
                        #Eliminar nuestra propia IP en caso de que el cliente se ejecute localmente junto con el servidor
                        entry_with_own_ip = search_in_list(data_from_tracker,get_own_ip(),"ip")
                        if entry_with_own_ip != None:   data_from_tracker.remove(entry_with_own_ip)
                        #Agregar incrementalmente cada entrada recibida del tracker sin repeticiones
                        for entry in data_from_tracker:
                            if entry not in self.ip_list:
                                self.ip_list.append(entry)
                        #Eliminar cada entrada que no esté en la lista que justo se obtuvo en esta iteración
                        for entry in self.ip_list:
                            if entry not in data_from_tracker:
                                self.ip_list.remove(entry)

                #Lista que únicamente contiene las IP 
                list_with_ip = []
                #Filtrar todas las IPs repetidas (equivalente a usar set()) de la lista que contiene diccionarios
                self.ip_list = list({l['ip']:l for l in self.ip_list}.values())
                print("IP LIST:",self.ip_list)
                #Sólo guardar IPs en list_with_ip obtenidas de la lista de diccionarios
                for entry in self.ip_list:
                    list_with_ip.append(entry["ip"])

                #Agregar cada ip a la lista de peers si es que no está ahí
                for ip in self.ip_list:
                    if ip["ip"] not in [ip[0] for ip in self.peer_list]:
                        self.peer_list.append((ip["ip"],ip["nick"]))
                #Quitar cada ip que no esté en la lista que justo se acaba de actualizar
                for ip in self.peer_list:
                    if ip[0] not in list_with_ip:
                        self.peer_list.remove(ip)
                print("PEERLIST:",self.peer_list)
                print("Updating list every ",self.update_interval,"s  with ",\
                    self.tracker_count," trackers. (",len(self.peer_list)," peers).",\
                    sep="")
                wx.CallAfter(self.label_2.SetLabel,("Status: connected to \n"+str(self.tracker_count))+" tracker(s).")
                self.tracker_count = 0
                time.sleep(self.update_interval)
            if(not self.is_okay_to_spam_server):    break
            
                


    def on_announce_press(self, event):  # wxGlade: main_frame.<event_handler>
        tracker_box_text = self.tracker_textctrl.GetValue()
        tracker_box_text = tracker_box_text.split(',')
        for entry in tracker_box_text:
            if(":" not in entry):
                wx.MessageBox("Your IP list seems to be corrupted or empty. Please check it again.", "Hell no!",  wx.OK | wx.ICON_ERROR)  
                self.is_okay_to_spam_server = False
                return
            else:
                try:
                    ip = entry.split(':')[0]
                    port = int(entry.split(':')[1])
                    entry = [ip,port]
                    if(entry not in self.tracker_list):
                        self.tracker_list.append(entry)
                    self.is_okay_to_spam_server = True
                    if(not self.is_running_the_thread):
                        threading.Thread(target = self.register_in_every_tracker).start()
                        #threading.Thread(target = self.update_peer_list_in_ui).start()
                except:
                    self.is_okay_to_spam_server = False
                    wx.MessageBox("Your IP list seems to be corrupted or empty. Please check it again.", "Hell no!",  wx.OK | wx.ICON_ERROR)  



    def on_private_key_file_button_press(self, event):  # wxGlade: main_frame.<event_handler>
        with wx.FileDialog(self, "Open your private key file", wildcard="RSA key file (*.pem)|*.pem",style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:
                if fileDialog.ShowModal() == wx.ID_CANCEL:  
                    return
                else:
                    private_key = open(fileDialog.GetPath(), "rb").read()       # We read the private key 
                    key = RSA.import_key(private_key)                           # We import the key
                    self.public_key =  key.publickey().export_key()             # And then we export the public key out of the private key
                    self.nickname_textctrl.Enable()
                    self.ok_nickname_button.Enable()
                    self.tracker_button.Enable()
                    self.waiting_for_key_label.SetLabel("  OK! Your private key seems to be a valid file.")
            

    def on_ok_nickname_button(self, event):  # wxGlade: main_frame.<event_handler>
        nickname = self.nickname_textctrl.GetValue()
        print(nickname)
        if(len(nickname) > 0):  
            self.nickname = nickname 
            wx.MessageBox("OK", "Hell no!",  wx.OK | wx.ICON_ERROR)  
        else:
            wx.MessageBox("Your nickname can't be an empty string!", "Hell no!",  wx.OK | wx.ICON_ERROR)  
         


    def on_generate_private_key_press(self, event):  # wxGlade: main_frame.<event_handler>
        final_name = GeneratePriVateKey()
        wx.MessageBox("Done. Private key saved as: "+final_name, "Hell no!",  wx.OK | wx.ICON_ERROR)  

    def on_apply_button(self, event):  # wxGlade: main_frame.<event_handler>
        print("Event handler 'on_apply_button' not implemented!")
        event.Skip()

    def on_show_notifications(self, event):  # wxGlade: main_frame.<event_handler>
        print("Event handler 'on_show_notifications' not implemented!")
        event.Skip()

    def on_remember_me(self, event):  # wxGlade: main_frame.<event_handler>
        print("Event handler 'on_remember_me' not implemented!")
        event.Skip()

    def on_dont_allow_small_keys(self, event):  # wxGlade: main_frame.<event_handler>
        print("Event handler 'on_dont_allow_small_keys' not implemented!")
        event.Skip()

# end of class main_frame

class MyApp(wx.App):
    def OnInit(self):
        self.frame = main_frame(None, wx.ID_ANY, "")
        self.SetTopWindow(self.frame)
        self.frame.Show()
        return True

# end of class MyApp
if __name__ == "__main__":
    app = MyApp(0)
    app.MainLoop()