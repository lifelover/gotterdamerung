#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import logging
from PIL import Image, ImageFile
from Crypto.Cipher import AES, DES

def padKeyAes(key):
	paddedKey = pad(key.encode(),'AES').decode()
	if(len(paddedKey) not in [16,24,32]):
		logging.warning("Key too long. Padding key with size: "+str(len(paddedKey)))
		if(len(paddedKey)>32):	return paddedKey[:32]
		if(len(paddedKey)>24):	return paddedKey[:24]
		if(len(paddedKey)>16):	return paddedKey[:16]
	return pad(key.encode(),'AES').decode()

def padKeyDes(key):
	if(len(key)<8):		return pad(key.encode(),'DES').decode()[:8]
	if(len(key)>8):		return pad(key[:8].encode(),'DES').decode()
	return pad(key.encode(),'DES').decode()

def padIV(iv,mode):
	padded_IV = pad(iv.encode(),mode).decode()
	if(mode=='AES' and len(padded_IV)>AES.block_size):	return padded_IV[:AES.block_size]
	if(mode=='DES' and len(padded_IV)>DES.block_size):	return padded_IV[:DES.block_size]
	return padded_IV

def pad(data,mode):
	if(mode=='AES'):	length	=	(AES.block_size - len(data)) % AES.block_size
	elif(mode=='DES'):	length	=	(DES.block_size - len(data)) % DES.block_size
	return	data + bytes([length])*length

def unpad(data):	return data[:-data[-1]]

def toUtf8(data):	return data.encode('UTF-8')
####	DES

def desCipher(key,data,operation,mode,iv=None):
	if(operation=='ECB'):		tempCipher = DES.new(toUtf8(padKeyDes(key)), DES.MODE_ECB)
	elif(operation=='CBC'):		tempCipher = DES.new(toUtf8(padKeyDes(key)), DES.MODE_CBC, toUtf8(padIV(iv,'DES')))
	elif(operation=='OFB'):		tempCipher = DES.new(toUtf8(padKeyDes(key)), DES.MODE_OFB, toUtf8(padIV(iv,'DES')))
	elif(operation=='CFB'):		tempCipher = DES.new(toUtf8(padKeyDes(key)), DES.MODE_CFB, toUtf8(padIV(iv,'DES')))
	return tempCipher.encrypt(pad(data,'DES'))	if (mode=='ENCRYPT') else  tempCipher.decrypt(pad(data,'DES')) 

####	AES

def aesCipher(key,data,operation,mode,iv=None):
	if(operation=='ECB'):		tempCipher = AES.new(toUtf8(padKeyAes(key)), AES.MODE_ECB)
	elif(operation=='CBC'):		tempCipher = AES.new(toUtf8(padKeyAes(key)), AES.MODE_CBC, toUtf8(padIV(iv,'AES')))
	elif(operation=='OFB'):		tempCipher = AES.new(toUtf8(padKeyAes(key)), AES.MODE_OFB, toUtf8(padIV(iv,'AES')))
	elif(operation=='CFB'):		tempCipher = AES.new(toUtf8(padKeyAes(key)), AES.MODE_CFB, toUtf8(padIV(iv,'AES')))
	return tempCipher.encrypt(pad(data,'AES'))	if (mode=='ENCRYPT') else  tempCipher.decrypt(pad(data,'AES')) 


class DisemboweledImage():   							         #Disemboweled image class.
	def __init__(self,imagePath):
		logging.basicConfig(level=logging.INFO)
		self.image 	=	Image.open(imagePath).convert('RGB')
		self.image_array	=	bytes(self.image.tobytes())

	def SaveAesModifiedImage(self,name,key,operation,mode,iv=None,format=None):
		logging.info("Key: "+str(key)+" "+str(len(key))+" Mode: "+str(mode)+" op_mode: "+str(operation)+" iv_len "+str(len(iv)))
		if(operation=='ECB'):		image_array =  aesCipher(key,self.image_array,'ECB',mode)
		elif(operation=='CBC'):		image_array =  aesCipher(key,self.image_array,'CBC',mode,iv)
		elif(operation=='OFB'):		image_array =  aesCipher(key,self.image_array,'OFB',mode,iv)
		elif(operation=='CFB'):		image_array =  aesCipher(key,self.image_array,'CFB',mode,iv)
		logging.info("Success!")
		if(format):	Image.frombytes("RGB", self.image.size, image_array, "raw", "RGB").save(name,format)
		else:		Image.frombytes("RGB", self.image.size, image_array, "raw", "RGB").save(name,format='BMP')

	######################################################################

	def SaveDesModifiedImage(self,name,key,operation,mode,iv=None,format=None):
		logging.info("Key: "+str(key)+" "+str(len(key))+" Mode: "+str(mode)+" op_mode: "+str(operation)+" iv_len "+str(len(iv)))
		if(operation=='ECB'):		image_array =  desCipher(key,self.image_array,'ECB',mode)
		elif(operation=='CBC'):		image_array =  desCipher(key,self.image_array,'CBC',mode,iv)
		elif(operation=='OFB'):		image_array =  desCipher(key,self.image_array,'OFB',mode,iv)
		elif(operation=='CFB'):		image_array =  desCipher(key,self.image_array,'CFB',mode,iv)
		logging.info("Success!")
		if(format):	Image.frombytes("RGB", self.image.size, image_array, "raw", "RGB").save(name,format)
		else:		Image.frombytes("RGB", self.image.size, image_array, "raw", "RGB").save(name,format='BMP')

if __name__ == "__main__":

	#
	#	AVAILABLE MODES:
	#	ECB, CBC, OFB, CFB
	#	We don't need an IV for ECB mode, but you can pass it to the encryption functions without any problems.
	#
	#	The following section is a test. Doesn't actually run when this file is called from the main app,
	#	but might help you to debug the backend code.
	
	logging.basicConfig(level=logging.INFO)

	key = 'unapasswordbiensabrosongayyyylmao'
	iv 	= '123456ayyyylmao21231'

	modes = ['ECB','CBC','OFB','CFB']
	for mode in modes:
		encrypted = DisemboweledImage("1.bmp")
		encrypted.SaveDesModifiedImage(mode+'test.png',key,mode,'ENCRYPT',iv,'PNG')
		decrypted = DisemboweledImage(mode+'test.png')
		decrypted.SaveDesModifiedImage(mode+'TestDecrypted.png',key,mode,'DECRYPT',iv,'PNG')

