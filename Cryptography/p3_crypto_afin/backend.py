import sys

def createLanguageTable():
	languageTable = {}
	'''
	for i in range(65,91):
		languageTable[chr(i)] = i-65
		languageTable[i-65] = chr(i)
	for i in range(97,123):
		languageTable[chr(i)] = i-97+26
		languageTable[i-97+26]	= chr(i)
	strangechars = ["á","é","í","ó","ú","ñ","\n",";",".",","," ","?","¿","¡","!","-","_"]
	cont = 52
	for i in range(len(strangechars)):
		languageTable[strangechars[i]] = cont
		languageTable[cont]	= strangechars[i]
		cont+=1
	'''
	for i in range(0,256):
		languageTable[chr(i)] = i
		languageTable[i] = chr(i)
	LANGSIZE = len(languageTable)//2
	print("Dictionary:",languageTable,"with size:",LANGSIZE)
	return languageTable, LANGSIZE


def quickcheck(a,b):
	if((a*b)/b == 1):	#Si por ejemplo, a= 1 y m= 3, entonces el inverso es a*m (regresar a)
		return a
	if((a*b)/a == 1):	#El otro caso
		return 0
	return None

def EuclideanAlgorithm(a,m):				#Esta parte del código ejecuta el algoritmo de euclides.
	if(a == 0 or m == 0):	return False, False			#Si alguno de los números es 0, mandar error
	if(quickcheck(a,m) is not None):	return quickcheck(a,m), True
	if(a==m):	return False, False						#Si a == m, por ejemplo, a = 1, m = 1, entonces no hay inverso 
	if(a%m == 0 or m%a == 0):	return False, False		#Si, por ejemplo, a = 20 y m = 10, entonces también no hay inverso
	numA, numM, counter, remainder = max(a,m), min(a,m), 0, 666				#Ordenar los números, ¿cuál es el más grande?
	remainderlist, lst = [], []
	print("==================== EUCLIDEAN ALGORITHM ====================")
	while True:
		if(remainder==0):					#Si el residuo es 0
			if(len(remainderlist)>1):		#Y si la lista de "q0's" tiene al menos 2 elementos
				if(remainderlist[-2]!=1):	#Si el penúltimo elemento de la lista no es un uno, entonces no existe el inverso multiplicativo
					return False, False
			break
		mult = numA//numM	#Esto es lo mismo que floor(numA/numM)
		remainder = numA - mult * numM
		print(numA,"=",mult, "(",numM,") + ",remainder, "	[ step ",counter,"]")
		counter+=1
		numA, numM = numM, remainder
		remainderlist.append(remainder)
		lst.append(mult)
	return lst,counter
		

def ExtendedEuclideanAlgorithm(lst,counter,m,toggle=False):	#Esta parte es la que ejecuta el cálculo de las P's.
	if(toggle):	p_array = [1,0] 			#Si el número a es mayor que m, invertir el orden de p0 y p1. Ejemplo: a = 9, m= 5
	else:	p_array = [0,1] 				#Si a es menor que m, usar el orden por defecto. Ejemplo: a = 69, m = 239
	print("=============== EXTENDED EUCLIDEAN ALGORITHM ===============")
	print("Initial vector: ",p_array)
	for i in range(2,counter+1):			#Omitir los dos primeros pasos, pues ya tenemos p0 y p1
		n = (p_array[i-2]-p_array[i-1]*lst[i-2])		#Fórmula: pi-2 - pi-1*qi-2	% m
		res = n%m
		print("(",p_array[i-2],"-",p_array[-1],"*",lst[i-2],") => ",n," % ",m," = ",res,"		[step ",i,"]",sep="")
		p_array.append(res)					#Regresar el resultado de la última iteración
	return res

def inverse(a,m):
	inversostuff =	EuclideanAlgorithm(a,m)	#Invertstuff es un arreglo que contiene una lista (con los coeficientes q0) y un número
	print(inversostuff)				#que nos dirá en qué paso encontraremos el módulo inverso.
	if(inversostuff[0] is False and inversostuff[1] is False):	return None
	if(inversostuff[1] is True):	#Este caso se activa con si a=1 y m=5, o si a=20 y m=1, por decir un ejemplo. Línea 13
		return inversostuff[0]
	if(a>m):	coprimo = ExtendedEuclideanAlgorithm(inversostuff[0],inversostuff[1],m,True)	#A es mayor, cambiar el orden de la lista
	else:	coprimo = ExtendedEuclideanAlgorithm(inversostuff[0],inversostuff[1],m)		#Dejar tal cual
	result = coprimo*a%m
	assert result==1,"Something wrong has happened :("			#Si hay algún error en el algoritmo, avisar
	return coprimo

if __name__ == "__main__":
	pass
	
	
