#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import wx, sys
import backend
from time import time
from wx.lib.embeddedimage import PyEmbeddedImage

def AffineEncDec(keyA,keyB,languageTable,LANGSIZE,file,mode=None):		#chr(CHAR_NUM)
    words=[]										#ord(ASCII_SYMBOL)						
    errflag = 0
    for line in file:           #Read file, line by line
        for char in line:       #Read line character by character
            try:                
                if(mode):	
                    if char in languageTable:	
                        words.append(languageTable.get((keyA*languageTable[char]+keyB)%LANGSIZE))
                else:	
                    if char in languageTable:	
                        words.append(languageTable.get((keyA*(languageTable[char]-keyB))%LANGSIZE))
            except Exception as e:
                errflag = 1
                words.append("_")       #This can happen with a negative ascii value
    return ''.join(words),errflag       


class theApp(wx.Frame):
    def __init__(self, *args, **kwds):
        # begin wxGlade: theApp.__init__
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        self.SetSize((501, 504))
        self.combo_box_1 = wx.ComboBox(self, wx.ID_ANY, choices=["Encrypt", "Decrypt", "Select..."], style=wx.CB_DROPDOWN | wx.CB_READONLY)
        self.text_ctrl_2 = wx.TextCtrl(self, wx.ID_ANY, "")
        self.text_ctrl_3 = wx.TextCtrl(self, wx.ID_ANY, "")
        self.button_1 = wx.Button(self, wx.ID_ANY, "Verify Keys/Preview")
        self.button_2 = wx.Button(self, wx.ID_ANY, "Save")
        self.text_ctrl_1 = wx.TextCtrl(self, wx.ID_ANY, "", style=wx.TE_MULTILINE | wx.TE_READONLY)

        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_COMBOBOX, self.OnComboBoxChange, self.combo_box_1)
        self.Bind(wx.EVT_TEXT_ENTER, self.OnEnterChange, self.combo_box_1)
        self.Bind(wx.EVT_TEXT, self.OnKeyATxtBox, self.text_ctrl_2)
        self.Bind(wx.EVT_TEXT, self.OnKeyBTxtBox, self.text_ctrl_3)
        self.Bind(wx.EVT_BUTTON, self.OnCheckKeyPress, self.button_1)
        self.Bind(wx.EVT_BUTTON, self.OnOKButtonPress, self.button_2)
        # end wxGlade

    def __set_properties(self):
        # begin wxGlade: theApp.__set_properties
        self.languageTable , self.LANGSIZE = backend.createLanguageTable()
        self.keyA = 0
        self.keyB = 0
        self.keyAInverse = 0
        self.keyAstatus = 0
        self.keyBstatus = 0
        self.filestuff = ""
        self.mode = ""
        self.filestatus = 0
        self.keystatus = 0
        self.button_1.Disable()
        self.button_2.Disable()
        self.text_ctrl_2.Disable()
        self.text_ctrl_3.Disable()
        self.SetTitle("Affine Cipher App")
        sys.stdout = self.text_ctrl_1
        self.combo_box_1.SetSelection(2)
        icon = PyEmbeddedImage('iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAABghJREFUeJzt3U+rJFcdx+HvjYsgd+K4UONCkCxD3kAYcGFwqW/AF6Cg2bjIUmkzWWSRd5OAu+ig+AeEcRTdqG9As0jIGFwExsW9xzSdW32qq0796erngYLbcPlxTlMfalPVlQAAAAAAAAAAAAAAAAAAAAAAAAAAAFyEqyT3Gs164XYebMJVkneSPEnylZGzXkzytyRvRSRsQInj2e0xJpISR5klEs7aYRxjIjmMQyScta44hkTSFYdIOEu1OE6JpBaHSDgrfePoE0nfOETCWTg1jmORnBqHSFi1oXHcFcnQOETCKo2NYz+SlzMuDpGwKq3ieJbkgyT/bDRLJCxuzXGIhEWdQxwiYRHnFIdImNU5xiESZnHOcYiESW0hDpEwiS3FIRKa2mIcIqGJLcchEka5hDhEwiAt4/hXkp82miUSFtc6jldu577RaKZIWMxUcRRrj+RRkueHfnls29RxFGuN5FHa/W4XGzNXHMXaIhEHneaOo1hLJOKg01JxFEtHIg46LR1HsVQk4qDTWuIo5o5EHHRaWxzFXJGIg05rjaOYOhJx0Kn1vVXfn2idU0XyOMk3J1ozZ651HH9P8jTJtyZab+tIniT5JG3eT8IGvZTk47SLo3w+h0hKHPufRcLnPMi4SA7jOIdIDuMQCUcNjaQrjjVH0hWHSDjq1EhqcawxklocIuGovpF8kOQfPf5vTZH0jUMkHFWL5NQ41hDJqXGIhKO6Ihkax5KRDI1DJBx1GMnYOJaIZGwc5fhTPFHIHUokreKYM5JWcTxL8vpEa2UDvpu2ccwRycOIgxm8mOSvaR/HlJF8O+JgBlPHMUUk4mAWc8XRMhJxMIu542gRiTiYxdj3kC8RiTiYxdJxDInktYiDGawljlMiEQezWFscfSJpGcePB3xnXIivZZ1xHIvElWPDnlt6AQc+SfLvpRdxxHWSX+SzSF5L8l6SLzaY/Zsk34jXGlBxLzc/e7P01aJ2Jflh2l05fr3399sRCRVrj+RxpolDJPS21kimjkMk9La2SOaKQyT0tpZI5o5DJPR2nWUjWSoOkdDbUpEsHYdI6G3uSNYSh0joba5I1haHSOht6kjWGodI6O06ya+y7jh+kptbSKaIWCRUtY6kZRzlrtwXIhIW1CqS93PzNOBHDWb96GCNImFRYyN5/3ZGkryacZEcxlGIhEUNjWQ/jmJoJF1xFCJhUddJfplxcRSnRlKLoxAJi+obybE4ir6R9I2jEAmLqkXSJ46iFsmpcRQiYVFdkZwSR9EVydA4CpGwqMNIhsRRHEYyNo5CJCyqRDImjuLVJB+mXRyFSFjUdcbHUXy10ZxDIoEKkUCFSKBCJFAhEqgQCVSIBCpEAhUigQqRQIVIoEIkUCESqBAJVIgEKkQCFSKBCpFAhUigQiRQIRKoEAlUfCnJHyMS+JyrJO8keZrkSUQC/1fiKCeySODWYRwigVtdcYiEi1eLQyRcrL5xiISLc2ocIuFiDI1DJGze2DhEwma1ikMkbNLDtD+RnyZ5PMFckTC7B0k+TtuT+FGSr8cNjmxEy0geJbl3O9ddwGxGi0j24yhEwmaMieSuOAqRsBlDIjkWRyESNuOUSPrEUYiEzegTySlxFCJhM45FMiSOQiRsxl2RjImjEAmbsR9JizgKkbAZD5K8m3ZxFBcRyXNLL4DJ/TbJ93Jzn1VLX0jyaZK/NJ6bJP/NTShwlr6c5A+5OYn/k+TPaXf12M23DWhvP45ytIpkN982oL274mgVyW6+bUB7x+IYG8luvm1Ae/dTj2NoJLv5tgHtnRLHqZHs5tsGtDckjr6R7ObbBrQ3Jo5aJLv5tgHttYijK5LdfNuA9lrGcRjJbr5tQHtTxFGOn824D2jufpLfZ5o4dvNtA9oTB3QQB3QQB3SYMo6fz7gPaO5+kt9lujhW8TQgDCEO6CAO6CAO6CAOOOIHmSaONyMONuAqyVsRB3RqGYk42KQWkYiDTRsTiTi4CEMieRhxcEFOiUQcXKQ+kYiDi3YsEnFA7o5EHLBnPxJxwB2uknwn4gAAAAAAAAAAAAAAAAAAAG78D3RBZn+BbaFgAAAAAElFTkSuQmCC').GetIcon()
        self.SetIcon(icon)
        # end wxGlade

    def __do_layout(self):
        # begin wxGlade: theApp.__do_layout
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_2 = wx.BoxSizer(wx.VERTICAL)
        sizer_3 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_7 = wx.BoxSizer(wx.VERTICAL)
        sizer_6 = wx.BoxSizer(wx.VERTICAL)
        sizer_5 = wx.BoxSizer(wx.VERTICAL)
        sizer_4 = wx.BoxSizer(wx.VERTICAL)
        label_1 = wx.StaticText(self, wx.ID_ANY, "  Select an action:")
        sizer_4.Add(label_1, 0, wx.ALIGN_CENTER | wx.ALL, 4)
        sizer_4.Add(self.combo_box_1, 1, wx.EXPAND, 0)
        sizer_3.Add(sizer_4, 1, wx.EXPAND, 0)
        label_2 = wx.StaticText(self, wx.ID_ANY, "Key (A):")
        sizer_5.Add(label_2, 0, wx.ALIGN_CENTER | wx.ALL, 4)
        sizer_5.Add(self.text_ctrl_2, 1, wx.EXPAND, 0)
        sizer_3.Add(sizer_5, 1, wx.EXPAND, 0)
        label_3 = wx.StaticText(self, wx.ID_ANY, "Key (B):")
        sizer_6.Add(label_3, 0, wx.ALIGN_CENTER | wx.ALL, 4)
        sizer_6.Add(self.text_ctrl_3, 1, wx.EXPAND, 0)
        sizer_3.Add(sizer_6, 1, wx.EXPAND, 0)
        sizer_7.Add(self.button_1, 1, wx.EXPAND, 0)
        sizer_7.Add(self.button_2, 1, wx.EXPAND, 0)
        sizer_3.Add(sizer_7, 1, wx.EXPAND, 0)
        sizer_2.Add(sizer_3, 1, wx.EXPAND, 0)
        sizer_2.Add(self.text_ctrl_1, 6, wx.ALL | wx.EXPAND, 0)
        sizer_1.Add(sizer_2, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        self.Layout()
        # end wxGlade
    def OnComboBoxChange(self, event):  # wxGlade: theApp.<event_handler>
        if(not self.combo_box_1.GetStringSelection().startswith("Select")): 
            with wx.FileDialog(self, "Open file", wildcard="TXT files (*.txt)|*.txt",style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:
                if fileDialog.ShowModal() == wx.ID_CANCEL:  return
                else:   
                    try:
                        with open(fileDialog.GetPath(), "r", encoding='utf-8') as file:   #Open selected file
                            self.filestuff =  file.read()
                            print("[INFO] File has been opened successfully")
                            self.text_ctrl_2.Enable()
                            self.text_ctrl_3.Enable()
                            if(self.keystatus): self.button_1.Enable()
                            self.mode = self.combo_box_1.GetStringSelection()   #This can be either 'Decrypt' or 'Encrypt'
                            return
                    except Exception as e: print("[ERROR] Woah! There was a problem:",e)
        self.text_ctrl_1.SetValue("")
        self.combo_box_1.SetSelection(2)
        self.button_1.Disable()
        self.text_ctrl_2.Disable()
        self.text_ctrl_3.Disable()

    def OnEnterChange(self, event):  # wxGlade: theApp.<event_handler>
        print("Event handler 'OnEnterChange' not implemented!")
        event.Skip()

    def OnKeyATxtBox(self, event):  # wxGlade: theApp.<event_handler>
        value = self.text_ctrl_2.GetValue()
        if(value.isdigit()):
            if(int(value)>=0):   
                self.keyA = int(value)
                print("[INFO] Key A has been set:",self.keyA)
                self.keyAstatus = 1
                if(self.keyBstatus): self.button_1.Enable()
                self.button_2.Disable()
                return 
            else:
                wx.MessageBox('The key value must be an integer!', 'Error', wx.OK | wx.ICON_ERROR)
                self.text_ctrl_2.SetValue("");
        else:
            if(value==""):  pass
            else:
                wx.MessageBox('The key value must be an integer!', 'Error', wx.OK | wx.ICON_ERROR)
                self.text_ctrl_2.SetValue("");
        self.keyAstatus = 0
        self.keystatus = 0
        self.button_1.Disable()
        self.button_2.Disable()

    def OnKeyBTxtBox(self, event):  # wxGlade: theApp.<event_handler>
        value = self.text_ctrl_3.GetValue()
        if(value.isdigit()):
            if(int(value)>0):   
                self.keyB = int(value)
                print("[INFO] Key B has been set:",self.keyB)
                self.keyBstatus = 1
                if(self.keyAstatus): self.button_1.Enable()
                self.button_2.Disable()
                return 
            else:
                wx.MessageBox('The key value must be a positive integer!', 'Error', wx.OK | wx.ICON_ERROR)
                self.text_ctrl_3.SetValue("");
        else:
            if(value==""):  pass
            else:
                wx.MessageBox('The key value must be an integer!', 'Error', wx.OK | wx.ICON_ERROR)
                self.text_ctrl_3.SetValue("");
        self.keyBstatus = 0
        self.keystatus = 0
        self.button_1.Disable()
        self.button_2.Disable()

    def OnCheckKeyPress(self, event):  # wxGlade: theApp.<event_handler>
        self.keyAInverse = backend.inverse(self.keyA,self.LANGSIZE)
        if(self.keyAInverse is None):
            wx.MessageBox('Key (A) has to be coprime with '+str(self.LANGSIZE)+' !', 'Error', wx.OK | wx.ICON_ERROR)
            self.text_ctrl_2.SetValue("");
            return
        for line in self.filestuff:
        	for char in line:
        		if char in self.languageTable:
        			if(self.mode=="Encrypt"):	newchar = self.languageTable.get((self.keyA*self.languageTable[char]+self.keyB)%self.LANGSIZE)
        			else:	newchar = self.languageTable.get((self.keyAInverse*(self.languageTable[char]-self.keyB))%self.LANGSIZE)
        			print(newchar,sep="", end="")
        self.button_2.Enable()
        self.keystatus = 1

    def OnOKButtonPress(self, event):  # wxGlade: theApp.<event_handler>
        with wx.FileDialog(self, "Save "+self.mode+"ed file", defaultFile=self.mode+"ed.txt", wildcard="TXT files (*.txt)|*.txt",
                   style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT) as fileDialog:
            if fileDialog.ShowModal() == wx.ID_CANCEL:  return
            try:
                with open(fileDialog.GetPath(),"w", encoding="utf-8") as file:    #Try to create a new file
                    if(self.mode=="Encrypt"):
                        print("[ENCRYPT MODE]")
                        data, info = AffineEncDec(self.keyA,self.keyB,self.languageTable,self.LANGSIZE,self.filestuff,True)
                        written = file.write(data)
                    else:   
                        print("[DECRYPT MODE]")
                        data, info = AffineEncDec(self.keyAInverse,self.keyB,self.languageTable,self.LANGSIZE,self.filestuff)
                        written = file.write(data)
                    if(not info):   wx.MessageBox(str(written)+" bytes written to disk.", self.mode+" OK",  wx.OK | wx.ICON_INFORMATION)
                    else:   wx.MessageBox(str(written)+" bytes written to disk. There were errors trying to decrypt your file ", self.mode+" failure",  wx.OK | wx.ICON_ERROR)
            except Exception as e: print("[ERROR] Woah! There was a problem:",e)
        self.filestatus = 0


# end of class theApp

class MyApp(wx.App):
    def OnInit(self):
        self.frame = theApp(None, wx.ID_ANY, "")
        self.SetTopWindow(self.frame)
        self.frame.Show()
        return True

# end of class MyApp

if __name__ == "__main__":
    app = MyApp(0)
    app.MainLoop()
