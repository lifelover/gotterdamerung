#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import wx

def basicencryptdecrypt(key,file,mode=None):
    words=[]
    errflag = 0
    for line in file:           #Read file, line by line
        for char in line:       #Read line character by character
            try:                
                if(mode): words.append(chr(ord(char)+key))    #Encrypt mode
                else: words.append(chr(ord(char)-key))        #Decrypt mode
            except:
                errflag = 1
                words.append("[E: "+char+"]")       #This can happen if a negative ascii value
                print("ERROR trying to decode",char)#is used with chr()
    return ''.join(words),errflag       

class MyFrame(wx.Frame):
    def __init__(self, *args, **kwds):
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        self.filestuff = ""
        self.key = 0
        self.filestatus = 0
        self.keystatus = 0
        self.mode = 0
        self.SetSize((400, 80))
        self.panel_1 = wx.Panel(self, wx.ID_ANY)
        self.combo_box_1 = wx.ComboBox(self.panel_1, wx.ID_ANY, choices=["Action...", "Encrypt", "Decrypt"], style=wx.CB_DROPDOWN | wx.CB_READONLY | wx.TE_PROCESS_ENTER)
        self.text_ctrl_1 = wx.TextCtrl(self.panel_1, wx.ID_ANY, "", style=wx.TE_NO_VSCROLL | wx.TE_PROCESS_ENTER)
        self.button_1 = wx.Button(self.panel_1, wx.ID_ANY, "OK")
        self.__set_properties()
        self.__do_layout()
        self.Bind(wx.EVT_COMBOBOX, self.oncomboboxchange, self.combo_box_1)
        self.Bind(wx.EVT_TEXT, self.ontextinputhandler, self.text_ctrl_1)
        self.Bind(wx.EVT_BUTTON, self.okbutton, self.button_1)

    def __set_properties(self):
        self.SetTitle("Juan Méndez' encryption tool")
        self.combo_box_1.SetSelection(0)

    def __do_layout(self):
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_4 = wx.BoxSizer(wx.VERTICAL)
        sizer_3 = wx.BoxSizer(wx.VERTICAL)
        label_1 = wx.StaticText(self.panel_1, wx.ID_ANY, "Choose an action")
        sizer_3.Add(label_1, 0, wx.ALIGN_CENTER, 0)
        sizer_3.Add(self.combo_box_1, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALL | wx.EXPAND, 0)
        sizer_2.Add(sizer_3, 1, wx.EXPAND, 0)
        label_2 = wx.StaticText(self.panel_1, wx.ID_ANY, "Key:")
        sizer_4.Add(label_2, 0, wx.ALIGN_CENTER, 0)
        sizer_4.Add(self.text_ctrl_1, 0, wx.EXPAND, 0)
        sizer_2.Add(sizer_4, 1, wx.EXPAND, 0)
        sizer_2.Add(self.button_1, 0, wx.EXPAND, 0)
        self.panel_1.SetSizer(sizer_2)
        sizer_1.Add(self.panel_1, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        self.Layout()
        # end wxGlade

    def oncomboboxchange(self, event):  # This function will trigger whenever the thing in te combobox changes. 
        if(not self.combo_box_1.GetStringSelection().startswith("Action")): 
            with wx.FileDialog(self, "Open file", wildcard="TXT files (*.txt)|*.txt",style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:
                if fileDialog.ShowModal() == wx.ID_CANCEL:  return
                else:   
                    try:
                        with open(fileDialog.GetPath(), "r") as file:   #Open selected file
                            self.filestuff =  file.read()
                            print("[INFO] File has been opened successfully")
                            self.filestatus = 1
                            self.mode = self.combo_box_1.GetStringSelection()   #This can be either 'Decrypt' or 'Encrypt'
                            return
                    except Exception as e: print("[ERROR] Woah! There was a problem:",e)
        self.mode = 0

    def ontextinputhandler(self, event):  # Validate key value 
        value = self.text_ctrl_1.GetValue()
        if(value.isdigit()):
            if(1 <= int(value) <= 255):   
                self.key = int(value)
                self.keystatus = 1
                return 
            else:
                wx.MessageBox('The key value must be an integer between 1 and 255!', 'Error', wx.OK | wx.ICON_ERROR)
                self.text_ctrl_1.SetValue("");
        else:
            if(value==""):  pass
            else:
                wx.MessageBox('The key value must be an integer!', 'Error', wx.OK | wx.ICON_ERROR)
                self.text_ctrl_1.SetValue("");
        self.keystatus = 0

    def okbutton(self, event):  # Here, we do all the things we need to do whenever the OK button is pressed
        print("[INFO] KeyStatus:",self.keystatus,"FileStatus:",self.filestatus,"Mode:",self.mode)
        if(self.mode == 0):     #Return if no action has been selected
            wx.MessageBox("You must choose an action!", 'Error', wx.OK | wx.ICON_ERROR)
            return  
        if(self.keystatus and self.filestatus):
            with wx.FileDialog(self, "Save "+self.mode+"ed file", defaultFile=self.mode+"ed.txt", wildcard="TXT files (*.txt)|*.txt",
                       style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT) as fileDialog:
                if fileDialog.ShowModal() == wx.ID_CANCEL:  return
                try:
                    with open(fileDialog.GetPath(),"w") as file:    #Try to create a new file
                        if(self.mode=="Decrypt"):
                            data, info = basicencryptdecrypt(self.key,self.filestuff)
                            written = file.write(data)
                        else:   
                            data, info = basicencryptdecrypt(self.key,self.filestuff,"Fake love should be outdressed, and not because it's not a joke. You never know she is")
                            written = file.write(data)
                        if(not info):   wx.MessageBox(str(written)+" bytes written to disk.", self.mode+" OK",  wx.OK | wx.ICON_INFORMATION)
                        else:   wx.MessageBox(str(written)+" bytes written to disk. There were errors trying to decrypt your file ", self.mode+" failure",  wx.OK | wx.ICON_ERROR)
                except Exception as e: print("[ERROR] Woah! There was a problem:",e)
            self.filestatus = 0
        else:   
            wx.MessageBox("You must choose a file/Set a key!", 'Error', wx.OK | wx.ICON_ERROR)

class MyApp(wx.App):
    def OnInit(self):
        self.frame = MyFrame(None, wx.ID_ANY, "")
        self.SetTopWindow(self.frame)
        self.frame.Show()
        return True

if __name__ == "__main__":
    app = MyApp(0)
    app.MainLoop()
