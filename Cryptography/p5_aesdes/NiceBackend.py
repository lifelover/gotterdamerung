#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os
from PIL import Image, ImageFile
from Crypto.Cipher import AES, DES

def padKeyAes(key):
	return pad(key.encode(),'AES').decode()

def padKeyDes(key):
	if(len(key)>8):		return pad(key[:8].encode(),'DES').decode()
	return pad(key.encode(),'DES').decode()

def pad(data,mode):
	if(mode=='AES'):	length	=	(AES.block_size - len(data)) % AES.block_size
	else:				length	=	(DES.block_size - len(data)) % DES.block_size
	return	data + bytes([length])*length
'''	
def unpad(data):
	return data[:-data[-1]]
'''
####	DES

def desEcbEncrypt(key,data):
	tempCipher = DES.new(padKeyDes(key), DES.MODE_ECB)
	return tempCipher.encrypt(pad(data,'DES'))

def desEcbDecrypt(key,data):
	tempCipher = DES.new(padKeyDes(key), DES.MODE_ECB)
	return tempCipher.decrypt(pad(data,'DES'))

####	AES 

def aesEcbEncrypt(key,data):
	tempCipher = AES.new(padKeyAes(key), AES.MODE_ECB)
	return tempCipher.encrypt(pad(data,'AES'))

def aesEcbDecrypt(key,data):
	tempCipher = AES.new(padKeyAes(key), AES.MODE_ECB)
	return tempCipher.decrypt(pad(data,'AES'))

######################################

class DisemboweledImage():   							         #Disemboweled image class.
	def __init__(self,imagePath):
		self.image = Image.open(imagePath).convert('RGB')
		print("[INFO] Disemboweled image from",imagePath,"loaded as class!")

	def SavePPM(self):
		with open("original.ppm", 'wb') as outputFile:
			outputFile.write(self.GetImageHead() + self.GetImageBody())

	def SaveAesEncryptedImage(self,name,key,format=None):
		image_array = bytes(self.image.tobytes())
		print("[INFO/AES] Trying to encrypt with key", key, len(key))
		image_array =  aesEcbEncrypt(key,image_array)
		print("[INFO/AES] Attempt to encrypt was successful!")
		if(format):	Image.frombytes("RGB", self.image.size, image_array, "raw", "RGB").save(name,format)
		else:		Image.frombytes("RGB", self.image.size, image_array, "raw", "RGB").save(name,format='BMP')

	def SaveAesDecryptedImage(self,name,key,format=None):
		image_array = bytes(self.image.tobytes())
		print("[INFO/AES] Trying to decrypt with key", key, len(key))
		image_array =  aesEcbDecrypt(key,image_array)
		print("[INFO/AES] Attempt to decrypt was successful!")
		if(format):	Image.frombytes("RGB", self.image.size, image_array, "raw", "RGB").save(name,format)
		else:		Image.frombytes("RGB", self.image.size, image_array, "raw", "RGB").save(name,format='BMP')
		
	def SaveDesEncryptedImage(self,name,key,format=None):
		image_array = bytes(self.image.tobytes())
		print("[INFO/DES] Trying to encrypt with key", key, len(key))
		image_array =  desEcbEncrypt(key,image_array)
		print("[INFO/DES] Attempt to encrypt was successful!")
		if(format):	Image.frombytes("RGB", self.image.size, image_array, "raw", "RGB").save(name,format)
		else:		Image.frombytes("RGB", self.image.size, image_array, "raw", "RGB").save(name,format='BMP')

	def SaveDesDecryptedImage(self,name,key,format=None):
		image_array = bytes(self.image.tobytes())
		print("[INFO/DES] Trying to decrypt with key", key, len(key))
		image_array =  desEcbDecrypt(key,image_array)
		print("[INFO/DES] Attempt to decrypt was successful!")
		if(format):	Image.frombytes("RGB", self.image.size, image_array, "raw", "RGB").save(name,format)
		else:		Image.frombytes("RGB", self.image.size, image_array, "raw", "RGB").save(name,format='BMP')

if __name__ == "__main__":	pass
'''
key = 'unapasswordbiensabrosa'

chidori = DisemboweledImage("test")
chidori.SaveAesEncryptedImage('AEStest.png',key,'PNG')
chidori.SaveDesEncryptedImage('DEStest.png',key,'PNG')

topkek = DisemboweledImage('AEStest.png')
topkek.SaveAesDecryptedImage('AESTestDecrypted.png',key,'PNG')

ayyylmao = DisemboweledImage("DEStest.png")
ayyylmao.SaveDesDecryptedImage('DESTestDecrypted',key,'PNG')
'''