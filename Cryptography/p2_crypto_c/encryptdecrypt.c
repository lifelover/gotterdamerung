#include <string.h>
#include <stdio.h>
#include <openssl/evp.h>

void encrypt(FILE *ifp, FILE *ofp, unsigned char *key)
{
    fseek(ifp, 0L, SEEK_END);	//
    int fsize = ftell(ifp);		//	Tamaño del archivo (0L = long int)
    fseek(ifp, 0L, SEEK_SET);	//

    int outLen1 = 0; int outLen2 = 0;
    unsigned char *indata = malloc(fsize);
    unsigned char *outdata = malloc(fsize*2);
    unsigned char ivec[] = "0123456789012345";

    //Leer todo el archivo
    fread(indata,sizeof(char),fsize, ifp);//Read Entire File

    EVP_CIPHER_CTX *ctx;
	if(!(ctx = EVP_CIPHER_CTX_new())){
		printf("[C BACKEND] Coudln't start crypto proccess");
	}
    EVP_EncryptInit(ctx,EVP_aes_128_cbc(),key,ivec);	//iniciar algoritmo de cifrado
    EVP_EncryptUpdate(ctx,outdata,&outLen1,indata,fsize);	//proveer contexto, inicializar algoritmo
    int val = EVP_EncryptFinal(ctx,outdata + outLen1,&outLen2);		//finalizar operación de encriptado
    printf("[C BACKEND/CRYPTO] Encrypt OK: %d\n",val);
    fwrite(outdata,sizeof(char),outLen1 + outLen2,ofp);
    EVP_CIPHER_CTX_free(ctx);

}

int decrypt(FILE *ifp, FILE *ofp, unsigned char *key)
{
	fseek(ifp, 0L, SEEK_END);	//
    int fsize = ftell(ifp);		//	Tamaño del archivo (0L = long int)
    fseek(ifp, 0L, SEEK_SET);	//

    int outLen1 = 0; int outLen2 = 0;
    unsigned char *indata = malloc(fsize);
    unsigned char *outdata = malloc(fsize);
    unsigned char ivec[] = "0123456789012345";

    //Leer el archivo
    fread(indata,sizeof(char),fsize, ifp);//Leer archivo entero

    EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();

    EVP_DecryptInit(ctx,EVP_aes_128_cbc(),key,ivec);	//Iniciar algoritmo de descrifrado
    EVP_DecryptUpdate(ctx,outdata,&outLen1,indata,fsize);	//Proveer contexto, datos, longitud de salida y datos de entrada
    int val = EVP_DecryptFinal(ctx,outdata + outLen1,&outLen2);
    fwrite(outdata,sizeof(char),outLen1 + outLen2,ofp);
    EVP_CIPHER_CTX_free(ctx);
    if(val){
        printf("[C BACKEND] DECRYPT OK: %d",val);
        return 0;
    }
    return 1;

}

int main(int argc, char *argv[])
{   
    unsigned char *key;
    key = (unsigned char *) argv[3];
    FILE *fIN, *fOUT;
    if(strcmp(argv[4],"0")==0){
        if(fIN = fopen(argv[1], "rb")) {
            if(fOUT = fopen(argv[2], "wb")){
                encrypt(fIN, fOUT,key);        
                fclose(fIN);
                fclose(fOUT);
                printf("[C BACKEND] Encrypt OK\n");
                return 0;
            }
        return 1;
        }
    return 1;
    }
        
    if(strcmp(argv[4],"1")==0)
        if(fIN = fopen(argv[1], "rb")){
            if(fOUT = fopen(argv[2], "wb")){
                int status = decrypt(fIN, fOUT, key);        
                fclose(fIN);
                fclose(fOUT);
                if(status>0){
                    printf("[C BACKEND] Wrong or weak decryption key\n");
                    return 2;
                }
                return 0;
        	}
        	return 1;
        }
    return 1;
}
