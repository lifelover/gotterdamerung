from gi.repository import GObject
from gi.repository import Notify
import os

class Notification(GObject.Object):
    def __init__(self):
        super(Notification, self).__init__()
        # lets initialise with the application name
        Notify.init("Messengure")
    def send_notification(self, title, text, file_path_to_icon=os.getcwd()+"/logo.jpg"):
        n = Notify.Notification.new(title, text, file_path_to_icon)
        n.show()
my = Notification()
my.send_notification("this is a title", "this is some text")