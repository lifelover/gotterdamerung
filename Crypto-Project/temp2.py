import socket, pickle, struct, threading, logging
from helper import recv_data,send_data, get_own_ip, encrypt666, decrypt666


class MessageReceiver():
    def __init__(self, private_key,widget=None):
        self.stop_the_thread = False
        print("INIT RAN!")
        incoming_data_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
        incoming_data_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:    incoming_data_socket.bind(("0.0.0.0",1234))
        except Exception as e: logging.error(str(e))
        incoming_data_socket.listen(1234)
        while True:
            while not self.stop_the_thread:
                client, address = incoming_data_socket.accept()   
                threading.Thread(target = self.listen_to_client, args = (client,private_key,widget)).start() #thread

    def listen_to_client(self,client,private_key,widget=None):
        while not self.stop_the_thread:
            try:
                data = recv_data(client)
                if data:
                    client_ip = client.getpeername()[0] 
                    if client_ip.startswith("127"):
                        client_ip = get_own_ip()
                    data = pickle.loads(decrypt666(data,private_key))
                    #send_data(client,pickle.dumps("OK"))
                    print("Got data from",client_ip,":",data)
                    if data == "stop":
                        self.stop_the_thread = True
                    break
                else:  raise ValueError("Client disconnected")
            except Exception as e:
                print("ERROR:",e)
                client.close()                          
                break  


      
private_key = open("5QH4Y4AG3B.pem","rb").read()
receive_thr = threading.Thread(target = MessageReceiver, args = (private_key,)).start()
print("kek")