import struct, socket, sys, pickle
from io import BytesIO
import hashlib

def get_sha256_hash(binary_data):
    file_descriptor = BytesIO(binary_data)
    hasher = hashlib.sha256()
    while True:
        data = file_descriptor.read(65536)
        if not data:
            break
        hasher.update(data)
    return hasher.hexdigest()


def get_own_ip():
    return str((([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")] or [[(s.connect(("8.8.8.8", 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) + ["no IP found"])[0])
#
#   Resumen: Esta función se encarga de cifrar datos usando RSA+AES_EAX
#   Parámetros: <bytes> data_bytes: Datos a cifrar.
#               <bytes> public_key: Archivo de llave pública a utilizar para cifrar los datos.
#   
#   Return:     <bytes> Datos cifrados con la llave pública del destinatario.
#
def encrypt_data(data_bytes,public_key):
    from Crypto.PublicKey import RSA
    from Crypto.Random import get_random_bytes
    from Crypto.Cipher import AES, PKCS1_OAEP
    data_out = bytes()
    recipient_key = RSA.import_key(public_key)
    session_key = get_random_bytes(16)
    # Cifrar la llave de sesión usando la llave pública RSA
    cipher_rsa = PKCS1_OAEP.new(recipient_key)
    enc_session_key = cipher_rsa.encrypt(session_key)

    # Cifrar los datos usando AES y la llave de sesión
    cipher_aes = AES.new(session_key, AES.MODE_EAX)
    ciphertext, tag = cipher_aes.encrypt_and_digest(data_bytes)
    for x in (enc_session_key, cipher_aes.nonce, tag, ciphertext):
        data_out += x
    return data_out

#
#   Resumen: Esta función se encarga de descifrar datos usando RSA+AES_EAX
#   Parámetros: <bytes> data_bytes: Datos a descifrar.
#               <bytes> private_key:    Archivo de llave privada para descifrar los datos.
#   
#   Return:     <bytes> Datos descifrados con la llave privada del destinatario.
#

def decrypt_data(data_bytes, private_key):
    from Crypto.PublicKey import RSA
    from Crypto.Random import get_random_bytes
    from Crypto.Cipher import AES, PKCS1_OAEP
    file_in = BytesIO(data_bytes)
    private_key = RSA.import_key(private_key)
    enc_session_key, nonce, tag, ciphertext = \
        [ file_in.read(x) for x in (private_key.size_in_bytes(), 16, 16, -1) ]
    # Decrypt the session key with the private RSA key
    cipher_rsa = PKCS1_OAEP.new(private_key)
    session_key = cipher_rsa.decrypt(enc_session_key)
    # Decrypt the data with the AES session key
    cipher_aes = AES.new(session_key, AES.MODE_EAX, nonce)
    return cipher_aes.decrypt_and_verify(ciphertext, tag)

def send_data(sock, data):
    # A cada mensaje pegarle la longitud del mismo (entero de 4 bytes)
    data = struct.pack('>I', len(data)) + data
    sock.sendall(data)

def recv_data(sock):
    # Leer la longitud del mensaje y desempaquetar su longitud
    raw_datalen = recvall(sock, 4)
    if not raw_datalen:     return None
    datalen = struct.unpack('>I', raw_datalen)[0]
    # Leer n cantidad de bytes a recibir
    return recvall(sock, datalen)

def recvall(sock, n):
    # Función para recibir todos los datos o regresar None si se ha alcanzado el final del archivo
    data = b''
    while len(data) < n:
        packet = sock.recv(n - len(data))
        if not packet:  return None
        data += packet
    return data

def send_secure_message(message,ip,public_key,port):
    try:    
        with socket.socket() as theSocket:
            #theSocket.setblocking(0)
            theSocket.connect((ip,port))
            send_data(theSocket,encrypt_data(pickle.dumps(message),public_key))
            theSocket.close()
    except Exception as e:  
        print("Failure!",e)


if __name__ == '__main__':
    print(get_sha256_hash(b'aaa'))
