#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import wx, threading, time, socket, pickle, random, string
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes
from Crypto.Cipher import AES, PKCS1_OAEP
from wx.lib.embeddedimage import PyEmbeddedImage
from helper import recv_data,send_data, get_own_ip, decrypt_data, encrypt_data, send_secure_message, get_sha256_hash
from os import getcwd, mkdir
from os.path import basename, getsize, isdir
from os.path import join as osjoin
from gi.repository import GObject
from gi.repository import Notify
from contextlib import contextmanager
from drunken import get_random_art

@contextmanager
def color_set_to(widget,color):
    font1 = wx.Font(12, wx.MODERN, wx.NORMAL, wx.NORMAL, False, u'Consolas')
    wx.CallAfter(widget.SetFont,font1)
    wx.CallAfter(widget.SetDefaultStyle,wx.TextAttr(color))
    yield 
    widget.ShowPosition(widget.GetLastPosition())
    wx.CallAfter(widget.SetDefaultStyle,wx.TextAttr("white"))


class File():
    def __init__(self,file):
        self.name = basename(file)
        with open(file,'rb') as file_descriptor:
            self.file_contents = file_descriptor.read()
        self.size = getsize(file)
        self.hash = get_sha256_hash(self.file_contents)

    def get_contents(self):
        return self.file_contents

    def compute_hash(self):
        return get_sha256_hash(self.file_contents)


class Notification(GObject.Object):
    def __init__(self):
        super(Notification, self).__init__()
        # lets initialise with the application name
        Notify.init("Messengure")
    def send_notification(self, title, text, file_path_to_icon=getcwd()+"/logo3.png"):
        n = Notify.Notification.new(title, text, file_path_to_icon)
        n.show()


def random_string(N):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=N))

def search_in_list(theList,ip,field):
        return next((item for item in theList if item[field] == ip),None)

def SendCommand(ip,port,data_to_be_sent):
    try:    
        with socket.socket() as theSocket:
            theSocket.connect((ip,port))
            #print("COMMAND:",data_to_be_sent[0])
            data_to_be_sent = pickle.dumps(data_to_be_sent)
            send_data(theSocket,data_to_be_sent)
            received_data = recv_data(theSocket)
            theSocket.close()
            #print("SOCKET CLOSED\n")
            return received_data
    except Exception as e:  
        print("Something wrong has happened!",e)
        return None

def GeneratePriVateKey(keySize=4096):
    rand_str = random_string(10) + ".pem"
    new_key = RSA.generate(keySize, e=65537)
    private_key = new_key.exportKey('PEM')
    #public_key = new_key.publickey().exportKey('PEM')
    with open(rand_str,'wb') as privateKeyFile: #,   open(publicKeyName,'wb') as publicKeyFile:
        privateKeyFile.write(private_key)
        #publicKeyFile.write(public_key)
    return rand_str 

class main_frame(wx.Frame):
    def __init__(self, *args, **kwds):
        # begin wxGlade: main_frame.__init__
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        self.SetSize((1000, 511))
        icon = PyEmbeddedImage('iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAABghJREFUeJzt3U+rJFcdx+HvjYsgd+K4UONCkCxD3kAYcGFwqW/AF6Cg2bjIUmkzWWSRd5OAu+ig+AeEcRTdqG9As0jIGFwExsW9xzSdW32qq0796erngYLbcPlxTlMfalPVlQAAAAAAAAAAAAAAAAAAAAAAAAAAAFyEqyT3Gs164XYebMJVkneSPEnylZGzXkzytyRvRSRsQInj2e0xJpISR5klEs7aYRxjIjmMQyScta44hkTSFYdIOEu1OE6JpBaHSDgrfePoE0nfOETCWTg1jmORnBqHSFi1oXHcFcnQOETCKo2NYz+SlzMuDpGwKq3ieJbkgyT/bDRLJCxuzXGIhEWdQxwiYRHnFIdImNU5xiESZnHOcYiESW0hDpEwiS3FIRKa2mIcIqGJLcchEka5hDhEwiAt4/hXkp82miUSFtc6jldu577RaKZIWMxUcRRrj+RRkueHfnls29RxFGuN5FHa/W4XGzNXHMXaIhEHneaOo1hLJOKg01JxFEtHIg46LR1HsVQk4qDTWuIo5o5EHHRaWxzFXJGIg05rjaOYOhJx0Kn1vVXfn2idU0XyOMk3J1ozZ651HH9P8jTJtyZab+tIniT5JG3eT8IGvZTk47SLo3w+h0hKHPufRcLnPMi4SA7jOIdIDuMQCUcNjaQrjjVH0hWHSDjq1EhqcawxklocIuGovpF8kOQfPf5vTZH0jUMkHFWL5NQ41hDJqXGIhKO6Ihkax5KRDI1DJBx1GMnYOJaIZGwc5fhTPFHIHUokreKYM5JWcTxL8vpEa2UDvpu2ccwRycOIgxm8mOSvaR/HlJF8O+JgBlPHMUUk4mAWc8XRMhJxMIu542gRiTiYxdj3kC8RiTiYxdJxDInktYiDGawljlMiEQezWFscfSJpGcePB3xnXIivZZ1xHIvElWPDnlt6AQc+SfLvpRdxxHWSX+SzSF5L8l6SLzaY/Zsk34jXGlBxLzc/e7P01aJ2Jflh2l05fr3399sRCRVrj+RxpolDJPS21kimjkMk9La2SOaKQyT0tpZI5o5DJPR2nWUjWSoOkdDbUpEsHYdI6G3uSNYSh0joba5I1haHSOht6kjWGodI6O06ya+y7jh+kptbSKaIWCRUtY6kZRzlrtwXIhIW1CqS93PzNOBHDWb96GCNImFRYyN5/3ZGkryacZEcxlGIhEUNjWQ/jmJoJF1xFCJhUddJfplxcRSnRlKLoxAJi+obybE4ir6R9I2jEAmLqkXSJ46iFsmpcRQiYVFdkZwSR9EVydA4CpGwqMNIhsRRHEYyNo5CJCyqRDImjuLVJB+mXRyFSFjUdcbHUXy10ZxDIoEKkUCFSKBCJFAhEqgQCVSIBCpEAhUigQqRQIVIoEIkUCESqBAJVIgEKkQCFSKBCpFAhUigQiRQIRKoEAlUfCnJHyMS+JyrJO8keZrkSUQC/1fiKCeySODWYRwigVtdcYiEi1eLQyRcrL5xiISLc2ocIuFiDI1DJGze2DhEwma1ikMkbNLDtD+RnyZ5PMFckTC7B0k+TtuT+FGSr8cNjmxEy0geJbl3O9ddwGxGi0j24yhEwmaMieSuOAqRsBlDIjkWRyESNuOUSPrEUYiEzegTySlxFCJhM45FMiSOQiRsxl2RjImjEAmbsR9JizgKkbAZD5K8m3ZxFBcRyXNLL4DJ/TbJ93Jzn1VLX0jyaZK/NJ6bJP/NTShwlr6c5A+5OYn/k+TPaXf12M23DWhvP45ytIpkN982oL274mgVyW6+bUB7x+IYG8luvm1Ae/dTj2NoJLv5tgHtnRLHqZHs5tsGtDckjr6R7ObbBrQ3Jo5aJLv5tgHttYijK5LdfNuA9lrGcRjJbr5tQHtTxFGOn824D2jufpLfZ5o4dvNtA9oTB3QQB3QQB3SYMo6fz7gPaO5+kt9lujhW8TQgDCEO6CAO6CAO6CAOOOIHmSaONyMONuAqyVsRB3RqGYk42KQWkYiDTRsTiTi4CEMieRhxcEFOiUQcXKQ+kYiDi3YsEnFA7o5EHLBnPxJxwB2uknwn4gAAAAAAAAAAAAAAAAAAAG78D3RBZn+BbaFgAAAAAElFTkSuQmCC').GetIcon()
        self.SetIcon(icon)
        self.notif = Notification()
        self.notebook_1 = wx.Notebook(self, wx.ID_ANY)
        self.notebook_1_pane_2 = wx.Panel(self.notebook_1, wx.ID_ANY)
        self.window_1 = wx.SplitterWindow(self.notebook_1_pane_2, wx.ID_ANY)
        self.window_1_pane_1 = wx.Panel(self.window_1, wx.ID_ANY)
        self.window_2 = wx.SplitterWindow(self.window_1_pane_1, wx.ID_ANY, style=wx.SP_3D | wx.SP_THIN_SASH)
        self.window_2_pane_1 = wx.Panel(self.window_2, wx.ID_ANY)
        self.chatbox_show_textctrl = wx.TextCtrl(self.window_2_pane_1, wx.ID_ANY, "", style=wx.TE_MULTILINE | wx.TE_READONLY)
        self.window_2_pane_2 = wx.Panel(self.window_2, wx.ID_ANY)
        self.list_ctrl_1 = wx.ListCtrl(self.window_2_pane_2, wx.ID_ANY, style=wx.LC_HRULES | wx.LC_REPORT | wx.LC_VRULES)
        self.window_1_pane_2 = wx.Panel(self.window_1, wx.ID_ANY)
        self.window_3 = wx.SplitterWindow(self.window_1_pane_2, wx.ID_ANY)
        self.window_3_pane_1 = wx.Panel(self.window_3, wx.ID_ANY)
        self.chatbox_input_textctrl = wx.TextCtrl(self.window_3_pane_1, wx.ID_ANY, "", style=wx.TE_MULTILINE | wx.TE_PROCESS_ENTER)
        self.window_3_pane_2 = wx.Panel(self.window_3, wx.ID_ANY)
        self.window_6 = wx.SplitterWindow(self.window_3_pane_2, wx.ID_ANY)
        self.window_6_pane_1 = wx.Panel(self.window_6, wx.ID_ANY)
        self.window_7 = wx.SplitterWindow(self.window_6_pane_1, wx.ID_ANY)
        self.window_7_pane_1 = wx.Panel(self.window_7, wx.ID_ANY)
        self.send_button = wx.Button(self.window_7_pane_1, wx.ID_ANY, "SEND")
        self.window_7_pane_2 = wx.Panel(self.window_7, wx.ID_ANY)
        self.check_key_button = wx.Button(self.window_7_pane_2, wx.ID_ANY, "🔑")
        self.window_6_pane_2 = wx.Panel(self.window_6, wx.ID_ANY)
        self.window_8 = wx.SplitterWindow(self.window_6_pane_2, wx.ID_ANY)
        self.window_8_pane_1 = wx.Panel(self.window_8, wx.ID_ANY)
        self.send_file_button = wx.Button(self.window_8_pane_1, wx.ID_ANY, "FILE")
        self.window_8_pane_2 = wx.Panel(self.window_8, wx.ID_ANY)
        self.ban_unban_button = wx.Button(self.window_8_pane_2, wx.ID_ANY, "BAN/UNBAN")
        self.notebook_1_TRACKERS = wx.Panel(self.notebook_1, wx.ID_ANY)
        self.window_5 = wx.SplitterWindow(self.notebook_1_TRACKERS, wx.ID_ANY)
        self.window_5_pane_1 = wx.Panel(self.window_5, wx.ID_ANY)
        self.tracker_input_textctrl = wx.TextCtrl(self.window_5_pane_1, wx.ID_ANY, "", style=wx.TE_MULTILINE)
        self.window_5_pane_2 = wx.Panel(self.window_5, wx.ID_ANY)
        self.spin_ctrl_1 = wx.SpinCtrl(self.window_5_pane_2, wx.ID_ANY, "5", min=1, max=180)
        self.button_2 = wx.Button(self.window_5_pane_2, wx.ID_ANY, "Announce/Parse")
        self.notebook_1_SETTINGS = wx.Panel(self.notebook_1, wx.ID_ANY)
        self.private_key_button = wx.Button(self.notebook_1_SETTINGS, wx.ID_ANY, "Select private key file...")
        self.nickname_textctrl = wx.TextCtrl(self.notebook_1_SETTINGS, wx.ID_ANY, "")
        self.ok_nickname_button = wx.Button(self.notebook_1_SETTINGS, wx.ID_ANY, "Change/Set nickname")
        self.button_3 = wx.Button(self.notebook_1_SETTINGS, wx.ID_ANY, "Generate private key... (4096 bits)")
        self.button_4 = wx.Button(self.notebook_1_SETTINGS, wx.ID_ANY, "Apply")
        self.show_notifications_checkbox = wx.CheckBox(self.notebook_1_SETTINGS, wx.ID_ANY, "Show notifications")
        self.stop_listening_service_checkbox = wx.CheckBox(self.notebook_1_SETTINGS, wx.ID_ANY, "Stop listening service")
        self.dont_allow_smaller_priv_checkbox = wx.CheckBox(self.notebook_1_SETTINGS, wx.ID_ANY, "Don't allow private keys with size smaller than 4096 bits")
        self.verbose_logging_checkbox = wx.CheckBox(self.notebook_1_SETTINGS, wx.ID_ANY, "Show encrypted message in chat window")
        self.allow_files_checkbox = wx.CheckBox(self.notebook_1_SETTINGS, wx.ID_ANY, "Enable file reception service")

        self.__set_properties()
        self.__do_layout()
        self.__set_vars()

        self.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.on_item_right_click, self.list_ctrl_1)
        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.on_item_selected, self.list_ctrl_1)
        self.Bind(wx.EVT_TEXT, self.on_text_change, self.chatbox_input_textctrl)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_enter_pressed, self.chatbox_input_textctrl)
        self.Bind(wx.EVT_BUTTON, self.on_send_press, self.send_button)
        self.Bind(wx.EVT_BUTTON, self.on_checkkey_press, self.check_key_button)
        self.Bind(wx.EVT_BUTTON, self.on_file_press, self.send_file_button)
        self.Bind(wx.EVT_BUTTON, self.on_ban_press, self.ban_unban_button)
        self.Bind(wx.EVT_SPINCTRL, self.on_update_interval_change, self.spin_ctrl_1)
        self.Bind(wx.EVT_BUTTON, self.on_announce_press, self.button_2)
        self.Bind(wx.EVT_BUTTON, self.on_private_key_file_button_press, self.private_key_button)
        self.Bind(wx.EVT_BUTTON, self.on_ok_nickname_button, self.ok_nickname_button)
        self.Bind(wx.EVT_BUTTON, self.on_generate_private_key_press, self.button_3)
        self.Bind(wx.EVT_BUTTON, self.on_apply_button, self.button_4)
        self.Bind(wx.EVT_CHECKBOX, self.on_stop_listening_service, self.stop_listening_service_checkbox)
        self.Bind(wx.EVT_CHECKBOX, self.on_dont_allow_small_keys, self.dont_allow_smaller_priv_checkbox)
        self.Bind(wx.EVT_CHECKBOX, self.on_allow_files, self.allow_files_checkbox)
        # end wxGlade

    def __set_vars(self):
        #self.pool = ThreadPool(processes=1)
        self.CHAT_PORT = 3333
        self.FILES_PORT = 6666
        self.DOWNLOADS_DIRECTORY = 'received_files'
        self.socket_has_been_created = False
        self.banned_peers = []
        self.this_selected_ip = None
        self.stop_the_thread = False
        self.public_key = ""
        self.nickname = ""
        self.update_interval = 1
        self.tracker_count = 0
        self.is_running_the_thread = False
        self.tracker_list = []
        self.is_okay_to_spam_server = False
        self.peer_list = []

    def __set_properties(self):
        # begin wxGlade: main_frame.__set_properties
        self.SetTitle("Ｍｅｓｓｅｎｇｕｒｅ")
        self.list_ctrl_1.InsertColumn(0, "Peers", format=wx.LIST_FORMAT_LEFT, width=130)
        self.list_ctrl_1.InsertColumn(1, "Nick", format=wx.LIST_FORMAT_LEFT, width=130)
        self.window_2.SetMinimumPaneSize(20)
        self.window_7.SetMinimumPaneSize(20)
        self.window_8.SetMinimumPaneSize(20)
        self.window_6.SetMinimumPaneSize(20)
        self.window_3.SetMinimumPaneSize(20)
        self.window_1.SetMinimumPaneSize(20)
        self.window_5.SetMinimumPaneSize(20)
        self.show_notifications_checkbox.SetValue(1)
        self.dont_allow_smaller_priv_checkbox.SetValue(1)
        # end wxGlade

    def __do_layout(self):
        # begin wxGlade: main_frame.__do_layout
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_15 = wx.BoxSizer(wx.VERTICAL)
        sizer_17 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_16 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_12 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_14 = wx.BoxSizer(wx.VERTICAL)
        sizer_13 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_4 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_5 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_9 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_11 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_22 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_21 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_10 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_20 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_19 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_6 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_3 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_8 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_7 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_7.Add(self.chatbox_show_textctrl, 1, wx.ALIGN_BOTTOM | wx.ALIGN_CENTER | wx.ALIGN_RIGHT | wx.ALL | wx.EXPAND, 0)
        self.window_2_pane_1.SetSizer(sizer_7)
        sizer_8.Add(self.list_ctrl_1, 1, wx.EXPAND, 0)
        self.window_2_pane_2.SetSizer(sizer_8)
        self.window_2.SplitVertically(self.window_2_pane_1, self.window_2_pane_2, 760)
        sizer_3.Add(self.window_2, 1, wx.EXPAND, 0)
        self.window_1_pane_1.SetSizer(sizer_3)
        sizer_6.Add(self.chatbox_input_textctrl, 1, wx.ALIGN_BOTTOM | wx.ALIGN_CENTER | wx.ALIGN_RIGHT | wx.ALL | wx.EXPAND, 0)
        self.window_3_pane_1.SetSizer(sizer_6)
        sizer_19.Add(self.send_button, 1, wx.ALIGN_BOTTOM | wx.ALIGN_CENTER | wx.ALIGN_RIGHT | wx.ALL | wx.EXPAND, 0)
        self.window_7_pane_1.SetSizer(sizer_19)
        sizer_20.Add(self.check_key_button, 1, wx.ALIGN_BOTTOM | wx.ALIGN_CENTER | wx.ALIGN_RIGHT | wx.ALL | wx.EXPAND, 0)
        self.window_7_pane_2.SetSizer(sizer_20)
        self.window_7.SplitVertically(self.window_7_pane_1, self.window_7_pane_2, 50)
        sizer_10.Add(self.window_7, 1, wx.EXPAND, 0)
        self.window_6_pane_1.SetSizer(sizer_10)
        sizer_21.Add(self.send_file_button, 0, wx.ALIGN_BOTTOM | wx.ALIGN_CENTER | wx.ALIGN_RIGHT | wx.ALL | wx.EXPAND, 0)
        self.window_8_pane_1.SetSizer(sizer_21)
        sizer_22.Add(self.ban_unban_button, 0, wx.ALIGN_BOTTOM | wx.ALIGN_CENTER | wx.ALIGN_RIGHT | wx.ALL | wx.EXPAND, 0)
        self.window_8_pane_2.SetSizer(sizer_22)
        self.window_8.SplitVertically(self.window_8_pane_1, self.window_8_pane_2, 35)
        sizer_11.Add(self.window_8, 1, wx.EXPAND, 0)
        self.window_6_pane_2.SetSizer(sizer_11)
        self.window_6.SplitVertically(self.window_6_pane_1, self.window_6_pane_2, 90)
        sizer_9.Add(self.window_6, 1, wx.EXPAND, 0)
        sizer_5.Add(sizer_9, 1, wx.EXPAND, 0)
        self.window_3_pane_2.SetSizer(sizer_5)
        self.window_3.SplitVertically(self.window_3_pane_1, self.window_3_pane_2, 760)
        sizer_4.Add(self.window_3, 1, wx.EXPAND, 0)
        self.window_1_pane_2.SetSizer(sizer_4)
        self.window_1.SplitHorizontally(self.window_1_pane_1, self.window_1_pane_2, 416)
        sizer_2.Add(self.window_1, 1, wx.EXPAND, 0)
        self.notebook_1_pane_2.SetSizer(sizer_2)
        sizer_13.Add(self.tracker_input_textctrl, 1, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 0)
        self.window_5_pane_1.SetSizer(sizer_13)
        label_1 = wx.StaticText(self.window_5_pane_2, wx.ID_ANY, "Update inverval (s):", style=wx.ALIGN_CENTER)
        sizer_14.Add(label_1, 0, wx.ALIGN_CENTER | wx.ALL, 10)
        sizer_14.Add(self.spin_ctrl_1, 0, wx.ALIGN_CENTER | wx.EXPAND | wx.LEFT | wx.RIGHT, 11)
        sizer_14.Add(self.button_2, 0, wx.ALIGN_CENTER | wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, 11)
        self.server_status_label = wx.StaticText(self.window_5_pane_2, wx.ID_ANY, "Status: Waiting for servers...")
        self.server_status_label.Wrap(101)
        sizer_14.Add(self.server_status_label, 0, wx.ALIGN_CENTER | wx.ALL | wx.EXPAND, 19)
        self.window_5_pane_2.SetSizer(sizer_14)
        self.window_5.SplitVertically(self.window_5_pane_1, self.window_5_pane_2, 832)
        sizer_12.Add(self.window_5, 1, wx.EXPAND, 0)
        self.notebook_1_TRACKERS.SetSizer(sizer_12)
        sizer_16.Add(self.private_key_button, 1, wx.ALL | wx.EXPAND, 12)
        self.tracker_label = wx.StaticText(self.notebook_1_SETTINGS, wx.ID_ANY, "Status: waiting for private key file....")
        sizer_16.Add(self.tracker_label, 1, wx.ALL, 13)
        sizer_15.Add(sizer_16, 1, wx.EXPAND, 0)
        sizer_17.Add(self.nickname_textctrl, 2, wx.ALL | wx.EXPAND, 12)
        sizer_17.Add(self.ok_nickname_button, 1, wx.BOTTOM | wx.RIGHT | wx.TOP, 13)
        sizer_15.Add(sizer_17, 1, wx.EXPAND, 0)
        sizer_15.Add(self.button_3, 1, wx.ALL | wx.EXPAND, 13)
        sizer_15.Add(self.button_4, 1, wx.ALL | wx.EXPAND, 13)
        sizer_15.Add(self.show_notifications_checkbox, 0, wx.ALIGN_CENTER | wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, 10)
        sizer_15.Add(self.stop_listening_service_checkbox, 0, wx.ALIGN_CENTER | wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, 10)
        sizer_15.Add(self.dont_allow_smaller_priv_checkbox, 0, wx.ALIGN_CENTER | wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, 10)
        sizer_15.Add(self.verbose_logging_checkbox, 0, wx.ALIGN_CENTER | wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, 10)
        sizer_15.Add(self.allow_files_checkbox, 0, wx.ALIGN_CENTER | wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, 10)
        try:    self.own_ip = "    |    " + get_own_ip()
        except: self.own_ip = ""
        label_5 = wx.StaticText(self.notebook_1_SETTINGS, wx.ID_ANY, u"Made with ❤ @ ESCOM"+self.own_ip)
        self.own_ip = str((([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")] or [[(s.connect(("8.8.8.8", 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) + ["no IP found"])[0])
        sizer_15.Add(label_5, 1, wx.ALIGN_CENTER | wx.ALL, 13)
        self.notebook_1_SETTINGS.SetSizer(sizer_15)
        self.notebook_1.AddPage(self.notebook_1_pane_2, "    CHAT    ")
        self.notebook_1.AddPage(self.notebook_1_TRACKERS, "    TRACKERS    ")
        self.notebook_1.AddPage(self.notebook_1_SETTINGS, "    SETTINGS    ")
        sizer_1.Add(self.notebook_1, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        self.Layout()
        # end wxGlade

    def __is_peer_in_blacklist(self,client):
        client_ip = client.getpeername()[0] 
        if client_ip.startswith("127"):
            client_ip = get_own_ip()
        #Hacer un drop de su conexión si es que lo hemos baneado
        for peer in self.banned_peers:
            if peer[0] == client_ip:
                if peer[1] is False:    
                    print("Peer in blacklist! This ip is going to be dropped:",client_ip)
                    return True
        return client_ip
                    
                    

    def __listen_to_client(self,client,widget=None):
        #Obtener IP de este cliente que se ha conectado
        result = self.__is_peer_in_blacklist(client)
        if result == True:
            client.close()
            return
        else:
            client_ip = result

        try:
            data = recv_data(client)
            if data:
                nick = ""
                for ip in self.peer_list:
                    if ip[0]==client_ip:    nick = ip[1]
                if(self.verbose_logging_checkbox.GetValue()):
                    wx.CallAfter(self.chatbox_show_textctrl.AppendText,\
                        client_ip +" | [RAW DATA]> " + \
                        str(data) +"\n") if(len(data)<=500) else \
                        wx.CallAfter(self.chatbox_show_textctrl.AppendText,\
                        client_ip +" | [RAW DATA]> " + \
                        str(data[:500]) +"... (output truncated).\n")
                data = pickle.loads(decrypt_data(data,self.private_key))
                #send_data(client,pickle.dumps("OK"))
                


                #PONER CONTENIDO EN LA CAJA DE TEXTO DEL CHAT (LO QUE SE RECIBE)
                wx.CallAfter(self.chatbox_show_textctrl.AppendText,\
                    client_ip + " (" + nick + ") > " + \
                    data +"\n")
                if(self.show_notifications_checkbox.GetValue()): self.notif.send_notification(nick, data)

                #SCROLL HASTA LA ÚLTIMA POSICIÓN
                self.chatbox_show_textctrl.ShowPosition(self.chatbox_show_textctrl.GetLastPosition())
                return
            else:  raise ValueError("Client disconnected")
        except Exception as e:
            print("ERROR:",e)
            client.close()                          
            return  

    def __listen_to_client_data(self,client,widget=None):
        result = self.__is_peer_in_blacklist(client)
        if result == True:
            client.close()
            return
        else:
            client_ip = result
        try:
            with color_set_to(self.chatbox_show_textctrl,"YELLOW"):
                wx.CallAfter(self.chatbox_show_textctrl.AppendText, "receiving data from"+ client_ip + "...\n")
            data = recv_data(client)
            if data:
                data = pickle.loads(decrypt_data(data,self.private_key))
                if(self.show_notifications_checkbox.GetValue()): self.notif.send_notification(client_ip, "You just received a file!")
                with color_set_to(self.chatbox_show_textctrl,"LIGHT GREEN"):
                    wx.CallAfter(self.chatbox_show_textctrl.AppendText, client_ip + " " + data.name+" / "+str(round(data.size/1024**2,6))+"MB [saved successfully]\n")
                    wx.CallAfter(self.chatbox_show_textctrl.AppendText, "real hash: "+ data.compute_hash() +"\nincluded hash: "+ data.hash +"\n")
                    final_name = random_string(6) + "_" + data.name
                    if(not isdir(self.DOWNLOADS_DIRECTORY)):
                        mkdir(self.DOWNLOADS_DIRECTORY)
                    file_descriptor = open(osjoin(self.DOWNLOADS_DIRECTORY, '') + final_name,'wb')
                    file_descriptor.write(data.get_contents())
                    file_descriptor.close()
                    print("File got written correctly")
                    self.chatbox_show_textctrl.AppendText(data.name + ": saved as: "+ final_name + " \n")
                    del data
                    print("OK")
                    return
                self.chatbox_show_textctrl.ShowPosition(self.chatbox_show_textctrl.GetLastPosition())
            else:  raise ValueError("Client disconnected")
        except Exception as e:
            print("this_ERROR:",e)
        finally:
            client.close()
            return 

    def __start_listening_service(self,widget=None):
        print("LISTENING SERVICE STARTED")
        incoming_data_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
        incoming_data_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        incoming_files_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
        incoming_files_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        #
        try:    
            incoming_data_socket.bind(("0.0.0.0",self.CHAT_PORT))
            incoming_files_socket.bind(("0.0.0.0",self.FILES_PORT))
        except Exception as e: print(str(e))

        #
        incoming_data_socket.listen(self.CHAT_PORT)
        incoming_files_socket.listen(self.FILES_PORT)

        def accept_connections_for_messaging_socket(self,socket):
            while True:
                client, address = socket.accept()  
                if(self.stop_the_thread):
                    print("[MESSAGING] Discarding all connections for now: ",client.getpeername()[0])
                    client.close()
                    continue
                threading.Thread(target = self.__listen_to_client, args = (client,)).start() #thread

        def accept_connections_for_data_socket(self,socket):
            while True:
                client, address = socket.accept()  
                if(not self.allow_files_checkbox.GetValue()):
                    print("[DATA] Discarding all connections for now: ",client.getpeername()[0])
                    client.close()
                    continue
                threading.Thread(target = self.__listen_to_client_data, args = (client,)).start() #thread

        threading.Thread(target = accept_connections_for_messaging_socket, args = (self,incoming_data_socket,)).start() #thread
        threading.Thread(target = accept_connections_for_data_socket, args = (self,incoming_files_socket,)).start() #thread
            
    def on_item_right_click(self, event):  # wxGlade: main_frame.<event_handler>
        print("Event handler 'on_item_right_click' not implemented!")
        event.Skip()

    def on_item_selected(self, event):  # wxGlade: main_frame.<event_handler>
        self.this_selected_ip = self.list_ctrl_1.GetItemText(self.list_ctrl_1.GetFocusedItem(),0)

    def on_enter_pressed(self, event):  # wxGlade: main_frame.<event_handler>
        if(len(self.chatbox_input_textctrl.GetValue())>0):
            if(self.this_selected_ip):
                for peer in self.peer_list:
                    if(peer[0]==self.this_selected_ip):
                        print("Sending message to:",self.this_selected_ip)
                        with color_set_to(self.chatbox_show_textctrl,"LIGHT BLUE"):
                            wx.CallAfter(self.chatbox_show_textctrl.AppendText, "[ YOU -> "+peer[1]+" ]: "+\
                                self.chatbox_input_textctrl.GetValue()+"\n")
                        send_secure_message(self.chatbox_input_textctrl.GetValue(),self.this_selected_ip,peer[2],self.CHAT_PORT)
                self.chatbox_input_textctrl.SetValue("")
                

    def on_send_press(self, event):     self.on_enter_pressed(self)

    def on_checkkey_press(self, event):  # wxGlade: main_frame.<event_handler>
        if(self.this_selected_ip):
            for peer in self.peer_list:
                if peer[0]==self.this_selected_ip:
                    public_key = peer[2]
                    random_art = get_random_art(public_key)
                    with color_set_to(self.chatbox_show_textctrl,wx.CYAN):
                        wx.CallAfter(self.chatbox_show_textctrl.AppendText, "[ KEY ART FOR "+peer[0]+" ]\n"+\
                                random_art+"\n")
    
    def on_file_press(self, event):  # wxGlade: main_frame.<event_handler>
        with wx.FileDialog(self, "Send files...", wildcard="Any file (*.*)|*.*",style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:
                if fileDialog.ShowModal() == wx.ID_CANCEL:  
                    return
                else:
                    try:
                        FileObject = File(fileDialog.GetPath())
                        if(self.this_selected_ip):
                            for peer in self.peer_list:
                                if(peer[0]==self.this_selected_ip):
                                    print("Sending file to:",self.this_selected_ip)
                                    with color_set_to(self.chatbox_show_textctrl,"white"):
                                        wx.CallAfter(self.chatbox_show_textctrl.AppendText, "[ YOU -> "+peer[1]+" ]: "+\
                                            FileObject.name +"\n")
                                    send_secure_message(FileObject,self.this_selected_ip,peer[2],self.FILES_PORT)
                                    with color_set_to(self.chatbox_show_textctrl,"LIGHT GREEN"):
                                        wx.CallAfter(self.chatbox_show_textctrl.AppendText, "[ YOU -> "+peer[1]+" ]: "+\
                                            FileObject.name +" OK\n")
                                    del FileObject
                    except Exception as e:  pass



    def on_ban_press(self, event):  # wxGlade: main_frame.<event_handler>
        if self.this_selected_ip is not None and self.this_selected_ip not in [ip[0] for ip in self.banned_peers]:
            self.banned_peers.append([self.this_selected_ip,True])
        #Avisar que este peer se ha baneado en la consola

        for peer in self.banned_peers:
            if peer[0]==self.this_selected_ip:
                peer[1] = not peer[1]
                if peer[1] is False:
                    with color_set_to(self.chatbox_show_textctrl,"RED"):
                        wx.CallAfter(self.chatbox_show_textctrl.AppendText,peer[0]+" has been BLOCKED.\n")
                else:
                    with color_set_to(self.chatbox_show_textctrl,"GREEN"):
                        wx.CallAfter(self.chatbox_show_textctrl.AppendText,peer[0]+" has been UNBLOCKED.\n")
                self.chatbox_show_textctrl.ShowPosition(self.chatbox_show_textctrl.GetLastPosition())
                #No permitir que existan entradas repetidas en la lista de baneados
        print("BANNED PEERS:",self.banned_peers)


    def on_update_interval_change(self, event):  # wxGlade: main_frame.<event_handler>
        self.update_interval = int(self.spin_ctrl_1.GetValue())

    def update_peer_list_in_ui(self):
        ip_list = []
        while True:
            #Agregar todas las IPs a la lista de peers 
            for ip_pair in self.peer_list:
                if(ip_pair[0]) not in [self.list_ctrl_1.GetItemText(entry) for entry in range(self.list_ctrl_1.GetItemCount())]:
                    index = self.list_ctrl_1.InsertItem(999,str(ip_pair[0]))  #IP para este cliente
                    wx.CallAfter(self.list_ctrl_1.SetItem,index, 1, ip_pair[1]) #Nick

            #Obtener una lista de todo lo que está en la lista de peers (sólo IPs)
            ui_ip_list = [self.list_ctrl_1.GetItemText(entry) for entry in range(self.list_ctrl_1.GetItemCount())]
            
            #Eliminar aquellas entradas que estén en nuestra lista de IPs en la GUI pero 
            #que no estén en la lista que obtenemos de los trackers
            for ui_ip in ui_ip_list:
                if ui_ip not in [ip_field[0] for ip_field in self.peer_list]:
                    for entry in range(self.list_ctrl_1.GetItemCount()):
                        if(self.list_ctrl_1.GetItemText(entry)==ui_ip):
                            wx.CallAfter(self.list_ctrl_1.DeleteItem,entry)
            time.sleep(self.update_interval)

    def register_in_every_tracker(self):
        self.is_running_the_thread = True
        peer_list_temp = []
        while True:
            while self.is_okay_to_spam_server:
                #Conectar a cada IP (tracker)
                peer_list_temp = []
                for ip_pair in self.tracker_list:
                    temporary_peer_list = []
                    try:    tracker_response = pickle.loads(SendCommand(ip_pair[0],ip_pair[1],["Register",{"nick":self.nickname, "pubkey":self.public_key}]))
                    except: tracker_response = None
                    #Si la respuesta a este tracker es nula, eliminarlo de nuestra lista de trackers
                    if tracker_response == None:
                        print("Removing faulty tracker...",ip_pair)
                        try:    
                            self.tracker_list.remove(ip_pair)
                            print("OK: Removed:",ip_pair,self.tracker_list)
                            if(len(self.tracker_list)==0):
                                wx.CallAfter(wx.MessageBox,"None of the trackers seem to be available. Please check them out.\n(Announce thread shutting down).", "Hell no!",  wx.OK | wx.ICON_ERROR)
                                wx.CallAfter(self.server_status_label.SetLabel,("Status: connected to \n"+str(self.tracker_count))+" trackers.")
                                self.is_running_the_thread = False
                                return 
                        except: pass
                    #Si el tracker responde correctamente a nuestra petición
                    if tracker_response == "OK":
                        self.tracker_count += 1
                        #Esperar a que el tracker responda nuestra petición de recibir la lista
                        data_from_tracker = pickle.loads(SendCommand(ip_pair[0],ip_pair[1],["GetList"]))
                        #Eliminar nuestra propia IP en caso de que el cliente se ejecute localmente junto con el servidor
                        entry_with_own_ip = search_in_list(data_from_tracker,get_own_ip(),"ip")
                        if entry_with_own_ip != None:   data_from_tracker.remove(entry_with_own_ip)
                        #Agregar incrementalmente cada entrada recibida del tracker sin repeticiones
                        for entry in data_from_tracker:
                            if entry not in temporary_peer_list:    temporary_peer_list.append(entry)

                #Lista que únicamente contiene las temporaryIP 
                list_with_ip = []
                #Filtrar todas las IPs repetidas (equivalente a usar set()) de la lista que contiene diccionarios
                self.ip_list = list({l['ip']:l for l in temporary_peer_list}.values())
                #Agregar cada ip a la lista de peers si es que no está ahí
                for ip in self.ip_list:
                    if ip not in peer_list_temp:
                        peer_list_temp.append([ip["ip"],ip["nick"],ip["pubkey"],True])
                print("Updating list every ",self.update_interval,"s  with ",\
                    self.tracker_count," trackers. (",len(peer_list_temp)," peers).",\
                    sep="")
                wx.CallAfter(self.server_status_label.SetLabel,("Status: connected to \n"+str(self.tracker_count))+" tracker(s).")
                self.tracker_count = 0
                #self.peer_list es la lista que se usa en las demás partes del código, actualizarla hasta el final
                self.peer_list = peer_list_temp
                time.sleep(self.update_interval)            
                
    def on_announce_press(self, event):  # wxGlade: main_frame.<event_handler>
        tracker_box_text = self.tracker_input_textctrl.GetValue()
        tracker_box_text = tracker_box_text.split(',')
        for entry in tracker_box_text:
            if(":" not in entry):
                wx.MessageBox("Your IP list seems to be corrupted or empty. Please check it again.", "Hell no!",  wx.OK | wx.ICON_ERROR)  
                self.is_okay_to_spam_server = False
                return
            else:
                try:
                    ip = entry.split(':')[0]
                    port = int(entry.split(':')[1])
                    entry = [ip,port]
                    if(entry not in self.tracker_list):
                        self.tracker_list.append(entry)
                    self.is_okay_to_spam_server = True
                    if(not self.is_running_the_thread):
                        threading.Thread(target = self.register_in_every_tracker).start()
                        threading.Thread(target = self.update_peer_list_in_ui).start()
                except:
                    self.is_okay_to_spam_server = False
                    wx.MessageBox("Your IP list seems to be corrupted or empty. Please check it again.", "Hell no!",  wx.OK | wx.ICON_ERROR)  


    def on_private_key_file_button_press(self, event):  # wxGlade: main_frame.<event_handler>
        with wx.FileDialog(self, "Open your private key file", wildcard="RSA private key (*.pem)|*.pem",style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:
                if fileDialog.ShowModal() == wx.ID_CANCEL:  
                    return
                else:
                    try:
                        with open(fileDialog.GetPath(),'rb') as private_key_descriptor:
                            rsa_priv_key_file = private_key_descriptor.read()
                            try:    
                                key = RSA.import_key(rsa_priv_key_file)
                                if not key.has_private():
                                    wx.MessageBox("Your key file is not a private key.", "Hell no!",  wx.OK | wx.ICON_ERROR)  
                                    return 
                            except:
                                wx.MessageBox("Your key file is not a private key.", "Hell no!",  wx.OK | wx.ICON_ERROR)  
                                return
                            self.private_key = rsa_priv_key_file
                            keysize = key.size_in_bits()
                            if self.dont_allow_smaller_priv_checkbox.GetValue() == True and keysize < 4096:
                                wx.MessageBox("Your key doesn't match the required size (4096).", "Hell no!",  wx.OK | wx.ICON_ERROR)  
                                return 
                            self.public_key =   key.publickey().export_key()
                            if(not self.socket_has_been_created):
                                threading.Thread(target = self.__start_listening_service).start()
                                self.socket_has_been_created = True
                            self.tracker_label.SetLabel(" OK! Your private key seems to be a valid file. Size: "+str(keysize)+" bits.")

                    except Exception as e: 
                        wx.MessageBox("Something wrong happened:"+str(e), "Hell no!",  wx.OK | wx.ICON_ERROR)  

    def on_ok_nickname_button(self, event):  # wxGlade: main_frame.<event_handler>
        nickname = self.nickname_textctrl.GetValue()
        print(nickname)
        if(len(nickname) > 0):  
            self.nickname = nickname 
            wx.MessageBox("OK", "Hell no!",  wx.OK | wx.ICON_ERROR)  
        else:
            wx.MessageBox("Your nickname can't be an empty string!", "Hell no!",  wx.OK | wx.ICON_ERROR)  
    
    def on_generate_private_key_press(self, event):  # wxGlade: main_frame.<event_handler>
        final_name = GeneratePriVateKey()
        wx.MessageBox("Done. Private key saved as: "+final_name, "Hell no!",  wx.OK | wx.ICON_ERROR)  

    def on_apply_button(self, event):  # wxGlade: main_frame.<event_handler>
        print("Event handler 'on_apply_button' not implemented!")
        event.Skip()

    def on_text_change(self,event):
        if(not len(self.chatbox_input_textctrl.GetValue()) > 1):
            self.send_button.Disable()
        else:
            self.send_button.Enable()

    def on_stop_listening_service(self, event):  self.stop_the_thread = self.stop_listening_service_checkbox.GetValue()

    def on_dont_allow_small_keys(self, event):  # wxGlade: main_frame.<event_handler>
        print("Event handler 'on_dont_allow_small_keys' not implemented!")
        event.Skip()


    def on_allow_files(self,event):
        #Habilitar el servicio de recepción de archivos
        pass



# end of class main_frame

class MyApp(wx.App):
    def OnInit(self):
        self.frame = main_frame(None, wx.ID_ANY, "")
        self.SetTopWindow(self.frame)
        self.frame.Show()
        return True

# end of class MyApp

if __name__ == "__main__":
    app = MyApp(0)
    app.MainLoop()
