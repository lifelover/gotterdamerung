#!/usr/bin/env python

import base64, hashlib
import numpy as np

def get_steps(bits):
    bits_grouped = np.array(bits, dtype=np.int8).reshape((-1, 4, 2))
    bits_grouped_reordered = np.flip(bits_grouped, axis=1)
    return bits_grouped_reordered.reshape((-1, 2))

def drunken_bishop(steps):
    positions = np.zeros((9, 17), dtype=np.int8)

    current_position = np.array([4, 8])


    def move(b0, b1):
        if (b0, b1) == (0, 0):
            return (-1, -1)
        elif (b0, b1) == (0, 1):
            return (-1, 1)
        elif (b0, b1) == (1, 0):
            return (1, -1)
        elif (b0, b1) == (1, 1):
            return (1, 1)
        raise Exception('Impossible move: ({}, {})'.format(b0, b1))


    for step in steps:
        positions[tuple(current_position)] += 1
        current_position += move(step[0], step[1])
        if current_position[0] >= positions.shape[0]:
            current_position[0] = positions.shape[0] - 1
        elif current_position[0] <= 0:
            current_position[0] = 0
        if current_position[1] >= positions.shape[1]:
            current_position[1] = positions.shape[1] - 1
        elif current_position[1] <= 0:
            current_position[1] = 0

    positions[(4, 8)] = 15
    positions[tuple(current_position)] = 16
    return positions


def generate_randomart(atrium):
    values = {
        0: ' '
    }
    for i in range(33,127):
        values.update({i-32:chr(i)})
    random_art_str = ''
    random_art_str += '+--[   SIGN  ]----+\n'
    for row in atrium:
        random_art_str += '|'
        for character in row:
            random_art_str += values[character] 
        random_art_str += '|\n'
    random_art_str += '+-----------------+'
    return random_art_str


def get_md5_bits(fingerprint):
    return np.array([list('{:08b}'.format(int(i, 16))) for i in fingerprint.split(':')])


def get_sha256_bits(fingerprint):
    missing_padding = 4 - (len(fingerprint) % 4)
    fingerprint += '=' * missing_padding
    return np.array([list('{:08b}'.format(i)) for i in base64.b64decode(fingerprint)])


def get_random_art(binary_data):
    
    bits = get_sha256_bits(calculate_hash(binary_data))
    atrium = drunken_bishop(get_steps(bits))
    return generate_randomart(atrium)

def calculate_hash(data):
        message_hash = hashlib.sha256()
        message_hash.update(data)
        return message_hash.digest().hex()

if __name__ == '__main__':
    print(get_random_art(b'ASS'),end="")

