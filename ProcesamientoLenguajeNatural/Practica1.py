import nltk
from nltk.book import *
from nltk.corpus import brown

#Punto 1
fdist1 = FreqDist(text1)
fdist1.plot(1000, cumulative=False)

fdist1.plot(1000, cumulative=True)

#Punto 2
dict_text5 = set(list(text5))
longer_words = [word for word in dict_text5 if len(word)>=30]
print(longer_words)


cfd = nltk.ConditionalFreqDist(
        (genre, word)
        for genre in brown.categories()
        for word in brown.words(categories=genre)
        if genre in ["news", "romance", "humor"]
        if word in ["love", "hate", "speak", "control", "great", "president"])

cfd.tabulate()