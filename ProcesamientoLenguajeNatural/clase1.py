from nltk.book import text3

def main():
	vocabulary = sorted(set(text3))
	vocabulary_len = "Vocabulary len: {0}".format(len(vocabulary))
	print(vocabulary_len)
	with open("salidapy.txt","w") as fd:
		fd.write(vocabulary_len)
		fd.write("\n")
		[fd.write(word+"\n") for word in vocabulary]

if __name__=="__main__":
	main()
