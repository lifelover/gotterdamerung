import nltk
from nltk.corpus import brown

fileids=brown.fileids()
print('There are ', len(fileids), 'files in the Brown Corpus')

categories=brown.categories()
print('There are ', len(categories), 'categories in the Brown Corpus')

fname='cr09'

num_chars=len(brown.raw(fname))
num_words=len(brown.words(fname))
num_sentences=len(brown.sents(fname))

print('There are ', num_chars, 'characters in ', fname)
print('There are ', num_words, 'words in ', fname)
print('There are ', num_sentences, 'sentences in ', fname)


romance_text=brown.words(categories='romance')
fdist=nltk.FreqDist([w.lower() for w in romance_text])
print('The word \'love\' is used ', fdist['love'], 'times in the romance category.')
print('The word \'hate\' is used ', fdist['hate'], 'times in the romance category.')



cfd=nltk.ConditionalFreqDist((genre, word)

for genre in brown.categories()
for word in brown.words(categories=genre))

genres=['news', 'romance', 'humor']
ws=['love', 'hate']

cfd.tabulate(conditions=genres, samples=ws)
 