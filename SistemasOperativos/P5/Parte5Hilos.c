#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/wait.h>
#include <sys/types.h>
#define firstLevelThread 15
#define secondLevelThread 10
#define thirdLevelThread 5
void *hiloprincipal(void *data);
void *hiloSecundario(void *data);
void *hiloTerciario(void *data);
int ObtenerIDHilo(void );
struct datos {
    int iii;
}; //Estructuras para pasar datos entre proceso e hilos
struct datos2 {
    int ii;
    int hiloPadre;
};
struct datos3 {
    int iiii;
    int hiloPadre;
};
int main()
{
    printf("PID PADRE: %d\n",getpid());
    if(fork()==0) //Crear proceso hijo con fork
    {
    printf("PID HIJO: %d\n",getpid());
    pthread_t thread[firstLevelThread]; //Handlers de los primeros 15 hilos
    int hilonum[firstLevelThread];
        struct datos *datos = malloc(sizeof(struct datos));
    for (int i=0;i<firstLevelThread;i++)
            {
                datos->iii=i; //Pasa el numero de hilo que esta en ejecucion
                hilonum[i]=pthread_create(&(thread[i]),NULL, hiloprincipal, datos);
                if (hilonum[i])
                    printf("No fue posible crear un hilo.\n");
                pthread_join(thread[i], NULL); //Espera a que todos los hilos acaben
            }
    }
    wait(0);
    exit(0);
}

void *hiloprincipal(void *data)
{
    struct datos *datos = data;
    printf("[NIVEL 1] HiloN#: %d, Thread id: %d, Padre: %d\n",(datos->iii)+1,ObtenerIDHilo(),getpid());   
    pthread_t thread2[secondLevelThread]; //Handlers de los primeros 10 hilos
    int hilonum2[secondLevelThread];
    struct datos2 *datos2 = malloc(sizeof(struct datos2));
    for(int i=0;i<secondLevelThread;i++)
        {
            datos2->ii=i; //Num. de hilo
            datos2->hiloPadre=ObtenerIDHilo(); //Se pasa el id del hilo padre
            hilonum2[i]=pthread_create(&(thread2[i]),NULL, hiloSecundario, datos2);
            if(hilonum2[i])
                printf("No fue posible crear un hilo. \n");
            pthread_join(thread2[i], NULL); //Espera a que todos los hilos acaben
         }
    pthread_exit(NULL); //Sale del hilo
}
void *hiloSecundario(void *data)
{
    struct datos2 *datos2 = data;
    printf(" |--[NIVEL 2] HiloN#: %d, Thread id: %d, HiloPadre: %d\n",(datos2->ii)+1,ObtenerIDHilo(),(datos2->hiloPadre));   
    pthread_t thread3[thirdLevelThread]; //Handlers de los primeros 5 hilos
    int hilonum3[thirdLevelThread];
    struct datos3 *datos3 = malloc(sizeof(struct datos3));
    for(int i=0;i<thirdLevelThread;i++)
        {
            datos3->iiii=i;
            datos3->hiloPadre=ObtenerIDHilo();
            hilonum3[i]=pthread_create(&(thread3[i]),NULL, hiloTerciario, datos3);
            if(hilonum3[i])
                printf("No fue posible crear un hilo. \n");
            pthread_join(thread3[i], NULL); //Espera a que todos los hilos acaben
         }
    pthread_exit(NULL); //Sale del hilo
}
void *hiloTerciario(void *data)
{
    struct datos3 *datos3 = data;
    if((datos3->iiii)==4)
        printf("   |--[NIVEL 3] PRACTICA 5 (Thread id: %d)\n",ObtenerIDHilo());
    else
    printf("   |--[NIVEL 3] HiloN#: %d, Thread id: %d, HiloPadre: %d\n",(datos3->iiii)+1,ObtenerIDHilo(),(datos3->hiloPadre));   
    pthread_exit(NULL); //Sale del hilo
}
int ObtenerIDHilo(void ){
    pid_t threadid = syscall(SYS_gettid); 
    return threadid; //Sirve para obtener el identificador del hilo
}