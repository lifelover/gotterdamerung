#include <stdio.h>   	//Borrado de archivos de texto
#include <sys/mman.h>	//rm INV* RES* MULT* SU* TRANS*
#include <sys/wait.h>	//Compilar programa
#include <unistd.h>		//gcc -Wall -lm 1.c -o 1
#include <stdlib.h>		//InsEnMat (matrizFuente, IndiceA,IndiceB)
#include "testOK.h"		//ImpMat(matrizOrigen,IndiceA,IndiceB,Etiqueta,NoMostrarDecimales(1),NoImprimirAArchivo(1))
#define SIZ_MAX 10		//<= LargoXAncho de la matriz
int main(){				//ESTE SIZE_MAX TIENE QUE SER IGUAL AL DE LA BIBLIOTECA
			float a[SIZ_MAX][SIZ_MAX],b[SIZ_MAX][SIZ_MAX],c[SIZ_MAX][SIZ_MAX];
			int contc=1;				//Este demo solo usa una matriz de 3x3
			for(int i=0;i<SIZ_MAX;i++)		//debido a que es dificil crear de manera
				for(int j=0;j<SIZ_MAX;j++)	//automatizada una matriz 10x10
				{						//donde todas las entradas sean
					a[i][j]=contc;		//Lineal.Independientes (para que exista una inversa)
					b[i][j]=contc;		//DEMO
					contc++;			//DEMO
				}						//DEMO
			a[2][1]=b[2][1]=9;			//<= se hacen cambios en ciertos indices
			a[2][2]=b[2][2]=8;			//<= para tener una matriz L.I
			printf("[PADRE | PID: %d]\n",getpid());	
			//printf("Inserte Matriz A: \n"); //Descomentar lo de arriba y comentar esto para el demo
			//InsEnMat(a,SIZ_MAX,SIZ_MAX);	//Tambien
			//printf("Inserte Matriz B: \n");	//Tambien
			//InsEnMat(b,SIZ_MAX,SIZ_MAX);	//Tambien
			ImpMat(a,SIZ_MAX,SIZ_MAX,"blabla",1,1);	//NoComentar
				printf("\n===SUMA=== [HIJO | PPID: %d PID: %d]\n",getppid(),getpid());
				SumaOResta(a,b,c,SIZ_MAX,SIZ_MAX,1); //solo si es 1 las va sumar
				ImpMat(c, SIZ_MAX,SIZ_MAX, "SUMA",1,0);
				
				printf("\n===RESTA== [HIJO | PPID: %d PID: %d]\n",getppid(),getpid());
				SumaOResta(a,b,c,SIZ_MAX,SIZ_MAX,0); 
				ImpMat(c, SIZ_MAX,SIZ_MAX, "RESTA",1,0);
				
				printf("\n===MULTIP= [HIJO | PPID: %d PID: %d]\n",getppid(),getpid());
				Multiplicar(a,b,c); //solo si es 1 las va sumar
				ImpMat(c, SIZ_MAX,SIZ_MAX, "MULTIPLICACION",1,0);
				
				printf("\n===TRANSP= [HIJO | PPID: %d PID: %d]\n",getppid(),getpid());
				Transpuesta(a,c,SIZ_MAX,SIZ_MAX);
				printf("TRANSPUESTA DE A:\n");
				ImpMat(c, SIZ_MAX,SIZ_MAX, "TRANSPUESTA_A",1,0);
				Transpuesta(b,c,SIZ_MAX,SIZ_MAX);
				printf("TRANSPUESTA DE B:\n");
				ImpMat(c, SIZ_MAX,SIZ_MAX, "TRANSPUESTA_B",1,0);
				
				printf("\n=INVERSA== [HIJO | PPID: %d PID: %d]\n",getppid(),getpid());
				if(determ(a,SIZ_MAX)!=0 && determ(b,SIZ_MAX)!=0){
					TreInversaCaller(a,c,SIZ_MAX);
					printf("\nInversa Matriz A:\n");
					ImpMat(c, SIZ_MAX,SIZ_MAX, "INVERSA_A",0,0);
					TreInversaCaller(b,c,SIZ_MAX);
					printf("\nInversa Matriz B:\n");
					ImpMat(c, SIZ_MAX,SIZ_MAX, "INVERSA_B",0,0);
				}
				else
					printf("Alguna de las dos matrices no es invertible.\n");
					//HIJO QUE REALIZA OPERACIONES DE LECTURA 
				printf("\n===LECTURA ARCHIVOS CREADOS= [HIJO | PPID: %d PID: %d]\n",getppid(),getpid());
				LeerArchivos("SUMA");
				LeerArchivos("RESTA");
				LeerArchivos("MULTIPLICACION");
				LeerArchivos("TRANSPUESTA_A");
				LeerArchivos("TRANSPUESTA_B");
				LeerArchivos("INVERSA_A");
				LeerArchivos("INVERSA_B");
				
		}