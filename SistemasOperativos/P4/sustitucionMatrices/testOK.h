#include<stdio.h>
#include<math.h>
#include<string.h>
#define SIZ_MAX 10
float determ(float a[SIZ_MAX][SIZ_MAX], float tam);
void TreInversaCaller(float [SIZ_MAX][SIZ_MAX], float dest[SIZ_MAX][SIZ_MAX], float);
void treinversa(float [][SIZ_MAX], float [][SIZ_MAX], float dest[][SIZ_MAX], float);
void ImpMat(float matrix[SIZ_MAX][SIZ_MAX], int row, int column, char* nombre, int nodecim, int nofile, int nomostrar);
void InsEnMat(float matrix[SIZ_MAX][SIZ_MAX], int row, int column);
void SumaOResta (float matrix[SIZ_MAX][SIZ_MAX], float matrix2[SIZ_MAX][SIZ_MAX], float matrixDest[SIZ_MAX][SIZ_MAX], int row, int column, int pain);
void Transpuesta(float matrix[SIZ_MAX][SIZ_MAX], float matrixDest[SIZ_MAX][SIZ_MAX], int row, int column);
void Multiplicar(float matrix[SIZ_MAX][SIZ_MAX], float matrix2[SIZ_MAX][SIZ_MAX], float matrixDest[SIZ_MAX][SIZ_MAX]);
void CargarEnArreglo(char *NombreArchivo, float matrix[SIZ_MAX][SIZ_MAX],int tam);

float determ(float a[SIZ_MAX][SIZ_MAX], float tam)
{
  float s = 1, det = 0, b[SIZ_MAX][SIZ_MAX];
  int i, j, m, n, c;
  if (tam == 1)
    {
     return (a[0][0]);
    }
  else
    {
     det = 0;
     for (c = 0; c < tam; c++)
       {
        m = 0;
        n = 0;
        for (i = 0;i < tam; i++)
          {
            for (j = 0 ;j < tam; j++)
              {
                b[i][j] = 0;
                if (i != 0 && j != c)
                 {
                   b[m][n] = a[i][j];
                   if (n < (tam - 2))
                    n++;
                   else
                    {
                     n = 0;
                     m++;
                     }
                }
               }
             }
          det = det + s * (a[0][c] * determ(b, tam - 1));
          s = -1 * s;
          }
    }
    return (det);
}
void TreInversaCaller(float num[SIZ_MAX][SIZ_MAX],float dest[SIZ_MAX][SIZ_MAX], float f)
{
 float b[SIZ_MAX][SIZ_MAX], fac[SIZ_MAX][SIZ_MAX];
 int p, q, m, n, i, j;
 for (q = 0;q < f; q++)
 {
   for (p = 0;p < f; p++)
    {
     m = 0;
     n = 0;
     for (i = 0;i < f; i++)
     {
       for (j = 0;j < f; j++)
        {
          if (i != q && j != p)
          {
            b[m][n] = num[i][j];
            if (n < (f - 2))
             n++;
            else
             {
               n = 0;
               m++;
               }
            }
        }
      }
      fac[q][p] = pow(-1, q + p) * determ(b, f - 1);
    }
  }
  treinversa(num, fac, dest, f);
}
void treinversa(float num[SIZ_MAX][SIZ_MAX], float fac[SIZ_MAX][SIZ_MAX], float dest[SIZ_MAX][SIZ_MAX], float r)
{
  int i, j;
  float b[SIZ_MAX][SIZ_MAX], d;
  for (i = 0;i < r; i++)
     for (j = 0;j < r; j++)
        {
            b[i][j] = fac[j][i];
        }
  d = determ(num, r);
for (i = 0;i < r; i++)
    {
        for (j = 0;j < r; j++)
        {
            dest[i][j] = b[i][j] / d;
        }
    }
 }
 void InsEnMat (float matrix[SIZ_MAX][SIZ_MAX], int row, int column)
{
    int i, j;
    for (i=0;i<row;i++)
    {

        for (j=0;j<column;j++)
        {
            printf ("POS [%d | %d] ", i+1, j+1);
            scanf  ("%f", &matrix[i][j]);
        }
    }
}
void ImpMat (float matrix[SIZ_MAX][SIZ_MAX],int row, int column, char* operacion, int nodecim, int nofile, int nomostrar)
{
    int i, j;
    FILE *NuevoArchivo;
    if(nofile!=1)
      NuevoArchivo = fopen(operacion, "w");
    if(NuevoArchivo!=NULL && nofile!=1)
      fprintf(NuevoArchivo,"%s \n",operacion);
    for(i=0;i<row;i++)
      {
          for(j=0;j<column;j++)
          {
              if(nodecim==1)
              {
              if(nomostrar!=1)
                printf("%.0lf ", matrix[i][j]);
              if(NuevoArchivo!=NULL && nofile!=1)
                fprintf(NuevoArchivo,"%.0lf ",matrix[i][j]);
              }
              else
              {
              if(nomostrar!=1)
                printf("%.2lf ", matrix[i][j]);
              if(NuevoArchivo!=NULL && nofile!=1)
                fprintf(NuevoArchivo,"%.2lf ",matrix[i][j]);
              }
          }
          if(nomostrar!=1)
            printf("\n");
          if(NuevoArchivo!=NULL && nofile!=1)
            fprintf(NuevoArchivo,"\n");
      }
      if(NuevoArchivo!=NULL && nofile!=1)
        fclose(NuevoArchivo);
  }
void SumaOResta (float matrix[SIZ_MAX][SIZ_MAX], float matrix2[SIZ_MAX][SIZ_MAX], float matrixDest[SIZ_MAX][SIZ_MAX], int row, int column, int pain)
{
    int i;
    int j;
    for(i=0;i<row;i++)
        for(j=0;j<column;j++)
        {
          if(pain==1)
            matrixDest[i][j]=matrix[i][j]+matrix2[i][j];
          else
            matrixDest[i][j]=matrix[i][j]-matrix2[i][j];
        }
}
void Transpuesta(float matrix[SIZ_MAX][SIZ_MAX], float matrixDest[SIZ_MAX][SIZ_MAX], int row, int column)
{
    int i;
    int j;
    for(i=0;i<row;i++)
        for(j=0;j<column;j++)
        {
            matrixDest[i][j]=matrix[j][i];
        }
}
void Multiplicar(float matrix[SIZ_MAX][SIZ_MAX], float matrix2[SIZ_MAX][SIZ_MAX], float matrixDest[SIZ_MAX][SIZ_MAX])
{
    int i, j, k;
    for (i=0;i<SIZ_MAX; i++)
    {
        for (j=0;j<SIZ_MAX;j++)
        {
            matrixDest[i][j] = 0;
            for (k = 0; k < SIZ_MAX; k++)
                matrixDest[i][j] += matrix[i][k]*matrix2[k][j];
        }
    } 
}
void LeerArchivos(char *NombreArchivo)
{
    char c; FILE *PTRNomArch;
    PTRNomArch = fopen(NombreArchivo, "r");
    if (PTRNomArch == NULL)
    {
        printf("No se pudo abrir el archivo %s\n",NombreArchivo);
        exit(0);
    }
    c = fgetc(PTRNomArch);
    while (c != EOF)
    {
        printf ("%c", c);
        c = fgetc(PTRNomArch);
    }
    fclose(PTRNomArch);
}
 void CargarEnArreglo(char *NombreArchivo, float matrix[SIZ_MAX][SIZ_MAX], int tam)
{
    int counter=0, numberRead=0, i, j,cont=0;
    int array[100];
    FILE *PTRNomArch;
    PTRNomArch = fopen(NombreArchivo, "r");
    if (PTRNomArch == NULL)
    {
        printf("No se pudo abrir el archivo %s\n",NombreArchivo);
        exit(0);
    }
    char buffer[100];
    fgets(buffer, 100, PTRNomArch);
    char arr[666];
    char* ptr;
    fread(arr , 1,sizeof arr , PTRNomArch);
    ptr = strtok(arr , " ");
    while(ptr)
    {
      array[counter++] = strtol(ptr , NULL , 10);
      ++numberRead;
      ptr = strtok(NULL , " ");
    }
    for(i=0;i<tam;i++)
      for(j=0;j<tam;j++)
      {
        matrix[i][j]=(double)array[cont];
        cont++;
      }       
    fclose(PTRNomArch);
}         
