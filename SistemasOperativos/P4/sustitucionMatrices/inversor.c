#include <stdio.h>   	//Borrado de archivos de texto
#include <sys/mman.h>	//rm INV* RES* MULT* SU* TRANS*
#include <sys/wait.h>	//Compilar programa
#include <unistd.h>		//gcc -Wall -lm 1.c -o 1
#include <stdlib.h>		//InsEnMat (matrizFuente, IndiceA,IndiceB)
#include "testOK.h"		//ImpMat(matrizOrigen,IndiceA,IndiceB,Etiqueta,NoMostrarDecimales(1),NoImprimirAArchivo(1),NoMostrar(1)
#define SIZ_MAX 10		//<= LargoXAncho de la matriz, cambiar a 3 para el demo
int main(){				//ESTE SIZE_MAX TIENE QUE SER IGUAL AL DE LA BIBLIOTECA
			//PROCESO SUMA.
			printf("\n----[PROCESO QUE REALIZA LA INVERSION EN EJECUCION] PPID: %d PID: %d\n",getppid(),getpid());
			float a[SIZ_MAX][SIZ_MAX],b[SIZ_MAX][SIZ_MAX],c[SIZ_MAX][SIZ_MAX];
			CargarEnArreglo("MATRIZ A ORIGINAL",a,SIZ_MAX);
			CargarEnArreglo("MATRIZ A ORIGINAL",b,SIZ_MAX);
			if(determ(a,SIZ_MAX)!=0)
			{
					TreInversaCaller(a,c,SIZ_MAX);
					ImpMat(c,SIZ_MAX,SIZ_MAX,"INVERSA-A-EXECV",0,0,1);
			}
			else
				printf("No es posible invertir A.\n");
			if(determ(b,SIZ_MAX)!=0)
			{
					TreInversaCaller(b,c,SIZ_MAX);
					ImpMat(c,SIZ_MAX,SIZ_MAX,"INVERSA-B-EXECV",0,0,1);
			}
			else
				printf("No es posible invertir B.\n");
		}