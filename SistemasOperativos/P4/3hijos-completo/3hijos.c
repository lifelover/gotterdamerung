#include <stdio.h>   	//Borrado de archivos de texto
#include <sys/wait.h>	//Compilar programa
#include <unistd.h>		//gcc -Wall -lm 1.c -o 
#include <stdlib.h>		//InsEnMat (matrizFuente, IndiceA,IndiceB)
int main(){		
			if(fork()==0)
			{

				printf("\n----[HIJO QUE VERIFICA BALANCEO] PPID: %d PID: %d\n",getppid(),getpid());
				char *argv[] = {"./balancepar", "{}{}(()())", 0};
  				execv("./balancepar", argv);  
				exit(0);
			}
			wait(0);
			if(fork()==0)
			{
				printf("----[HIJO QUE CAMBIA PERMISOS] PPID: %d PID: %d\n",getppid(),getpid());
				char *argv[] = {"./cambiarpermisos", "test", "TRANSPUESTA_A", "0755", 0};
  				execv("./cambiarpermisos", argv);
				exit(0);
			}
			wait(0);
			if(fork()==0)
			{
				printf("\n----[HIJO QUE INVIERTE UNA MATRIZ] PPID: %d PID: %d\n",getppid(),getpid());
				char *argv[] = {"./inv",  0};
  				execvp("./inv", argv);  
				exit(0);
			}
			wait(0);

}