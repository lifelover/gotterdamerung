#include <stdio.h>   	//Borrado de archivos de texto
#include <sys/mman.h>	//rm INV* RES* MULT* SU* TRANS*
#include <sys/wait.h>	//Compilar programa
#include <unistd.h>		//gcc -Wall -lm 1.c -o 1
#include <stdlib.h>		//InsEnMat (matrizFuente, IndiceA,IndiceB)
#include "testOK.h"		//ImpMat(matrizOrigen,IndiceA,IndiceB,Etiqueta,NoMostrarDecimales(1),NoImprimirAArchivo(1))
#define SIZ_MAX 3		//<= LargoXAncho de la matriz, cambiar a 3 para el demo
int main(){				//ESTE SIZE_MAX TIENE QUE SER IGUAL AL DE LA BIBLIOTECA
			printf("[MATRICES INVERSAS] PPID: %d PID: %d\n",getppid(),getpid());	
			float a[SIZ_MAX][SIZ_MAX],b[SIZ_MAX][SIZ_MAX],c[SIZ_MAX][SIZ_MAX];
			int contc=1;				//Este demo solo usa una matriz de 3x3
			for(int i=0;i<SIZ_MAX;i++)		//debido a que es dificil crear de manera
				for(int j=0;j<SIZ_MAX;j++)	//automatizada una matriz 10x10
				{						//donde todas las entradas sean
					a[i][j]=contc;		//Lineal.Independientes (para que exista una inversa)
					b[i][j]=contc;		//DEMO
					contc++;			//DEMO
				}						//DEMO
			a[2][1]=b[2][1]=9;			//<= se hacen cambios en ciertos indices
			a[2][2]=b[2][2]=8;			//<= para tener una matriz L.I 
			printf("Matriz A y B originales:\n");
			ImpMat(a, SIZ_MAX,SIZ_MAX,NULL,1,1);
			if(fork()==0)
			{		//HIJO QUE REALIZA OPERACIONES DE INVERSION
				printf("=INVERSA== [HIJO | PPID: %d PID: %d]\n",getppid(),getpid());
				if(determ(a,SIZ_MAX)!=0 && determ(b,SIZ_MAX)!=0){
					TreInversaCaller(a,c,SIZ_MAX);
					printf("\nInversa Matriz A:\n");
					ImpMat(c, SIZ_MAX,SIZ_MAX, "INVERSA_A",0,0);
					TreInversaCaller(b,c,SIZ_MAX);
					printf("\nInversa Matriz B:\n");
					ImpMat(c, SIZ_MAX,SIZ_MAX, "INVERSA_B",0,0);
				}
				else
					printf("Alguna de las dos matrices no es invertible.\n");
				exit(0);
			}
				wait(0);	
			if(fork()==0)
			{		//HIJO QUE REALIZA OPERACIONES DE LECTURA 
				printf("\n===LECTURA ARCHIVOS CREADOS= [HIJO | PPID: %d PID: %d]\n",getppid(),getpid());
				LeerArchivos("INVERSA_A");
				LeerArchivos("INVERSA_B");
				exit(0);
			}
				wait(0);	
		}