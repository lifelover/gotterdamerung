#include <stdio.h>
#include "operadores.h"
#include <stdlib.h>
#define SIZ_MAX 3
int main(int argc, char **argv)
{
  sem_t *ptrsemaforo; //Apuntador de tipo semaforo
  if(sem_unlink("semaforochevere")!=0) //Tratar de desenlazar un semaforo ya existente
        printf("[info: no habia semaforos anteriormente]\n");
  if ((ptrsemaforo = sem_open("semaforochevere", O_CREAT, 0644, 1)) == SEM_FAILED) {
        printf("Error al inicializar semaforo\n"); //Tratar de crear un nuevo semaforo con nombre
        exit(1);
  }
    printf("[info: dir_semaforo: %p]\n",ptrsemaforo);
    float a[SIZ_MAX][SIZ_MAX],b[SIZ_MAX][SIZ_MAX],res[SIZ_MAX][SIZ_MAX];;
    printf("Ingrese datos en A:\n");
    InsEnMat(a,SIZ_MAX,SIZ_MAX);
    CompartirMat(a,1111);
    printf("Ingrese datos en B:\n");
    InsEnMat(b,SIZ_MAX,SIZ_MAX);
    CompartirMat(b,1112);
  if(fork()==0) //Primer hijo
  {
    if((sem_wait(ptrsemaforo))==0) //Chequeo del primers semaforo
    {
      printf("[info-hijo]: hijo ha entrado a la region critica\n");
      ObtenerMatCompartida(1111,a);
      ObtenerMatCompartida(1112,b);
      Multiplicar(a,b,res);
      CompartirMat(res,1113); //Aqui, esta matriz la recibe el padre
      printf("CALCULO DE DATOS HIJO 1\n");
    }
    sleep(0); 
    if(sem_post(ptrsemaforo)==0) //Dejar que el abuelo continue(1), a partir de este punto
    printf("[info-hijo] hijo salio de la seccion critica\n");//es posible reutilizar las matrices
    sem_wait(ptrsemaforo); //poner en "rojo" el semaforo para poder meter datos despues de regresar el resultado
      printf("Ingrese datos en A:\n");
      InsEnMat(a,SIZ_MAX,SIZ_MAX);
      CompartirMat(a,1111);
      printf("Ingrese datos en B:\n");
      InsEnMat(b,SIZ_MAX,SIZ_MAX);
      CompartirMat(b,1112);
    sem_post(ptrsemaforo);//liberar el semaforo para el nieto
    if(fork()==0)
    {  
        if((sem_wait(ptrsemaforo))==0) //Esperar autorizacion del abuelo (2)
          {
          printf("[info-nieto] nieto ha entrado a la seccion critica\n");//es posible reutilizar las matrices
          ObtenerMatCompartida(1111,a);
          ObtenerMatCompartida(1112,b);
          SumaOResta(a,b,res,1);
          //ImpMat(res,NULL,1,1,0);
          CompartirMat(res,1113);
          }
        sleep(0);
          if(sem_post(ptrsemaforo)==0) //Dejar que el abuelo continue(3)
          printf("[info-nieto] nieto salio de la seccion critica\n");      
        exit(0);
    }
    exit(0);
  }
    sleep(0);
    sem_wait(ptrsemaforo); //Esperar a que el hijo termine de liberar datos (1)
    ObtenerMatCompartida(1113,res);
    printf("Resultado de la multiplicacion: \n");
    ImpMat(res,NULL,1,1,0);
    if(determ(res,SIZ_MAX)!=0)
    {
        TreInversaCaller(res,a,SIZ_MAX);
        printf("Inversa de la multiplicacion: \n");
        ImpMat(a,"Inversa de la multiplicacionMem  .txt",0,0,0);
    }
    else
        printf("[ERROR] La multiplicacion no fue L.I.\n");
    sem_post(ptrsemaforo); //Permitir que el nieto se ejecute (2)
    sleep(15);
    sem_wait(ptrsemaforo); //Esperar a que el nieto realice operaciones(3)
    ObtenerMatCompartida(1113,res);
    printf("Resultado de la suma: \n");
    ImpMat(res,NULL,1,1,0);
    if(determ(res,SIZ_MAX)!=0)
    {
        TreInversaCaller(res,a,SIZ_MAX);
        printf("Inversa de la suma: \n");
        ImpMat(a,"Inversa de la suma  .txt",0,0,0);
    }
    else
        printf("[ERROR] La suma no fue L.I.\n");
    
    exit(0);
}
