#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

union semun {
	int val;
	struct semid_ds *buf;
	ushort *array;
};

void BorrarDeMemoria(key_t key){
	int semid;
	if ((semid = semget(key, 1, 0)) == -1) {
		printf("[nuevo semaforo en uso]\n");
	}
	else{
	if (semctl(semid, 0, IPC_RMID) == -1) {
		printf("No se pudo borrar\n");
	}
	else
		printf("[%d borrado de memoria.]\n",key);
	}
}
int iniciarsemaforo(key_t key, int nsems)  /* key from ftok() */
{
	union semun arg;
//	struct semid_ds buf;
	struct sembuf sb;
	int semid;
	BorrarDeMemoria(key); //Eliminar rastros del semaforo en memoria
	semid = semget(key, nsems, IPC_CREAT | IPC_EXCL | 0666);
	if (semid >= 0) { //Verificar que se pudo obtener el semaforo
		sb.sem_op = 1; sb.sem_flg = 0;
		arg.val = 1;
		for(sb.sem_num = 0; sb.sem_num < nsems; sb.sem_num++) { 
			/* do a semop() to "free" the semaphores. */
			/* this sets the sem_otime field, as needed below. */
			if (semop(semid, &sb, 1) == -1) {
				int e = errno;
				semctl(semid, 0, IPC_RMID); /* clean up */
				errno = e;
				return -1; /* error, check errno */
			}
		}

	} else if (errno == EEXIST) { /* someone else got it first */
		printf("[Imposible crear nuevo semaforo]\n");
		return -1;
	} else 
		return semid; /* error, check errno */
	return semid;
}

int main(void)
{
	int i;
	key_t key=666;
	int semid;
	struct sembuf sb;
	sb.sem_num = 0;
	sb.sem_op = -1;  /* set to allocate resource */
	sb.sem_flg = SEM_UNDO;

	/* grab the semaphore set created by seminit.c: */
	if ((semid = iniciarsemaforo(key, 1)) == -1) {
		printf("Error iniciarsemaforo\n");
		exit(1);
	}
/*
	printf("Press return to lock: ");
	getchar();
	printf("Trying to lock...\n");

	if (semop(semid, &sb, 1) == -1) {
		printf("Error semop\n");
		exit(1);
	}
	printf("Locked.\n");
	printf("Press return to unlock: ");
	getchar();

	sb.sem_op = 1; / free resource 
	if (semop(semid, &sb, 1) == -1) {
		printf("Error semop\n");
		exit(1);
	}
	printf("Unlocked\n");
*/
	if (fork() == 0) { /* child process*/
    for (i = 0; i < 10; i++) {
    if (semop(semid, &sb, 1) == -1) {
		printf("Error semop\n");
		exit(1);
	}
      printf("child entered crititical section: %d\n", i);
      printf("child leaving critical section\n");
      sb.sem_op = 1;
	if (semop(semid, &sb, 1) == -1) {
		printf("Error semop\n");
		exit(1);
	}
      sleep(1);
    }
    exit(0);
  }
  /* back to parent process */
  for (i = 0; i < 10; i++) {
    if (semop(semid, &sb, 1) == -1) {
		printf("Error semop\n");
		exit(1);
	}
    printf("parent entered critical section: %d\n", i);
    sleep(2);
    printf("parent leaving critical section\n");
    if (semop(semid, &sb, 1) == -1) {
		printf("Error semop\n");
		exit(1);
	}
	else
		printf("ok \n");
    
  }
	return 0;
}
