LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
ENTITY VARIABLES IS PORT(
    CLK, CLR, CTRL: IN STD_LOGIC;
    Q1: INOUT STD_LOGIC_VECTOR (1 DOWNTO 0);
	Q2: INOUT STD_LOGIC_VECTOR (2 DOWNTO 0)
);
ATTRIBUTE PIN_NUMBERS OF VARIABLES: ENTITY IS 
"CLK:1 CLR:2 CTRL:3 Q1(0):14 Q1(1):15 Q2(0):16 Q2(1):17 Q2(2):18";
END VARIABLES;
ARCHITECTURE CONT_NOSEC OF VARIABLES IS BEGIN
    CONTADOR: PROCESS (CLK,CLR,CTRL,Q1,Q2)BEGIN 
        IF (CLR='0') THEN
            Q1<="00";
            Q2<="000";
        ELSIF (CLK'EVENT AND CLK='1') THEN
            IF (CTRL='0') THEN
                CASE Q1 IS
                    WHEN "00"=>Q1<="01";
                    WHEN "01"=>Q1<="10";
                    WHEN "10"=>Q1<="11";
                    WHEN "11"=>Q1<="00";
                    WHEN OTHERS=>Q1<="00";
                END CASE;
            ELSE 
                CASE Q2 IS 
                    WHEN "000"=>Q2<="001";
                    WHEN "001"=>Q2<="010";
                    WHEN "010"=>Q2<="011";
                    WHEN "011"=>Q2<="100";
                    WHEN "100"=>Q2<="101";
                    WHEN "101"=>Q2<="110";
                    WHEN "110"=>Q2<="111"; 
                    WHEN "111"=>Q2<="000";
                    WHEN OTHERS=>Q2<="000";
                END CASE;
            END IF;
        END IF;
    END PROCESS CONTADOR;
END CONT_NOSEC;