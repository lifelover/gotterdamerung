module variables ( 
	clk,
	clr,
	ctrl,
	q1,
	q2
	) ;

input  clk;
input  clr;
input  ctrl;
inout [1:0] q1;
inout [2:0] q2;
