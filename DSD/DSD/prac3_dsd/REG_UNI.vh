module variables ( 
	clk,
	clr,
	cd,
	ci,
	c,
	dato,
	q
	) ;

input  clk;
input  clr;
input  cd;
input  ci;
input [1:0] c;
input [7:0] dato;
inout [7:0] q;
