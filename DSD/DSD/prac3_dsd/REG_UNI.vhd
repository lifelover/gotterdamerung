LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
ENTITY VARIABLES IS PORT(
	CLK,CLR,CD,CI: IN STD_LOGIC;
	C: IN STD_LOGIC_VECTOR (1 DOWNTO 0);
	DATO: IN STD_LOGIC_VECTOR (7 DOWNTO 0);
	Q: INOUT STD_LOGIC_VECTOR (7 DOWNTO 0)
);
ATTRIBUTE PIN_NUMBERS OF VARIABLES: ENTITY IS
"CLK:1 CLR:2 C(1):3 C(0):4 DATO(7):5 DATO(6):6 DATO(5):7 DATO(4):8 DATO(3):9 DATO(2):10 DATO(1):11 DATO(0):13 CD:14 CI:15 Q(0):16 Q(1):17 Q(2):18 Q(3):19 Q(4):20 Q(5):21 Q(6):22 Q(7):23";
END VARIABLES;
ARCHITECTURE REG_UNI OF VARIABLES IS BEGIN
PROCESS(CLK,CLR,C) BEGIN
IF (CLR='0') THEN
	Q<="00000000";
ELSIF (CLK'EVENT AND CLK='1') THEN
	CASE C IS
		WHEN "00"=>Q<=DATO;
		WHEN "01"=>Q<=Q;
		WHEN "10"=>Q<=TO_STDLOGICVECTOR((TO_BITVECTOR(Q)) SRL 1);
				   Q(7)<=CD;
		WHEN OTHERS=>Q<=TO_STDLOGICVECTOR((TO_BITVECTOR(Q)) SLL 1);
				     Q(0)<=CI;
	END CASE;
END IF;
END PROCESS;
END REG_UNI;
