module variables ( 
	clk,
	clr,
	pre,
	jk,
	q,
	qn
	) ;

input  clk;
input  clr;
input  pre;
input [1:0] jk;
inout  q;
inout  qn;
