module variables ( 
	d,
	clk,
	clr,
	pre,
	q,
	qn
	) ;

input  d;
input  clk;
input  clr;
input  pre;
inout  q;
inout  qn;
