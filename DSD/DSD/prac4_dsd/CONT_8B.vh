module variables ( 
	c,
	clk,
	clr,
	q
	) ;

input  c;
input  clk;
input  clr;
inout [7:0] q;
