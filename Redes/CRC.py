from simpleeval import simple_eval
import re #Para las expresiones regulares

def mod2div(dividendo, divisor): #Funcion que aplica division mod2 
    llong = len(divisor)
    tmp = dividendo[0 : llong]
    while llong < len(dividendo):
        if tmp[0] == '1':
            tmp = xor(divisor, tmp) + dividendo[llong]
        else:   
            tmp = xor('0'*llong, tmp) + dividendo[llong]
        llong += 1
    if tmp[0] == '1':
        tmp = xor(divisor, tmp)
    else:
        tmp = xor('0'*llong, tmp)
    resultado = tmp
    return resultado
def xor(a, b): #Esta funcion aplica el xor a dos cadenas
    result = []
    for i in range(1, len(b)):
        if a[i] == b[i]: #Si son iguales, poner 0
            result.append('0')
        else:             #Poner 1 si son diferentes
            result.append('1')
    return ''.join(result)

def sxor(x, y):
    return '{0:b}'.format(int(x, 2) ^ int(y, 2))

mensaje = input("Ingrese su mensaje (binario): ")
polinomio = input("Ingrese su polinomio: ")
polinomioAEval = polinomio.replace("x^","(2)**") #Reemplazar las x con (2) para evaluar
GarbBinPoliEval = bin(simple_eval(polinomioAEval)) # Evaluar y convertir a binario
BinPoliEval = GarbBinPoliEval[2:] #Quitar el 0b que pone python
maxGrad = re.findall(r'\d+', polinomio) #Buscar todos los numeros en el polinomio generador
maxGrad = [int(i) for i in maxGrad] #Convertirlos a enteros
maxGrad = max(maxGrad)              #Obtener el numero maximo (el grado)
PdX = "" #Cadena vacia que se va llenar con los 0's dependiendo del grado
for i in range (int(maxGrad)):
    PdX = PdX + "0" #Poner tantos ceros como sean necesarios
PdX = mensaje + PdX #Pegar el mensaje a PdX
print("[I] Grado maximo: ", maxGrad)
print("[I] Polinomio generador en binario: ",BinPoliEval)	#DIVISOR
print(">>> Funcion multiplicada por el grado: ",PdX)		#DIVIDENDO
residuo =  mod2div(PdX,BinPoliEval) #Se hace la division modulo 2
DatosChidos = mensaje + residuo #Se le pega el residuo al mensaje original
print(">>> Residuo: ",residuo)
print(">>> CRC (datos+residuo): ",DatosChidos)
print(">>> Comprobacion... ")
print("--> Dividir el CRC (",DatosChidos, ") mod2 el polinomio generador (",BinPoliEval,")")
residuo2 = mod2div(DatosChidos,BinPoliEval)
print(">>> Residuo: ", residuo2)

